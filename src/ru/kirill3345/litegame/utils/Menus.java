package ru.kirill3345.litegame.utils;

import java.util.Arrays;
import java.util.List;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import ru.kirill3345.litegame.LiteGame;
import ru.kirill3345.litegame.utils.world.WorldManager;

public class Menus {
    public static Inventory worldSettings = worldSettings();
    public static Inventory gadgets = gadgets();
    public static Inventory setTime = setTime();
    public static Inventory setMusic = setMusic();
    public static Inventory needLogin = needLogin();
    public static Inventory setWeather = setWeather();
    public static Inventory skyBlock = skyBlock();
    public static Inventory deleteWorld = deleteWorld();
    public static Inventory saveWorld = saveWorld();
    public static Inventory selectGenerator = selectGenerator();
    public static Inventory otherBlocks = otherBlocks();
    public static Inventory info = info();
    public static Inventory myWorldCreate = myWorldCreate();
    public static Inventory myWorldDefault = myWorldDefault();
    public static Inventory myWorldArena = myWorldArena();
    public static Inventory myWorldSkyBlock = myWorldSkyBlock();
    public static Inventory myWorldArtStudio = myWorldArtStudio();
    public static Inventory kitPvp = kitPvp();
    private static final List<String> allowedBuild = Arrays.asList("§aРазрешено строить");
    private static final List<String> disallowedBuild = Arrays.asList("§cЗапрещено строить");
    private static final List<String> worldAllowedBuild = Arrays.asList
            (
                    "§aНовые игроки",
                    "§aмогут строить."
            );
    private static final List<String> worldDisallowedBuild = Arrays.asList
            (
                    "§cНовые игроки не",
                    "§cмогут строить."
            );
    private static final List<String> allowedFlying = Arrays.asList("§aРазрешено летать");
    private static final List<String> disallowedFlying = Arrays.asList("§cЗапрещено летать");
    private static final List<String> worldAllowedFlying = Arrays.asList
            (
                    "§aНовые игроки",
                    "§aмогут летать."
            );
    private static final List<String> worldDisallowedFlying = Arrays.asList
            (
                    "§cНовые игроки не",
                    "§cмогут летать."
            );

    private static Inventory worldSettings() {
        Inventory menu = LiteGame.server.createInventory(null, 45, "Управление миром");

        {
            ItemStack item = new ItemStack(Material.MAP);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fСохранить/Выйти");
            meta.setLore(Arrays.asList("§7Выйти из мира"));
            item.setItemMeta(meta);
            menu.setItem(10, item);
        }

        {
            ItemStack item = new ItemStack(Material.BRICK);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fУправление строительством");
            meta.setLore(Arrays.asList("§7Разрешить/запретить строить"));
            item.setItemMeta(meta);
            menu.setItem(12, item);
        }

        {
            ItemStack item = new ItemStack(Material.WATCH);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fВремя");
            meta.setLore(Arrays.asList("§7Сделать утро/день/вечер/ночь"));
            item.setItemMeta(meta);
            menu.setItem(14, item);
        }

        {
            ItemStack item = new ItemStack(Material.JUKEBOX);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fМузыка");
            meta.setLore(Arrays.asList("§7Поставить другую музыку"));
            item.setItemMeta(meta);
            menu.setItem(16, item);
        }

        {
            ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fВыгнать игрока");
            meta.setLore(Arrays.asList("§7Кто-то мешает?", "§7Выгони его!"));
            item.setItemMeta(meta);
            menu.setItem(27, item);
        }

        {
            ItemStack item = new ItemStack(Material.WOOD_DOOR);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fУправление входом");
            meta.setLore(Arrays.asList("§7Разрешить входить только", "§7некоторым игрокам"));
            item.setItemMeta(meta);
            menu.setItem(29, item);
        }

        {
            ItemStack item = new ItemStack(Material.FEATHER);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fУправление полётом");
            meta.setLore(Arrays.asList("§7Разрешить/запретить летать"));
            item.setItemMeta(meta);
            menu.setItem(31, item);
        }

        {
            ItemStack item = new ItemStack(Material.ENDER_PEARL);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fТочка появления");
            meta.setLore(Arrays.asList("§7ЛКМ - установить где Вы стоите", "§7ПКМ - сбросить на стандартную"));
            item.setItemMeta(meta);
            menu.setItem(33, item);
        }

        {
            ItemStack item = new ItemStack(Material.WATER_BUCKET);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fПогода");
            meta.setLore(Arrays.asList("§7Сделать солнечно/дождливо"));
            item.setItemMeta(meta);
            menu.setItem(35, item);
        }

        return menu;
    }

    private static Inventory gadgets() {
        Inventory menu = LiteGame.server.createInventory(null, 45, "Гаджеты");

        menu.setItem(10, Items.enderPearlGadget);
        menu.setItem(12, Items.fireworkGadget);
        menu.setItem(14, Items.snowBallGun);
        menu.setItem(16, Items.tntGadget);
        menu.setItem(29, Items.arrowGadget);
        menu.setItem(31, Items.enderRodGadget);
        menu.setItem(33, Items.dragonSkullGadget);

        return menu;
    }

    private static Inventory needLogin() {
        Inventory menu = LiteGame.server.createInventory(null, 27, "Войдите в аккаунт");

        ItemStack item = new ItemStack(Material.BARRIER);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("§fВойдите в аккаунт");
        meta.setLore(Arrays.asList("§7Командой§f /pass"));
        item.setItemMeta(meta);
        menu.setItem(13, item);

        return menu;
    }

    public static Inventory playerSettings(Player player) {
        Inventory menu = LiteGame.server.createInventory(null, 27, "Настройки");

        String uuid = player.getUniqueId().toString();

        {
            ItemStack item = new ItemStack(Material.SADDLE);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fСтакинг");
            meta.setLore
                    (
                            Arrays.asList
                                    (
                                            "§7При включенном стакинге вы",
                                            "§7сможете брать себе на плечи",
                                            "§7других игроков и швыряться",
                                            "§7ими куда угодно. То же самое",
                                            "§7могут делать и с вами.",
                                            !PlayersUtils.isEnabled(uuid, "stackingDisabled") ? "§aВключено" : "§cВыключено"
                                    )
                    );
            item.setItemMeta(meta);
            menu.setItem(10, item);
        }

        {
            ItemStack item = new ItemStack(Material.JUKEBOX);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fМузыка в лобби");
            meta.setLore
                    (
                            Arrays.asList
                                    (
                                            "§7Случайная музыка при",
                                            "§7входе в лобби.",
                                            !PlayersUtils.isEnabled(uuid, "musicInHubDisabled") ? "§aВключено" : "§cВыключено"
                                    )
                    );
            item.setItemMeta(meta);
            menu.setItem(12, item);
        }

        {
            ItemStack item = new ItemStack(Material.PAPER);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fЛичные сообщения");
            meta.setLore
                    (
                            Arrays.asList
                                    (
                                            "§7Принимать личные сообщения",
                                            "§7от других игроков.",
                                            !PlayersUtils.isEnabled(uuid, "privateMessagesDisabled") ? "§aВключено" : "§cВыключено"
                                    )
                    );
            item.setItemMeta(meta);
            menu.setItem(14, item);
        }

        {
            ItemStack item = new ItemStack(Material.NOTE_BLOCK);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fЗвуковые оповещения");
            meta.setLore
                    (
                            Arrays.asList
                                    (
                                            "§7Звук при получении",
                                            "§7личного сообщения.",
                                            !PlayersUtils.isEnabled(uuid, "disableSoundNotify") ? "§aВключено" : "§cВыключено"
                                    )
                    );
            item.setItemMeta(meta);
            menu.setItem(16, item);
        }

        return menu;
    }

    private static Inventory skyBlock() {
        Inventory menu = LiteGame.server.createInventory(null, 27, "Управление SkyBlock");

        ItemStack item0 = new ItemStack(Material.MAP);
        ItemMeta meta0 = item0.getItemMeta();
        meta0.setDisplayName("§fСохранить/Выйти");
        meta0.setLore(Arrays.asList("§7Выйти из мира"));
        item0.setItemMeta(meta0);
        menu.setItem(10, item0);

        ItemStack item1 = new ItemStack(Material.BRICK);
        ItemMeta meta1 = item1.getItemMeta();
        meta1.setDisplayName("§fУправление строительством");
        meta1.setLore(Arrays.asList("§7Разрешить/запретить строить"));
        item1.setItemMeta(meta1);
        menu.setItem(12, item1);

        ItemStack item2 = new ItemStack(Material.WOOD_DOOR);
        ItemMeta meta2 = item2.getItemMeta();
        meta2.setDisplayName("§fУправление входом");
        meta2.setLore(Arrays.asList("§7Разрешить входить только", "§7некоторым игрокам"));
        item2.setItemMeta(meta2);
        menu.setItem(14, item2);

        ItemStack item3 = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        ItemMeta meta3 = item3.getItemMeta();
        meta3.setDisplayName("§fВыгнать игрока");
        meta3.setLore(Arrays.asList("§7Кто-то мешает?", "§7Выгони его!"));
        item3.setItemMeta(meta3);
        menu.setItem(16, item3);

        return menu;
    }

    private static Inventory deleteWorld() {
        Inventory menu = LiteGame.server.createInventory(null, 27, "Удалить мир");

        {
            ItemStack item = new ItemStack(Material.LAVA_BUCKET);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fУдалить мир");
            meta.setLore(Arrays.asList("§7Безвозвратное действие!"));
            item.setItemMeta(meta);
            menu.setItem(12, item);
        }

        {
            ItemStack item = new ItemStack(Material.MAP);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fНе удалять");
            meta.setLore(Arrays.asList("§7Закрыть меню"));
            item.setItemMeta(meta);
            menu.setItem(14, item);
        }

        return menu;
    }

    private static Inventory saveWorld() {
        Inventory menu = LiteGame.server.createInventory(null, 27, "Сохранить мир");

        ItemStack item0 = new ItemStack(Material.MAP);
        ItemMeta meta0 = item0.getItemMeta();
        meta0.setDisplayName("§fСохранить");
        meta0.setLore(Arrays.asList("§7Сохранить мир"));
        item0.setItemMeta(meta0);
        menu.setItem(11, item0);

        ItemStack item1 = new ItemStack(Material.REDSTONE_BLOCK);
        ItemMeta meta1 = item1.getItemMeta();
        meta1.setDisplayName("§fСохранить и выйти");
        meta1.setLore(Arrays.asList("§7Выйти из мира"));
        item1.setItemMeta(meta1);
        menu.setItem(13, item1);

        ItemStack item2 = new ItemStack(Material.WOOD_DOOR);
        ItemMeta meta2 = item2.getItemMeta();
        meta2.setDisplayName("§fВыйти без сохранения");
        meta2.setLore(Arrays.asList("§7Выйти из мира"));
        item2.setItemMeta(meta2);
        menu.setItem(15, item2);

        return menu;
    }

    private static Inventory selectGenerator() {
        Inventory menu = LiteGame.server.createInventory(null, 45, "Генерация");

        {
            ItemStack item = new ItemStack(Material.GRASS);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fПлоский мир");
            meta.setLore(Arrays.asList("§7Плоский мир для построек."));
            item.setItemMeta(meta);
            menu.setItem(11, item);
        }

        {
            ItemStack item = new ItemStack(Material.GLASS);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fПустой мир");
            meta.setLore(Arrays.asList("§7Для особых видов", "§7строений."));
            item.setItemMeta(meta);
            menu.setItem(13, item);
        }

        {
            ItemStack item = new ItemStack(Material.SAND);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fОстров");
            meta.setLore(Arrays.asList("§7Для других построек.", "§fНужен донат"));
            item.setItemMeta(meta);
            menu.setItem(15, item);
        }

        {
            ItemStack item = new ItemStack(Material.SNOW_BLOCK);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fСнежный мир");
            meta.setLore(Arrays.asList("§7Плоский мир для построек."));
            item.setItemMeta(meta);
            menu.setItem(30, item);
        }

        {
            ItemStack item = new ItemStack(Material.ICE);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fСнежный остров");
            meta.setLore(Arrays.asList("§7Для других построек.", "§fНужен донат"));
            item.setItemMeta(meta);
            menu.setItem(32, item);
        }

        return menu;
    }

    private static Inventory otherBlocks() {
        Inventory menu = LiteGame.server.createInventory(null, 27, "Особые блоки");

        menu.setItem(12, new ItemStack(Material.BARRIER));
        menu.setItem(13, new ItemStack(Material.DRAGON_EGG));
        menu.setItem(14, new ItemStack(Material.MOB_SPAWNER));

        return menu;
    }

    private static Inventory info() {
        Inventory menu = LiteGame.server.createInventory(null, 27, "Информация");

        {
            ItemStack item = new ItemStack(Material.CAKE);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fГруппа вконтакте");
            meta.setLore(Arrays.asList("§7" + ServerSettings.vkLink));
            item.setItemMeta(meta);
            menu.setItem(11, item);
        }

        {
            ItemStack item = new ItemStack(Material.EMERALD);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fКупить донат");
            meta.setLore(Arrays.asList("§7" + ServerSettings.donateLink));
            item.setItemMeta(meta);
            menu.setItem(13, item);
        }

        {
            ItemStack item = new ItemStack(Material.LAVA_BUCKET);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fУдаление миров");
            meta.setLore
                    (
                            Arrays.asList
                                    (
                                            "§7Миры удаляются когда нет",
                                            "§7свободного места, при условии",
                                            "§7что их хозяины не заходили",
                                            "§7на сервер более чем 5 дней.",
                                            "§fНа донатеров это не влияет."
                                    )
                    );
            item.setItemMeta(meta);
            menu.setItem(15, item);
        }

        return menu;
    }

    private static Inventory myWorldCreate() {
        Inventory menu = LiteGame.server.createInventory(null, 27, "Мой мир");

        {
            ItemStack item = new ItemStack(Material.SAPLING);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fСоздать остров");
            meta.setLore(Arrays.asList("§7SkyBlock."));
            item.setItemMeta(meta);
            menu.setItem(11, item);
        }

        {
            ItemStack item = new ItemStack(Material.MAP);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fСоздать мир");
            meta.setLore(Arrays.asList("§7Создать новый мир."));
            item.setItemMeta(meta);
            menu.setItem(13, item);
        }

        {
            ItemStack item = new ItemStack(Material.PAINTING);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fСоздать студию анимации");
            meta.setLore(Arrays.asList("§7Любишь строить пиксель-арты?", "§7Попробуй рисовать их!", "§7Анимируй их!"));
            item.setItemMeta(meta);
            menu.setItem(15, item);
        }

        return menu;
    }

    private static Inventory myWorldDefault() {
        Inventory menu = LiteGame.server.createInventory(null, 45, "Мой мир");

        {
            ItemStack item = new ItemStack(Material.BRICK);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fСтроительство");
            meta.setLore(Arrays.asList("§7Построй свою арену!"));
            item.setItemMeta(meta);
            menu.setItem(12, item);
        }

        {
            ItemStack item = new ItemStack(Material.DROPPER);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fЗагрузить мир в меню арен");
            meta.setLore(Arrays.asList("§7Вы не сможете редактировать свой мир."));
            item.setItemMeta(meta);
            menu.setItem(14, item);
        }

        {
            ItemStack item = new ItemStack(Material.LAVA_BUCKET);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fУдалить мир");
            meta.setLore(Arrays.asList("§7Удалить ваш вир", "§7без возможности", "§7восстановления."));
            item.setItemMeta(meta);
            menu.setItem(31, item);
        }

        return menu;
    }

    private static Inventory myWorldArena() {
        Inventory menu = LiteGame.server.createInventory(null, 54, "Арена");

        {
            ItemStack item = new ItemStack(Material.IRON_SWORD);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fPVP");
            meta.setLore(Arrays.asList("§7PVP", "§fИзменить тип мини-игры."));
            meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
            item.setItemMeta(meta);
            menu.setItem(10, item);
        }

        {
            ItemStack item = new ItemStack(Material.LEATHER_BOOTS);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fParkour");
            meta.setLore(Arrays.asList("§7Игрок должен добраться до", "§7финиша и ударить кристалл Энда.", "§fИзменить тип мини-игры."));
            meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
            item.setItemMeta(meta);
            menu.setItem(11, item);
        }

        {
            ItemStack item = new ItemStack(Material.EGG);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fSplegg");
            meta.setLore(Arrays.asList("§7Сломай под врагом", "§7блок!", "§fИзменить тип мини-игры."));
            item.setItemMeta(meta);
            menu.setItem(12, item);
        }

        {
            ItemStack item = new ItemStack(Material.ICE);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fThrowOut");
            meta.setLore(Arrays.asList("§7Скинь врага!", "§fИзменить тип мини-игры."));
            item.setItemMeta(meta);
            menu.setItem(13, item);
        }

        {
            ItemStack item = new ItemStack(Material.WOOD_SWORD);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fDuel");
            meta.setLore(Arrays.asList("§7Битва один на один.", "§fИзменить тип мини-игры."));
            meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
            item.setItemMeta(meta);
            menu.setItem(14, item);
        }

        {
            ItemStack item = new ItemStack(Material.REDSTONE);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fMurder");
            meta.setLore(Arrays.asList("§7Один против всех.", "§fИзменить тип мини-игры."));
            item.setItemMeta(meta);
            menu.setItem(15, item);
        }

        {
            ItemStack item = new ItemStack(Material.DIAMOND_BARDING);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fPaintball");
            meta.setLore(Arrays.asList("§7Разукрась врага!", "§fИзменить тип мини-игры."));
            meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
            item.setItemMeta(meta);
            menu.setItem(16, item);
        }

        {
            ItemStack item = new ItemStack(Material.TNT);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fTNTRun");
            meta.setLore(Arrays.asList("§7Блоки падают под тобой!", "§7Используйте нажимные плиты.", "§fИзменить тип мини-игры."));
            item.setItemMeta(meta);
            menu.setItem(20, item);
        }

        {
            ItemStack item = new ItemStack(Material.SHIELD);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fKitPVP");
            meta.setLore(Arrays.asList("§7PVP с наборами.", "§fИзменить тип мини-игры."));
            item.setItemMeta(meta);
            menu.setItem(21, item);
        }

        {
            ItemStack item = new ItemStack(Material.ENDER_PEARL);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fSkyWars");
            meta.setLore(Arrays.asList("§7PVP в небесах.", "§7Используйте кристаллы для", "§7появления игроков.", "§fИзменить тип мини-игры."));
            item.setItemMeta(meta);
            menu.setItem(22, item);
        }

        {
            ItemStack item = new ItemStack(Material.CHEST);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fSurvivalGames");
            meta.setLore(Arrays.asList("§7Используйте кристаллы для", "§7появления игроков.", "§fИзменить тип мини-игры."));
            item.setItemMeta(meta);
            menu.setItem(23, item);
        }

        {
            ItemStack item = new ItemStack(Material.DIAMOND);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fGame");
            meta.setLore(Arrays.asList("§7Уникальные игры, которые", "§7пишут игроки."));
            item.setItemMeta(meta);
            menu.setItem(24, item);
        }

        {
            ItemStack item = new ItemStack(Material.BARRIER);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fУбрать из меню арен");
            meta.setLore(Arrays.asList("§7Вы вновь сможете", "§7строить."));
            item.setItemMeta(meta);
            menu.setItem(40, item);
        }

        return menu;
    }

    private static Inventory myWorldSkyBlock() {
        Inventory menu = LiteGame.server.createInventory(null, 45, "Мой мир");

        {
            ItemStack item = new ItemStack(Material.SAPLING);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fЗапустить остров");
            meta.setLore(Arrays.asList("§7Продолжить играть на", "§7SkyBlock."));
            item.setItemMeta(meta);
            menu.setItem(13, item);
        }

        {
            ItemStack item = new ItemStack(Material.LAVA_BUCKET);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fУдалить мир");
            meta.setLore(Arrays.asList("§7Удалить ваш мир", "§7без возможности", "§7восстановления."));
            item.setItemMeta(meta);
            menu.setItem(31, item);
        }

        return menu;
    }

    private static Inventory myWorldArtStudio() {
        Inventory menu = LiteGame.server.createInventory(null, 45, "Мой мир");

        {
            ItemStack item = new ItemStack(Material.PAINTING);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fЗапустить студию пртов");
            meta.setLore(Arrays.asList("§7Продолжить рисовать."));
            item.setItemMeta(meta);
            menu.setItem(13, item);
        }

        {
            ItemStack item = new ItemStack(Material.LAVA_BUCKET);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fУдалить мир");
            meta.setLore(Arrays.asList("§7Удалить ваш мир", "§7без возможности", "§7восстановления."));
            item.setItemMeta(meta);
            menu.setItem(31, item);
        }

        return menu;
    }

    private static Inventory kitPvp() {
        Inventory menu = LiteGame.server.createInventory(null, 27, "Набор");

        {
            ItemStack item = new ItemStack(Material.IRON_SWORD);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fНачальный");
            meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
            item.setItemMeta(meta);
            menu.setItem(11, item);
        }

        {
            ItemStack item = new ItemStack(Material.BOW);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fЛучник");
            meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
            item.setItemMeta(meta);
            menu.setItem(13, item);
        }

        {
            ItemStack item = new ItemStack(Material.DIAMOND_AXE);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fБерсерк");
            meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
            item.setItemMeta(meta);
            menu.setItem(15, item);
        }

        return menu;
    }

    private static Inventory setTime() {
        Inventory menu = LiteGame.server.createInventory(null, 27, "Время");

        {
            ItemStack item = new ItemStack(Material.STAINED_CLAY, 1, (short) 4);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fУтро");
            meta.setLore(Arrays.asList("§7Сделать утро."));
            item.setItemMeta(meta);
            menu.setItem(10, item);
        }

        {
            ItemStack item = new ItemStack(Material.STAINED_CLAY, 1, (short) 13);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fДень");
            meta.setLore(Arrays.asList("§7Сделать день."));
            item.setItemMeta(meta);
            menu.setItem(12, item);
        }

        {
            ItemStack item = new ItemStack(Material.STAINED_CLAY, 1, (short) 14);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fВечер");
            meta.setLore(Arrays.asList("§7Сделать вечер."));
            item.setItemMeta(meta);
            menu.setItem(14, item);
        }

        {
            ItemStack item = new ItemStack(Material.STAINED_CLAY, 1, (short) 11);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fНочь");
            meta.setLore(Arrays.asList("§7Сделать ночь."));
            item.setItemMeta(meta);
            menu.setItem(16, item);
        }

        return menu;
    }

    public static Inventory loginManager(String worldName) {
        Inventory menu = LiteGame.server.createInventory(null, 27, "Управление входом");

        ItemStack item0 = new ItemStack(Material.WOOD_DOOR);
        ItemMeta meta0 = item0.getItemMeta();
        meta0.setDisplayName("§fРазрешить всем входить");

        ItemStack item1 = new ItemStack(Material.IRON_DOOR);
        ItemMeta meta1 = item1.getItemMeta();
        meta1.setDisplayName("§fРазрешить входить только авторизированным");

        ItemStack item2 = new ItemStack(Material.NOTE_BLOCK);
        ItemMeta meta2 = item2.getItemMeta();
        meta2.setDisplayName("§fРазрешить входить только донатерам");

        ItemStack item3 = new ItemStack(Material.BARRIER);
        ItemMeta meta3 = item3.getItemMeta();
        meta3.setDisplayName("§fЗапретить всем входить");

        switch (WorldManager.getLogin(worldName)) {
            default: {
                meta0.addEnchant(Enchantment.LURE, 1, true);
                meta0.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                break;
            }
            case 1: {
                meta1.addEnchant(Enchantment.LURE, 1, true);
                meta1.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                break;
            }
            case 2: {
                meta2.addEnchant(Enchantment.LURE, 1, true);
                meta2.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                break;
            }
            case 3: {
                meta3.addEnchant(Enchantment.LURE, 1, true);
                meta3.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                break;
            }
        }

        item0.setItemMeta(meta0);
        menu.setItem(10, item0);
        item1.setItemMeta(meta1);
        menu.setItem(12, item1);
        item2.setItemMeta(meta2);
        menu.setItem(14, item2);
        item3.setItemMeta(meta3);
        menu.setItem(16, item3);

        return menu;
    }

    private static Inventory setMusic() {
        Inventory menu = LiteGame.server.createInventory(null, 54, "Музыка");

        menu.setItem(10, new ItemStack(Material.GREEN_RECORD));
        menu.setItem(11, new ItemStack(Material.RECORD_4));
        menu.setItem(12, new ItemStack(Material.RECORD_5));
        menu.setItem(13, new ItemStack(Material.RECORD_6));
        menu.setItem(14, new ItemStack(Material.RECORD_8));
        menu.setItem(15, new ItemStack(Material.RECORD_9));
        menu.setItem(16, new ItemStack(Material.RECORD_12));

        {
            ItemStack item = new ItemStack(Material.JUKEBOX);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§f§bФоновая музыка");
            meta.setLore(Arrays.asList("§7C418 - credits"));
            item.setItemMeta(meta);
            menu.setItem(19, item);
        }

        {
            ItemStack item = new ItemStack(Material.REDSTONE_BLOCK);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fВыключить музыку");
            item.setItemMeta(meta);
            menu.setItem(40, item);
        }

        return menu;
    }

    public static Inventory kickPlayer(World world) {
        Inventory menu = LiteGame.server.createInventory(null, 54, "Выгнать игрока");

        String worldName = world.getName();

        world.getPlayers().forEach(players ->
        {
            String playersNames = players.getName();
            if (!players.getUniqueId().toString().equals(worldName)) {
                ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
                SkullMeta meta = (SkullMeta) item.getItemMeta();
                meta.setOwner(playersNames);
                meta.setDisplayName(players.getDisplayName());
                meta.setLocalizedName(playersNames);
                item.setItemMeta(meta);
                menu.addItem(item);
            }
        });

        return menu;
    }

    public static Inventory buildSettings(World world) {
        Inventory menu = LiteGame.server.createInventory(null, 54, "Управление строительством");

        String worldName = world.getName();

        world.getPlayers().forEach(players ->
        {
            String playersNames = players.getName();
            if (!players.getUniqueId().toString().equals(worldName)) {
                ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
                SkullMeta meta = (SkullMeta) item.getItemMeta();
                meta.setOwner(playersNames);
                meta.setDisplayName(players.getDisplayName());
                meta.setLocalizedName(playersNames);
                meta.setLore
                        (
                                players.getGameMode() != GameMode.ADVENTURE
                                        ? allowedBuild
                                        : disallowedBuild
                        );
                item.setItemMeta(meta);
                menu.addItem(item);
            }
        });

        {
            ItemStack item = new ItemStack(Material.BRICK);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fСтроительство");
            meta.setLore
                    (
                            WorldManager.getAllowBuild(worldName)
                                    ? worldAllowedBuild
                                    : worldDisallowedBuild
                    );
            item.setItemMeta(meta);
            menu.setItem(53, item);
        }

        return menu;
    }

    public static Inventory flyingSettings(World world) {
        Inventory menu = LiteGame.server.createInventory(null, 54, "Управление полётом");

        String worldName = world.getName();

        world.getPlayers().forEach(players ->
        {
            String playersNames = players.getName();
            if (!players.getUniqueId().toString().equals(worldName)) {
                ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
                SkullMeta meta = (SkullMeta) item.getItemMeta();
                meta.setOwner(playersNames);
                meta.setDisplayName(players.getDisplayName());
                meta.setLocalizedName(playersNames);
                meta.setLore
                        (
                                players.getAllowFlight()
                                        ? allowedFlying
                                        : disallowedFlying
                        );
                item.setItemMeta(meta);
                menu.addItem(item);
            }
        });

        {
            ItemStack item = new ItemStack(Material.FEATHER);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fПолёт");
            meta.setLore
                    (
                            WorldManager.getAllowFlying(worldName)
                                    ? worldAllowedFlying
                                    : worldDisallowedFlying
                    );
            item.setItemMeta(meta);
            menu.setItem(53, item);
        }

        return menu;
    }

    private static Inventory setWeather() {
        Inventory menu = LiteGame.server.createInventory(null, 27, "Погода");

        {
            ItemStack item = new ItemStack(Material.BUCKET);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fСолнечно");
            meta.setLore(Arrays.asList("§7Солнечная погода"));
            item.setItemMeta(meta);
            menu.setItem(12, item);
        }

        {
            ItemStack item = new ItemStack(Material.WATER_BUCKET);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fДождливо");
            meta.setLore(Arrays.asList("§7Дождливая погода"));
            item.setItemMeta(meta);
            menu.setItem(14, item);
        }

        return menu;
    }
}