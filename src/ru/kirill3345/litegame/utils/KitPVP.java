package ru.kirill3345.litegame.utils;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.HumanEntity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public enum KitPVP {
    DEFAULT,
    ARCHER,
    BERSERKER;

    private static final ItemStack itemIronSword = itemIronSword();
    private static final ItemStack itemLeatherChestplate = itemLeatherChestplate();
    private static final ItemStack itemWoodSword = itemWoodSword();
    private static final ItemStack itemBow = itemBow();
    private static final ItemStack itemArrow = new ItemStack(Material.ARROW);
    private static final ItemStack itemDiamondAxe = itemDiamondAxe();

    public static void join(HumanEntity player, KitPVP kit) {
        PlayerInventory inventory = player.getInventory();
        switch (kit) {
            case DEFAULT: {
                inventory.setItem(0, itemIronSword);
                inventory.setChestplate(itemLeatherChestplate);
                break;
            }
            case ARCHER: {
                inventory.setItem(0, itemWoodSword);
                inventory.setChestplate(itemLeatherChestplate);
                inventory.setItemInOffHand(itemBow);
                inventory.setItem(1, itemArrow);
                break;
            }
            case BERSERKER: {
                inventory.setItem(0, itemDiamondAxe);
                break;
            }
        }
        player.teleport(player.getWorld().getSpawnLocation());
        player.setNoDamageTicks(50);
        player.setGameMode(GameMode.ADVENTURE);
    }

    private static ItemStack itemIronSword() {
        ItemStack item = new ItemStack(Material.IRON_SWORD);
        ItemMeta meta = item.getItemMeta();
        meta.setUnbreakable(true);
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack itemLeatherChestplate() {
        ItemStack item = new ItemStack(Material.LEATHER_CHESTPLATE);
        ItemMeta meta = item.getItemMeta();
        meta.setUnbreakable(true);
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack itemWoodSword() {
        ItemStack item = new ItemStack(Material.WOOD_SWORD);
        ItemMeta meta = item.getItemMeta();
        meta.setUnbreakable(true);
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack itemBow() {
        ItemStack item = new ItemStack(Material.BOW);
        ItemMeta meta = item.getItemMeta();
        meta.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
        meta.setUnbreakable(true);
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack itemDiamondAxe() {
        ItemStack item = new ItemStack(Material.DIAMOND_AXE);
        ItemMeta meta = item.getItemMeta();
        meta.setUnbreakable(true);
        item.setItemMeta(meta);
        return item;
    }
}