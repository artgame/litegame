package ru.kirill3345.litegame.utils;

import com.destroystokyo.paper.Title;

public class Titles {
    public static Title death = new Title("", "Вы умерли", 5, 50, 5);
    public static Title kicked = new Title("", "Вас выгнали из мира", 5, 50, 5);
    public static Title needDonate = new Title("", "Доступно только донатерам!", 5, 50, 5);
    public static Title notAllowedBuilding = new Title("", "Вам не разрешено строить", 5, 50, 5);
    public static Title worldClosed = new Title("", "Мир закрыт администратором", 5, 50, 5);
    public static Title gameStarted = new Title("", "Игра началась!", 5, 50, 5);
    public static Title questCompleted = new Title("", "Задание выполнено!", 5, 50, 5);
    public static Title stackingDisabled = new Title("", "У вас выключен стакинг.", 5, 50, 5);
    public static Title victimStackingDisabled = new Title("", "У этого игрока выключен стакинг.", 5, 50, 5);
}