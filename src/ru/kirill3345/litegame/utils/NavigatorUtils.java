package ru.kirill3345.litegame.utils;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.kirill3345.litegame.LiteGame;
import ru.kirill3345.litegame.utils.world.Hub;
import ru.kirill3345.litegame.utils.world.WorldManager;

public final class NavigatorUtils {
    public static ItemStack[] defaultContents;
    public static Inventory defaultMenu = LiteGame.server.createInventory(null, 63, "Меню");
    public static Inventory buildingMenu = LiteGame.server.createInventory(null, 63, "Building");
    public static Inventory pvpMenu = LiteGame.server.createInventory(null, 63, "PVP");
    public static Inventory parkourMenu = LiteGame.server.createInventory(null, 63, "Parkour");
    public static Inventory spleggMenu = LiteGame.server.createInventory(null, 63, "Splegg");
    public static Inventory throwOutMenu = LiteGame.server.createInventory(null, 63, "ThrowOut");
    public static Inventory duelMenu = LiteGame.server.createInventory(null, 63, "Duel");
    public static Inventory skyBlockMenu = LiteGame.server.createInventory(null, 63, "SkyBlock");
    public static Inventory murderMenu = LiteGame.server.createInventory(null, 63, "Murder");
    public static Inventory paintBallMenu = LiteGame.server.createInventory(null, 63, "Paintball");
    public static Inventory tntRunMenu = LiteGame.server.createInventory(null, 63, "TNTRun");
    public static Inventory kitPvpMenu = LiteGame.server.createInventory(null, 63, "KitPVP");
    public static Inventory skyWarsMenu = LiteGame.server.createInventory(null, 63, "SkyWars");
    public static Inventory survivalGamesMenu = LiteGame.server.createInventory(null, 63, "SurvivalGames");
    public static Inventory artStudioMenu = LiteGame.server.createInventory(null, 63, "ArtStudio");
    public static Inventory gameMenu = LiteGame.server.createInventory(null, 63, "Game");

    static {
        ItemStack glass = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
        ItemMeta glassMeta = glass.getItemMeta();
        glassMeta.setDisplayName("§r");
        glass.setItemMeta(glassMeta);
        NavigatorUtils.defaultMenu.setItem(54, glass);
        {
            ItemStack item = new ItemStack(Material.MAP);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fМой мир");
            item.setItemMeta(meta);
            NavigatorUtils.defaultMenu.setItem(55, item);
        }
        {
            ItemStack item = new ItemStack(Material.MAGMA_CREAM);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fХаб");
            item.setItemMeta(meta);
            NavigatorUtils.defaultMenu.setItem(56, item);
        }
        NavigatorUtils.defaultMenu.setItem(57, glass);
        NavigatorUtils.defaultMenu.setItem(58, glass);
        NavigatorUtils.defaultMenu.setItem(59, glass);
        NavigatorUtils.defaultMenu.setItem(60, glass);
        {
            ItemStack item = new ItemStack(Material.ARROW);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fМеню");
            item.setItemMeta(meta);
            NavigatorUtils.defaultMenu.setItem(61, item);
        }
        NavigatorUtils.defaultMenu.setItem(62, glass);
        defaultContents = NavigatorUtils.defaultMenu.getContents();
        {
            {
                ItemStack item = new ItemStack(Material.BRICK);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§fBuilding");
                meta.setLore(Arrays.asList("§7Здесь создаются шедевры."));
                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                item.setItemMeta(meta);
                NavigatorUtils.defaultMenu.setItem(10, item);
            }

            {
                ItemStack item = new ItemStack(Material.IRON_SWORD);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§fPVP");
                meta.setLore(Arrays.asList("§7Классическая PVP-арена."));
                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                item.setItemMeta(meta);
                NavigatorUtils.defaultMenu.setItem(11, item);
            }

            {
                ItemStack item = new ItemStack(Material.LEATHER_BOOTS);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§fParkour");
                meta.setLore(Arrays.asList("§7Игрок должен добраться до", "§7и ударить кристалл Энда."));
                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                item.setItemMeta(meta);
                NavigatorUtils.defaultMenu.setItem(12, item);
            }

            {
                ItemStack item = new ItemStack(Material.EGG);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§fSplegg");
                meta.setLore(Arrays.asList("§7Сломай под врагом", "§7блок!"));
                item.setItemMeta(meta);
                NavigatorUtils.defaultMenu.setItem(13, item);
            }

            {
                ItemStack item = new ItemStack(Material.ICE);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§fThrowOut");
                meta.setLore(Arrays.asList("§7Скинь врага!"));
                item.setItemMeta(meta);
                NavigatorUtils.defaultMenu.setItem(14, item);
            }

            {
                ItemStack item = new ItemStack(Material.WOOD_SWORD);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§fDuel");
                meta.setLore(Arrays.asList("§7Битва один на один."));
                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                item.setItemMeta(meta);
                NavigatorUtils.defaultMenu.setItem(19, item);
            }

            {
                ItemStack item = new ItemStack(Material.SAPLING);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§fSkyBlock");
                meta.setLore(Arrays.asList("§7Выживание на острове."));
                item.setItemMeta(meta);
                NavigatorUtils.defaultMenu.setItem(20, item);
            }

            {
                ItemStack item = new ItemStack(Material.REDSTONE);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§fMurder");
                meta.setLore(Arrays.asList("§7Один против всех."));
                item.setItemMeta(meta);
                NavigatorUtils.defaultMenu.setItem(21, item);
            }

            {
                ItemStack item = new ItemStack(Material.DIAMOND_BARDING);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§fPaintball");
                meta.setLore(Arrays.asList("§7Разукрась врага!"));
                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                item.setItemMeta(meta);
                NavigatorUtils.defaultMenu.setItem(22, item);
            }

            {
                ItemStack item = new ItemStack(Material.TNT);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§fTNTRun");
                meta.setLore(Arrays.asList("§7Блоки падают под тобой!"));
                item.setItemMeta(meta);
                NavigatorUtils.defaultMenu.setItem(23, item);
            }

            {
                ItemStack item = new ItemStack(Material.SHIELD);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§fKitPVP");
                meta.setLore(Arrays.asList("§7PVP с наборами."));
                item.setItemMeta(meta);
                NavigatorUtils.defaultMenu.setItem(28, item);
            }

            {
                ItemStack item = new ItemStack(Material.ENDER_PEARL);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§fSkyWars");
                meta.setLore(Arrays.asList("§7PVP в небесах."));
                item.setItemMeta(meta);
                NavigatorUtils.defaultMenu.setItem(29, item);
            }

            {
                ItemStack item = new ItemStack(Material.CHEST);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§fSurvivalGames");
                meta.setLore(Arrays.asList("§7Классическая мини-игра."));
                item.setItemMeta(meta);
                NavigatorUtils.defaultMenu.setItem(30, item);
            }

            {
                ItemStack item = new ItemStack(Material.PAINTING);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§fArtStudio");
                meta.setLore(Arrays.asList("§72D-строительство."));
                item.setItemMeta(meta);
                NavigatorUtils.defaultMenu.setItem(31, item);
            }

            {
                ItemStack item = new ItemStack(Material.DIAMOND);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§fGame");
                meta.setLore(Arrays.asList("§7Уникальные игры, которые", "§7пишут игроки."));
                item.setItemMeta(meta);
                NavigatorUtils.defaultMenu.setItem(32, item);
            }

            {
                ItemStack item = new ItemStack(Material.WORKBENCH);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§fРеалистичное выживание");
                meta.setLore(Arrays.asList("§7- Хардкорность", "§7- Новая система строительства", "§7- Жажда"));
                meta.addEnchant(Enchantment.LURE, 0, true);
                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                item.setItemMeta(meta);
                NavigatorUtils.defaultMenu.setItem(16, item);
            }

            {
                ItemStack item = new ItemStack(Material.BED);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§fBedWars");
                meta.setLore(Arrays.asList("§fАрена 1", "§7Hosted by XjCyan1de"));
                item.setItemMeta(meta);
                NavigatorUtils.defaultMenu.setItem(25, item);
            }

            {
                ItemStack item = new ItemStack(Material.BED);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§fBedWars");
                meta.setLore(Arrays.asList("§fАрена 2", "§7Hosted by XjCyan1de"));
                item.setItemMeta(meta);
                NavigatorUtils.defaultMenu.setItem(34, item);
            }

            {
                ItemStack item = new ItemStack(Material.COMMAND);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§fТест-сервер");
                meta.setLore(Arrays.asList("§7..."));
                item.setItemMeta(meta);
                NavigatorUtils.defaultMenu.setItem(43, item);
            }
        }
        updateMenus();
    }

    public static void updateMenus() {
        buildingMenu.setContents(defaultContents);
        pvpMenu.setContents(defaultContents);
        parkourMenu.setContents(defaultContents);
        spleggMenu.setContents(defaultContents);
        throwOutMenu.setContents(defaultContents);
        duelMenu.setContents(defaultContents);
        skyBlockMenu.setContents(defaultContents);
        murderMenu.setContents(defaultContents);
        paintBallMenu.setContents(defaultContents);
        tntRunMenu.setContents(defaultContents);
        kitPvpMenu.setContents(defaultContents);
        skyWarsMenu.setContents(defaultContents);
        survivalGamesMenu.setContents(defaultContents);
        artStudioMenu.setContents(defaultContents);
        gameMenu.setContents(defaultContents);
        for (String worldsNames : WorldManager.getKeys()) {
            switch (WorldManager.getType(worldsNames)) {
                case 1: {
                    int logins = WorldManager.getLogin(worldsNames);
                    if (logins != 3)
                        buildingMenu.addItem(buildLoginableItem(worldsNames, logins));
                    break;
                }
                case 2: {
                    pvpMenu.addItem(buildItem(worldsNames));
                    break;
                }
                case 3: {
                    parkourMenu.addItem(buildItem(worldsNames));
                    break;
                }
                case 4: {
                    if (!WorldManager.isStarted(worldsNames))
                        spleggMenu.addItem(buildItem(worldsNames));
                    break;
                }
                case 5: {
                    if (!WorldManager.isStarted(worldsNames))
                        throwOutMenu.addItem(buildItem(worldsNames));
                    break;
                }
                case 6: {
                    if (!WorldManager.isStarted(worldsNames))
                        duelMenu.addItem(buildItem(worldsNames));
                    break;
                }
                case 7: {
                    if (LiteGame.server.getWorld(worldsNames) != null) {
                        int logins = WorldManager.getLogin(worldsNames);
                        if (logins != 3)
                            skyBlockMenu.addItem(buildLoginableItem(worldsNames, logins));
                    }
                    break;
                }
                case 8: {
                    if (!WorldManager.isStarted(worldsNames))
                        murderMenu.addItem(buildItem(worldsNames));
                    break;
                }
                case 10: {
                    if (!WorldManager.isStarted(worldsNames))
                        paintBallMenu.addItem(buildItem(worldsNames));
                    break;
                }
                case 11: {
                    if (!WorldManager.isStarted(worldsNames))
                        tntRunMenu.addItem(buildItem(worldsNames));
                    break;
                }
                case 12: {
                    if (!WorldManager.isStarted(worldsNames))
                        kitPvpMenu.addItem(buildItem(worldsNames));
                    break;
                }
                case 13: {
                    if (!WorldManager.isStarted(worldsNames))
                        skyWarsMenu.addItem(buildItem(worldsNames));
                    break;
                }
                case 14: {
                    if (!WorldManager.isStarted(worldsNames))
                        survivalGamesMenu.addItem(buildItem(worldsNames));
                    break;
                }
                case 17: {
                    if (LiteGame.server.getWorld(worldsNames) != null) {
                        int logins = WorldManager.getLogin(worldsNames);
                        if (logins != 3)
                            artStudioMenu.addItem(buildLoginableItem(worldsNames, logins));
                    }
                    break;
                }
                case 18: {
                    if (!WorldManager.isStarted(worldsNames))
                        gameMenu.addItem(buildItem(worldsNames));
                    break;
                }
            }
        }
    }

    public static void updateMenu(int type) {
        if (type == 0)
            return;
        Inventory menu = getMenuByType(type);
        menu.setContents(defaultContents);
        for (String worldsNames : WorldManager.getKeys()) {
            if (WorldManager.getType(worldsNames) == type) {
                switch (type) {
                    case 0:
                        break;
                    case 1:
                    case 7:
                    case 17: {
                        if (LiteGame.server.getWorld(worldsNames) != null) {
                            int logins = WorldManager.getLogin(worldsNames);
                            if (logins != 3)
                                menu.addItem(buildLoginableItem(worldsNames, logins));
                        }
                        break;
                    }
                    default: {
                        if (!WorldManager.isStarted(worldsNames))
                            menu.addItem(buildItem(worldsNames));
                        break;
                    }
                }
            }
        }
    }

    public static void clickDefault(InventoryClickEvent event) {
        Inventory inventory = event.getInventory();
        event.setCancelled(true);
        if (event.getClickedInventory() != inventory)
            return;
        switch (event.getSlot()) {
            case 10: {
                event.getWhoClicked().openInventory(buildingMenu);
                break;
            }
            case 11: {
                event.getWhoClicked().openInventory(pvpMenu);
                break;
            }
            case 12: {
                event.getWhoClicked().openInventory(parkourMenu);
                break;
            }
            case 13: {
                event.getWhoClicked().openInventory(spleggMenu);
                break;
            }
            case 14: {
                event.getWhoClicked().openInventory(throwOutMenu);
                break;
            }
            case 19: {
                event.getWhoClicked().openInventory(duelMenu);
                break;
            }
            case 20: {
                event.getWhoClicked().openInventory(skyBlockMenu);
                break;
            }
            case 21: {
                event.getWhoClicked().openInventory(murderMenu);
                break;
            }
            case 22: {
                event.getWhoClicked().openInventory(paintBallMenu);
                break;
            }
            case 23: {
                event.getWhoClicked().openInventory(tntRunMenu);
                break;
            }
            case 28: {
                event.getWhoClicked().openInventory(kitPvpMenu);
                break;
            }
            case 29: {
                event.getWhoClicked().openInventory(skyWarsMenu);
                break;
            }
            case 30: {
                event.getWhoClicked().openInventory(survivalGamesMenu);
                break;
            }
            case 31: {
                event.getWhoClicked().openInventory(artStudioMenu);
                break;
            }
            case 32: {
                event.getWhoClicked().openInventory(gameMenu);
                break;
            }
            case 16: {
                Player player = (Player) event.getWhoClicked();
                if (PlayersUtils.isLoggined(player.getUniqueId().toString()))
                    BungeeUtils.connect(player, "Survival");
                else
                    player.sendMessage("Вы не вошли в аккаунт. Используйте команду§7 /pass");
                break;
            }
            case 25: {
                Player player = (Player) event.getWhoClicked();
                BungeeUtils.connect(player, "BedWars-1");
                break;
            }
            case 34: {
                Player player = (Player) event.getWhoClicked();
                BungeeUtils.connect(player, "BedWars-2");
                break;
            }
            case 43: {
                Player player = (Player) event.getWhoClicked();
                String uuid = player.getUniqueId().toString();
                if (PlayersUtils.isLoggined(uuid)) {
                    if (PlayersUtils.isDonator(uuid))
                        BungeeUtils.connect(player, "Test");
                    else
                        player.sendMessage("Нужен донат.");
                } else
                    player.sendMessage("Вы не вошли в аккаунт. Используйте команду§7 /pass");
                break;
            }
            case 55: {
                HumanEntity player = event.getWhoClicked();
                player.openInventory(generateMyWorldMenu(player));
                break;
            }
            case 56: {
                event.getWhoClicked().teleport(Hub.location);
                break;
            }
        }
    }

    public static void clickArena(InventoryClickEvent event) {
        Inventory inventory = event.getInventory();
        event.setCancelled(true);
        if (event.getClickedInventory() != inventory)
            return;
        ItemStack currentItem = event.getCurrentItem();
        switch (currentItem.getType()) {
            case STAINED_CLAY: {
                String worldName = currentItem.getItemMeta().getLocalizedName();
                World world = LiteGame.server.getWorld(worldName);
                if (world == null) {
                    world = WorldManager.createGame(worldName, WorldManager.getType(worldName));
                    event.getWhoClicked().teleport(world.getSpawnLocation());
                } else {
                    HumanEntity player = event.getWhoClicked();
                    if (player.getWorld() != world)
                        event.getWhoClicked().teleport(world.getSpawnLocation());
                }
                break;
            }
            case MAP: {
                HumanEntity player = event.getWhoClicked();
                player.openInventory(generateMyWorldMenu(player));
                break;
            }
            case MAGMA_CREAM: {
                event.getWhoClicked().teleport(Hub.location);
                break;
            }
            case ARROW: {
                event.getWhoClicked().openInventory(defaultMenu);
                break;
            }
        }
    }

    public static void clickLoginable(InventoryClickEvent event) {
        Inventory inventory = event.getInventory();
        event.setCancelled(true);
        if (event.getClickedInventory() != inventory)
            return;
        ItemStack currentItem = event.getCurrentItem();
        switch (currentItem.getType()) {
            case STAINED_CLAY: {
                switch (currentItem.getDurability()) {
                    default: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        World world = LiteGame.server.getWorld(currentItem.getItemMeta().getLocalizedName());
                        if (player.getWorld() != world)
                            event.getWhoClicked().teleport(world.getSpawnLocation());
                        break;
                    }
                    case 9: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        if (PlayersUtils.isLoggined(player.getUniqueId().toString())) {
                            World world = LiteGame.server.getWorld(currentItem.getItemMeta().getLocalizedName());
                            if (player.getWorld() != world)
                                event.getWhoClicked().teleport(world.getSpawnLocation());
                        } else
                            player.sendMessage("Вы не вошли в аккаунт. Используйте команду§7 /pass");
                        break;
                    }
                    case 3: {
                        HumanEntity player = event.getWhoClicked();
                        String uuid = player.getUniqueId().toString();
                        player.closeInventory();
                        if (PlayersUtils.isLoggined(uuid)) {
                            if (PlayersUtils.isDonator(uuid)) {
                                World world = LiteGame.server.getWorld(currentItem.getItemMeta().getLocalizedName());
                                if (player.getWorld() != world)
                                    event.getWhoClicked().teleport(world.getSpawnLocation());
                            } else
                                player.sendMessage("Нужен донат.");
                        } else
                            player.sendMessage("Вы не вошли в аккаунт. Используйте команду§7 /pass");
                        break;
                    }
                }
                break;
            }
            case MAP: {
                HumanEntity player = event.getWhoClicked();
                player.openInventory(generateMyWorldMenu(player));
                break;
            }
            case MAGMA_CREAM: {
                event.getWhoClicked().teleport(Hub.location);
                break;
            }
            case ARROW: {
                event.getWhoClicked().openInventory(defaultMenu);
                break;
            }
        }
    }

    public static void clickSkyBlock(InventoryClickEvent event) {
        Inventory inventory = event.getInventory();
        if (event.getClickedInventory() != inventory)
            return;
        ItemStack currentItem = event.getCurrentItem();
        event.setCancelled(true);
        switch (currentItem.getType()) {
            case STAINED_CLAY: {
                switch (WorldManager.getType(String.valueOf(currentItem.getDurability()))) {
                    default: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        if (WorldManager.getType(player.getUniqueId().toString()) == 7) {
                            World world = LiteGame.server.getWorld(currentItem.getItemMeta().getLocalizedName());
                            if (player.getWorld() != world)
                                event.getWhoClicked().teleport(world.getSpawnLocation());
                        } else
                            player.sendMessage("У вас нет острова.");
                        break;
                    }
                    case 9: {
                        HumanEntity player = event.getWhoClicked();
                        String uuid = player.getUniqueId().toString();
                        player.closeInventory();
                        if (PlayersUtils.isLoggined(player.getUniqueId().toString())) {
                            if (WorldManager.getType(uuid) == 7) {
                                World world = LiteGame.server.getWorld(currentItem.getItemMeta().getLocalizedName());
                                if (player.getWorld() != world)
                                    event.getWhoClicked().teleport(world.getSpawnLocation());
                            } else
                                player.sendMessage("У вас нет острова.");
                        } else
                            player.sendMessage("Вы не вошли в аккаунт. Используйте команду§7 /pass");
                        break;
                    }
                    case 3: {
                        HumanEntity player = event.getWhoClicked();
                        String uuid = player.getUniqueId().toString();
                        player.closeInventory();
                        if (PlayersUtils.isLoggined(uuid)) {
                            if (PlayersUtils.isDonator(uuid)) {
                                if (WorldManager.getType(uuid) == 7) {
                                    World world = LiteGame.server.getWorld(currentItem.getItemMeta().getLocalizedName());
                                    if (player.getWorld() != world)
                                        event.getWhoClicked().teleport(world.getSpawnLocation());
                                } else
                                    player.sendMessage("У вас нет острова.");
                            } else
                                player.sendMessage("Нужен донат.");
                        } else
                            player.sendMessage("Вы не вошли в аккаунт. Используйте команду§7 /pass");
                        break;
                    }
                }
                break;
            }
            case MAP: {
                HumanEntity player = event.getWhoClicked();
                player.openInventory(generateMyWorldMenu(player));
                break;
            }
            case MAGMA_CREAM: {
                event.getWhoClicked().teleport(Hub.location);
                break;
            }
            case ARROW: {
                event.getWhoClicked().openInventory(defaultMenu);
                break;
            }
        }
    }

    public static ItemStack buildLoginableItem(String worldName, int login) {
        ItemStack item;
        World world = LiteGame.server.getWorld(worldName);
        if (world != null)
            item = new ItemStack(Material.STAINED_CLAY, !world.getPlayers().isEmpty() ? world.getPlayerCount() : 1, getDataByLogin(login));
        else
            item = new ItemStack(Material.STAINED_CLAY, 1, (short) 5);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(WorldManager.getDisplayName(worldName));
        meta.setLocalizedName(worldName);
        if (PlayersUtils.isDonator(worldName)) {
            meta.addEnchant(Enchantment.LURE, 0, true);
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        item.setItemMeta(meta);
        return item;
    }


    public static Inventory getMenuByType(int type) {
        switch (type) {
            default:
                return defaultMenu;
            case 1:
                return buildingMenu;
            case 2:
                return pvpMenu;
            case 3:
                return parkourMenu;
            case 4:
                return spleggMenu;
            case 5:
                return throwOutMenu;
            case 6:
                return duelMenu;
            case 7:
                return skyBlockMenu;
            case 8:
                return murderMenu;
            case 10:
                return paintBallMenu;
            case 11:
                return tntRunMenu;
            case 12:
                return kitPvpMenu;
            case 13:
                return skyWarsMenu;
            case 14:
                return survivalGamesMenu;
            case 17:
                return artStudioMenu;
            case 18:
                return gameMenu;
        }
    }

    public static short getDataByLogin(int login) {
        switch (login) {
            default:
                return 13;
            case 1:
                return 9;
            case 2:
                return 3;
        }
    }

    public static ItemStack buildItem(String worldName) {
        ItemStack item;
        World world = LiteGame.server.getWorld(worldName);
        if (world != null)
            item = new ItemStack(Material.STAINED_CLAY, !world.getPlayers().isEmpty() ? world.getPlayerCount() : 1, (short) 13);
        else
            item = new ItemStack(Material.STAINED_CLAY, 1, (short) 5);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(WorldManager.getDisplayName(worldName));
        meta.setLocalizedName(worldName);
        if (PlayersUtils.isDonator(worldName)) {
            meta.addEnchant(Enchantment.LURE, 0, true);
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        item.setItemMeta(meta);
        return item;
    }

    public static Inventory generateMyWorldMenu(HumanEntity player) {
        String uuid = player.getUniqueId().toString();
        if (PlayersUtils.isLoggined(player.getUniqueId().toString())) {
            if (WorldManager.getWorldFolder(uuid).isDirectory()) {
                switch (WorldManager.getType(uuid)) {
                    case 0:
                    case 1:
                        return !player.getWorld().getName().equals(uuid) ? Menus.myWorldDefault : Menus.worldSettings;
                    default:
                        return Menus.myWorldArena;
                    case 7:
                        return !player.getWorld().getName().equals(uuid) ? Menus.myWorldSkyBlock : Menus.skyBlock;
                    case 17:
                        return !player.getWorld().getName().equals(uuid) ? Menus.myWorldArtStudio : ArtStudioUtils.worldMenu;
                }
            } else
                return Menus.myWorldCreate;
        } else
            return Menus.needLogin;
    }
}