package ru.kirill3345.litegame.utils;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import ru.kirill3345.litegame.LiteGame;
import ru.kirill3345.litegame.utils.world.WorldManager;

public class GameplayUtils {
    public static void giveHubItems(PlayerInventory inventory) {
        inventory.setItem(1, Items.menu);
        inventory.setItem(2, Items.gadgets);
        inventory.setItem(4, randomGadget());
        inventory.setItem(6, Items.settings);
        inventory.setItem(7, Items.info);
        inventory.setChestplate(Items.unbreakableElytra);
    }

    public static ItemStack randomGadget() {
        switch (LiteGame.random.nextInt(7)) {
            default:
                return Items.enderPearlGadget;
            case 1:
                return Items.fireworkGadget;
            case 2:
                return Items.snowBallGun;
            case 3:
                return Items.tntGadget;
            case 4:
                return Items.arrowGadget;
            case 5:
                return Items.enderRodGadget;
            case 6:
                return Items.dragonSkullGadget;
        }
    }

    public static World setArenaType(String worldName, int type) {
        World world = LiteGame.server.getWorld(worldName);
        WorldManager.setType(worldName, type);
        if (world != null)
            LiteGame.server.unloadWorld(world, false);
        return WorldManager.createGame(worldName, type);
    }

    public static void runPlateEvent(Block block) {
        World world = block.getWorld();
        String worldName = world.getName();
        if (WorldManager.getType(worldName) != 11) {
            Location blockLocation = block.getLocation().add(0.5, 0, 0.5);
            world.getNearbyEntities(blockLocation.clone().add(0, 0.5, 0), 0.5, 0.5, 0.5)
                    .forEach(entities -> entities.setVelocity(entities.getLocation().getDirection().multiply(1).setY(1)));
            world.playSound(blockLocation, Sound.ENTITY_ENDERDRAGON_FLAP, 1, 1);
        } else if (WorldManager.isStarted(worldName)) {
            LiteGame.server.getScheduler().scheduleSyncDelayedTask(LiteGame.plugin, () ->
            {
                block.setType(Material.AIR);
                block.getRelative(0, -1, 0).setType(Material.AIR);
            }, 10);
        }
    }

    public static void skyWarsInventoryOpenEvent(InventoryOpenEvent event) {
        Inventory inventory = event.getInventory();
        switch (inventory.getType()) {
            case ENDER_CHEST:
            case ENCHANTING:
            case ANVIL:
            case BEACON:
            case BREWING: {
                event.setCancelled(true);
                break;
            }
            case CHEST: {
                if (inventory.getHolder() != null) {
                    Location location = inventory.getLocation().add(0.5, 0.5, 0.5);
                    event.setCancelled(true);
                    if (WorldManager.isStarted(location.getWorld().getName())) {
                        location.getBlock().setType(Material.AIR);
                        location.getWorld().dropItemNaturally(location, Items.skyWarsDrops[LiteGame.random.nextInt(Items.skyWarsDrops.length)]);
                    }
                }
                break;
            }
            default: {
                if (!WorldManager.isStarted(event.getPlayer().getWorld().getName()))
                    event.setCancelled(true);
                break;
            }
        }
    }

    public static void runWorldTabPlayers(Player player) {
        World playerWorld = player.getWorld();
        LiteGame.server.getOnlinePlayers().forEach(players ->
        {
            if (players.getWorld() == playerWorld) {
                player.showPlayer(players);
                players.showPlayer(player);
            } else {
                player.hidePlayer(players);
                players.hidePlayer(player);
            }
        });
    }

    public static void equipArmor(Player player, EquipmentSlot slot, ItemStack item, EquipmentSlot hand, Sound Sound) {
        if (player.getCooldown(item.getType()) == 0) {
            PlayerInventory inventory = player.getInventory();
            ItemStack itemInSlot = inventory.getItem(slot);
            inventory.setItem(hand, itemInSlot);
            inventory.setItem(slot, item);
            if (itemInSlot != null)
                player.setCooldown(itemInSlot.getType(), 15);
            player.getWorld().playSound(player.getEyeLocation(), Sound, 1, 1);
        }
    }
}