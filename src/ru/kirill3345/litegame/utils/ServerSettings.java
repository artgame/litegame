package ru.kirill3345.litegame.utils;

import com.destroystokyo.paper.Title;

import java.io.File;
import java.util.TimeZone;

import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.configuration.file.YamlConfiguration;
import ru.kirill3345.litegame.LiteGame;

public class ServerSettings {
    private static final YamlConfiguration config = YamlConfiguration.loadConfiguration(new File(LiteGame.dataFolder, "config.yml"));
    public static String motd = config.getString("motd", LiteGame.server.getMotd());
    public static TextComponent tabHeader = new TextComponent(config.getString("tabHeader", LiteGame.server.getServerName()));
    public static Title joinTitle = new Title(config.getString("titles.join.title", ""), config.getString("titles.join.subtitle", ""), 5, 50, 5);
    public static TimeZone timeZone = TimeZone.getTimeZone(config.getString("timeZone", "Europe/Moscow"));
    public static String unknowCommand = LiteGame.server.spigot().getSpigotConfig().getString("messages.unknown-command", "");
    public static boolean antiCheatFlyHack = config.getBoolean("antiCheat.flyHack", LiteGame.server.getAllowFlight());
    public static String vkLink = config.getString("messages.vkLink", "");
    public static String donateLink = config.getString("messages.donateLink", "");
}