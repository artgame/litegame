package ru.kirill3345.litegame.utils;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.bukkit.entity.Player;
import ru.kirill3345.litegame.LiteGame;

public class BungeeUtils {
    static {
        LiteGame.server.getMessenger().registerOutgoingPluginChannel(LiteGame.plugin, "BungeeCord");
    }

    public static void connect(Player player, String server) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(server);
        player.sendPluginMessage(LiteGame.plugin, "BungeeCord", out.toByteArray());
    }
}