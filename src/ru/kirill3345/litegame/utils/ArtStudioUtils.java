package ru.kirill3345.litegame.utils;

import com.google.common.collect.Maps;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import ru.kirill3345.litegame.LiteGame;
import ru.kirill3345.litegame.utils.world.WorldManager;

public class ArtStudioUtils {
    private static final File artsFile = new File(LiteGame.dataFolder, "arts.yml");
    private static final YamlConfiguration arts = YamlConfiguration.loadConfiguration(artsFile);
    private static final HashMap<String, Integer> currentArts = Maps.newHashMap();
    public static ItemStack brushItem = new ItemStack(Material.WOOL);
    public static ItemStack menuItem = menuItem();
    public static Inventory worldMenu = worldMenu();
    public static Inventory selectGeneratorMenu = selectGeneratorMenu();
    private static final byte nullByte = 0;
    private static final HashMap<String, Inventory> artsMenus = Maps.newHashMap();
    public static Inventory selectColorMenu = selectColorMenu();
    private static final List<String> artsLore = Arrays.asList
            (
                    "§7ЛКМ - Установить кадр",
                    "§7ПКМ - Сохранить кадр",
                    "§7Shift - Удалить кадр"
            );
    private static final List<String> allowedDraw = Arrays.asList("§aРазрешено рисовать");
    private static final List<String> disallowedDraw = Arrays.asList("§cЗапрещено рисовать");
    private static final List<String> worldAllowedDraw = Arrays.asList
            (
                    "§aНовые игроки",
                    "§aмогут рисовать."
            );
    private static final List<String> worldDisallowedDraw = Arrays.asList
            (
                    "§cНовые игроки не",
                    "§cмогут рисовать."
            );

    public static void loadArt(int art, World world) {
        String worldName = world.getName();
        if (arts.contains(worldName + "." + art)) {
            List<String> legacyArt = arts.getStringList(worldName + "." + art);
            if (!legacyArt.isEmpty()) {
                for (String blocks : legacyArt) {
                    String[] split = blocks.split("#", 3);
                    world.getBlockAt
                            (
                                    Integer.valueOf(split[0]),
                                    Integer.valueOf(split[1]),
                                    16
                            ).setData(Byte.valueOf(split[2]));
                }
                ChatUtils.broadcastMessage
                        (
                                "Внимание! Кадр " + art + " имеет устаревший формат данных,"
                                        + "\n" +
                                        "сохраните его, чтобы конвертировать в новый формат.",
                                world.getPlayers()
                        );
                return;
            }
            for (int x = 23; x >= -24; x--) {
                for (int y = 25; y >= 0; y--)
                    world.getBlockAt(x, y, 16).setData((byte) arts.getInt(worldName + "." + art + "." + x + "." + y, 0));
            }
        } else
            clearArt(world);
        currentArts.put(worldName, art);
        updateArtMenu(worldName);
    }

    public static void saveArt(String art, World world) {
        String worldName = world.getName();
        arts.set(worldName + "." + art, null);
        for (int x = 23; x >= -24; x--) {
            for (int y = 25; y >= 0; y--) {
                byte data = world.getBlockAt(x, y, 16).getData();
                arts.set(worldName + "." + art + "." + x + "." + y, data != 0 ? data : null);
            }
        }
        updateArtMenu(worldName);
    }

    public static void clearArt(World world) {
        for (int x = 23; x >= -24; x--) {
            for (int y = 25; y >= 0; y--)
                world.getBlockAt(x, y, 16).setData(nullByte);
        }
    }

    public static void loadBackArt(World world) {
        int art = ArtStudioUtils.currentArts.getOrDefault(world.getName(), -1);
        if (art != 0)
            art--;
        else
            art = 54;
        ArtStudioUtils.loadArt(art, world);
    }

    public static void loadNextArt(World world) {
        int art = ArtStudioUtils.currentArts.getOrDefault(world.getName(), -1);
        if (art != 54)
            art++;
        else
            art = 0;
        ArtStudioUtils.loadArt(art, world);
    }

    public static void changeMaterial(World world, Material material) {
        for (int x = 23; x >= -24; x--) {
            for (int y = 25; y >= 0; y--)
                world.getBlockAt(x, y, 16).setType(material);
        }
        world.setAutoSave(true);
        world.save();
        world.setAutoSave(false);
    }

    public static void removeArt(String art, String world) {
        arts.set(world + "." + art, null);
        updateArtMenu(world);
    }

    public static void removeArts(String world) {
        arts.set(world, null);
    }

    public static void updateArtMenu(String world) {
        Inventory menu = getArtMenu(world);
        int currentArt = currentArts.getOrDefault(world, -1);
        for (int ids = 0; ids < 54; ids++) {
            ItemStack items = new ItemStack(arts.contains(world + "." + ids) ? Material.WOOL : Material.GLASS);
            ItemMeta metas = items.getItemMeta();
            metas.setDisplayName("§f" + ids);
            metas.setLore(artsLore);
            if (ids == currentArt) {
                metas.addEnchant(Enchantment.LURE, 0, true);
                metas.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            }
            items.setItemMeta(metas);
            menu.setItem(ids, items);
        }
    }

    public static void clickArtMenu(InventoryClickEvent event) {
        HumanEntity player = event.getWhoClicked();
        World world = event.getWhoClicked().getWorld();
        String worldName = world.getName();
        player.closeInventory();
        if (event.isShiftClick())
            removeArt(String.valueOf(event.getSlot()), worldName);
        else if (event.isLeftClick())
            loadArt(event.getSlot(), world);
        else
            saveArt(String.valueOf(event.getSlot()), world);
        updateArtMenu(worldName);
    }

    public static Inventory getArtMenu(String world) {
        Inventory menu = artsMenus.get(world);
        if (menu == null) {
            menu = LiteGame.server.createInventory(null, 54, "Анимация");
            artsMenus.put(world, menu);
        }
        return menu;
    }

    public static void clearHash(String world) {
        artsMenus.remove(world);
        currentArts.remove(world);
    }

    private static ItemStack menuItem() {
        ItemStack item = new ItemStack(Material.BONE);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("§fАнимация");
        meta.setLore(Arrays.asList("§7ПКМ - меню кадров", "§7ЛКМ + Shift - предыдущий кадр", "§7ПКМ + Shift - следующий кадр"));
        item.setItemMeta(meta);
        return item;
    }

    private static Inventory worldMenu() {
        Inventory menu = LiteGame.server.createInventory(null, 27, "Студия анимации");

        {
            ItemStack item = new ItemStack(Material.REDSTONE_BLOCK);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fЗакрыть мир");
            item.setItemMeta(meta);
            menu.setItem(11, item);
        }

        {
            ItemStack item = new ItemStack(Material.WOOL);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fУправление рисованием");
            meta.setLore(Arrays.asList("§7Разрешить/запретить рисовать"));
            item.setItemMeta(meta);
            menu.setItem(12, item);
        }

        {
            ItemStack item = new ItemStack(Material.WOOD_DOOR);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fУправление входом");
            meta.setLore(Arrays.asList("§7Разрешить входить только", "§7некоторым игрокам"));
            item.setItemMeta(meta);
            menu.setItem(13, item);
        }

        {
            ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fВыгнать игрока");
            meta.setLore(Arrays.asList("§7Кто-то мешает?", "§7Выгони его!"));
            item.setItemMeta(meta);
            menu.setItem(14, item);
        }

        {
            ItemStack item = new ItemStack(Material.WOOL);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fХолст");
            meta.setLore(Arrays.asList("§7Изменить материал", "§7листа."));
            item.setItemMeta(meta);
            menu.setItem(15, item);
        }

        return menu;
    }

    public static void clickWorldMenu(InventoryClickEvent event) {
        switch (event.getSlot()) {
            case 11: {
                WorldManager.closeWorld(event.getWhoClicked().getWorld(), false);
                break;
            }
            case 12: {
                HumanEntity player = event.getWhoClicked();
                player.openInventory(drawSettingsMenu(player.getWorld()));
                break;
            }
            case 13: {
                HumanEntity player = event.getWhoClicked();
                player.openInventory(Menus.loginManager(player.getWorld().getName()));
                break;
            }
            case 14: {
                HumanEntity player = event.getWhoClicked();
                player.openInventory(Menus.kickPlayer(player.getWorld()));
                break;
            }
            case 15: {
                HumanEntity player = event.getWhoClicked();
                player.openInventory(selectGeneratorMenu);
                break;
            }
        }
    }

    private static Inventory selectGeneratorMenu() {
        Inventory menu = LiteGame.server.createInventory(null, 45, "Холст");

        {
            ItemStack item = new ItemStack(Material.STAINED_CLAY);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fМайолика");
            item.setItemMeta(meta);
            menu.setItem(11, item);
        }

        {
            ItemStack item = new ItemStack(Material.WOOL);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fШерсть");
            item.setItemMeta(meta);
            menu.setItem(13, item);
        }

        {
            ItemStack item = new ItemStack(Material.STAINED_GLASS);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fВитраж");
            item.setItemMeta(meta);
            menu.setItem(15, item);
        }

        {
            ItemStack item = new ItemStack(Material.CONCRETE);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fБетон");
            item.setItemMeta(meta);
            menu.setItem(30, item);
        }

        {
            ItemStack item = new ItemStack(Material.CONCRETE_POWDER);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fЦемент");
            item.setItemMeta(meta);
            menu.setItem(32, item);
        }

        return menu;
    }

    public static void clickSelectGeneratorMenu(InventoryClickEvent event) {
        switch (event.getSlot()) {
            case 11: {
                Player player = (Player) event.getWhoClicked();
                player.closeInventory();
                if (!WorldManager.getWorldFolder(player.getUniqueId().toString()).isDirectory())
                    WorldManager.createWorld((Player) event.getWhoClicked(), WorldManager.ART_STUDIO_STAINED_CLAY, 17);
                else {
                    World world = player.getWorld();
                    if (world.getBlockAt(15, 18, 16).getType() != Material.STAINED_CLAY)
                        changeMaterial(world, Material.STAINED_CLAY);
                }
                break;
            }
            case 13: {
                Player player = (Player) event.getWhoClicked();
                player.closeInventory();
                if (!WorldManager.getWorldFolder(player.getUniqueId().toString()).isDirectory())
                    WorldManager.createWorld((Player) event.getWhoClicked(), WorldManager.ART_STUDIO_WOOL, 17);
                else {
                    World world = player.getWorld();
                    if (world.getBlockAt(15, 18, 16).getType() != Material.WOOL)
                        changeMaterial(world, Material.WOOL);
                }
                break;
            }
            case 15: {
                Player player = (Player) event.getWhoClicked();
                player.closeInventory();
                if (!WorldManager.getWorldFolder(player.getUniqueId().toString()).isDirectory())
                    WorldManager.createWorld((Player) event.getWhoClicked(), WorldManager.ART_STUDIO_STAINED_GLASS, 17);
                else {
                    World world = player.getWorld();
                    if (world.getBlockAt(15, 18, 16).getType() != Material.STAINED_GLASS)
                        changeMaterial(world, Material.STAINED_GLASS);
                }
                break;
            }
            case 30: {
                Player player = (Player) event.getWhoClicked();
                player.closeInventory();
                if (!WorldManager.getWorldFolder(player.getUniqueId().toString()).isDirectory())
                    WorldManager.createWorld((Player) event.getWhoClicked(), WorldManager.ART_STUDIO_CONCRETE, 17);
                else {
                    World world = player.getWorld();
                    if (world.getBlockAt(15, 18, 16).getType() != Material.CONCRETE)
                        changeMaterial(world, Material.CONCRETE);
                }
                break;
            }
            case 32: {
                Player player = (Player) event.getWhoClicked();
                player.closeInventory();
                if (!WorldManager.getWorldFolder(player.getUniqueId().toString()).isDirectory())
                    WorldManager.createWorld((Player) event.getWhoClicked(), WorldManager.ART_STUDIO_CONCRETE_POWDER, 17);
                else {
                    World world = player.getWorld();
                    if (world.getBlockAt(15, 18, 16).getType() != Material.CONCRETE_POWDER)
                        changeMaterial(world, Material.CONCRETE_POWDER);
                }
                break;
            }
        }
    }

    private static Inventory selectColorMenu() {
        Inventory menu = LiteGame.server.createInventory(null, 18, "Цвет");

        for (int i = 0; i <= 15; i++)
            menu.setItem(i, new ItemStack(Material.WOOL, 1, (short) i));

        return menu;
    }

    public static void clickColorMenu(InventoryClickEvent event) {
        ItemStack currentItem = event.getCurrentItem();
        if (currentItem.getType() != Material.AIR) {
            HumanEntity player = event.getWhoClicked();
            PlayerInventory inventory = player.getInventory();
            player.closeInventory();
            if (!inventory.contains(Material.WOOL))
                inventory.addItem(currentItem);
            else {
                inventory.remove(Material.WOOL);
                inventory.addItem(currentItem);
            }
        }
    }

    public static Inventory drawSettingsMenu(World world) {
        Inventory menu = LiteGame.server.createInventory(null, 54, "Управление рисованием");

        String worldName = world.getName();

        world.getPlayers().forEach(players ->
        {
            String playersNames = players.getName();
            if (!players.getUniqueId().toString().equals(worldName)) {
                ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
                SkullMeta meta = (SkullMeta) item.getItemMeta();
                meta.setOwner(playersNames);
                meta.setDisplayName(players.getDisplayName());
                meta.setLocalizedName(playersNames);
                meta.setLore
                        (
                                players.getInventory().contains(Material.WOOL)
                                        ? allowedDraw
                                        : disallowedDraw
                        );
                item.setItemMeta(meta);
                menu.addItem(item);
            }
        });

        {
            ItemStack item = new ItemStack(Material.WOOL);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§fРисование");
            meta.setLore
                    (
                            WorldManager.getAllowBuild(worldName)
                                    ? worldAllowedDraw
                                    : worldDisallowedDraw
                    );
            item.setItemMeta(meta);
            menu.setItem(53, item);
        }

        return menu;
    }

    public static void clickDrawSettingsMenu(InventoryClickEvent event) {
        ItemStack currentItem = event.getCurrentItem();
        switch (currentItem.getType()) {
            case SKULL_ITEM: {
                HumanEntity player = event.getWhoClicked();
                Player currentPlayer = LiteGame.server.getPlayer(currentItem.getItemMeta().getLocalizedName());
                player.closeInventory();
                if (currentPlayer != null) {
                    if (currentPlayer.getWorld() == player.getWorld()) {
                        PlayerInventory currentInventory = currentPlayer.getInventory();
                        if (!currentInventory.contains(Material.WOOL))
                            currentInventory.addItem(brushItem);
                        else
                            currentInventory.remove(Material.WOOL);
                    }
                }
                break;
            }
            case WOOL: {
                HumanEntity player = event.getWhoClicked();
                String worldName = player.getWorld().getName();
                player.closeInventory();
                if (!WorldManager.getAllowBuild(worldName))
                    WorldManager.setAllowBuild(worldName);
                else
                    WorldManager.setDisllowBuild(worldName);
            }
        }
    }

    public static void saveScreens() throws IOException {
        arts.save(artsFile);
    }
}