package ru.kirill3345.litegame.utils;

import com.destroystokyo.paper.Title;
import com.google.common.collect.Sets;

import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.bukkit.entity.Player;
import ru.kirill3345.litegame.LiteGame;

public class ChatUtils {
    public static Set<Player> chatCooldown = Sets.newConcurrentHashSet();

    public static void chat(Player player, String message) {
        if (!player.isOp()) {
            if (!chatCooldown.contains(player)) {
                StringBuilder stringBuilder = new StringBuilder("");
                for (String strings : message.split(" ")) {
                    stringBuilder.append
                            (
                                    !hasWeb(strings) && !hasIp(strings)
                                            ? strings
                                            : StringUtils.repeat("*", strings.length())
                            ).append(" ");
                }
                message = stringBuilder.toString();
                String format = player.getDisplayName() + " ➡§r " + message;
                chatCooldown.add(player);
                broadcastMessage(format, player.getWorld().getPlayers());
                LiteGame.server.getScheduler().scheduleSyncDelayedTask(LiteGame.plugin, () -> chatCooldown.remove(player), 30);
            } else
                player.sendMessage("Вы слишком быстро пишите в чат.");
        } else
            broadcastMessage(player.getDisplayName() + " ➡§r " + message, player.getWorld().getPlayers());
    }

    public static void broadcastMessage(String message, List<Player> list) {
        list.forEach(players -> players.sendMessage(message));
    }

    public static void broadcastActionBar(String message, List<Player> list) {
        list.forEach(players -> players.sendActionBar(message));
    }

    public static void broadcastTitle(Title title, List<Player> list) {
        list.forEach(players -> players.sendTitle(title));
    }

    public static boolean hasIp(String message) {
        Pattern ipPattern = Pattern.compile("((?<![0-9])(?:(?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2})[ ]?[.,-:; ][ ]?(?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2})[ ]?[., ][ ]?(?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2})[ ]?[., ][ ]?(?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2}))(?![0-9]))");
        Matcher regexMatcher = ipPattern.matcher(message);
        while (regexMatcher.find()) {
            if (regexMatcher.group().length() != 0 && ipPattern.matcher(message).find())
                return true;
        }
        return false;
    }

    public static boolean hasWeb(String message) {
        Pattern webpattern = Pattern.compile("[-a-zA-Z0-9@:%_\\+.~#?&//=]{2,256}\\.[a-z]{2,4}\\b(\\/[-a-zA-Z0-9@:%_\\+~#?&//=]*)?");
        Matcher regexMatcherurl = webpattern.matcher(message);
        while (regexMatcherurl.find()) {
            String text = regexMatcherurl.group().trim().replaceAll("www.", "").replaceAll("http://", "").replaceAll("https://", "");
            if (regexMatcherurl.group().length() != 0 && text.length() != 0) {
                if (webpattern.matcher(message).find())
                    return true;
            }
        }
        return false;
    }
}