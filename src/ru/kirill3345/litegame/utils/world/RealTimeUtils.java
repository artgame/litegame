package ru.kirill3345.litegame.utils.world;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class RealTimeUtils {
    public static long generate(TimeZone timeZone) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar(timeZone);
        int mcHour = gregorianCalendar.get(Calendar.HOUR_OF_DAY) - 6;
        if (mcHour <= 0)
            mcHour = 24 + mcHour;
        mcHour = mcHour * 1000;
        return mcHour + (gregorianCalendar.get(Calendar.MINUTE) * 16);
    }
}