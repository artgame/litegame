package ru.kirill3345.litegame.utils.world;

import org.bukkit.Material;
import org.bukkit.block.Block;
import ru.kirill3345.litegame.LiteGame;

public enum Trees {
    APPLE_TREE,
    PALM,
    POPLAR;

    public static void generate(Block block, Trees treeType) {
        switch (treeType) {
            case APPLE_TREE: {
                int height = LiteGame.random.nextInt(3) + 3;
                for (int i = 0; i <= height; i++)
                    block.getRelative(0, i, 0).setType(Material.LOG);
                block.getRelative(1, 0, 0).setType(Material.LOG);
                block.getRelative(0, 0, 1).setType(Material.LOG);
                if (LiteGame.random.nextBoolean())
                    block.getRelative(1, 1, 0).setType(Material.LOG);
                if (LiteGame.random.nextBoolean())
                    block.getRelative(0, 1, 1).setType(Material.LOG);
                if (LiteGame.random.nextBoolean())
                    block.getRelative(-1, 1, 0).setType(Material.LOG);
                if (LiteGame.random.nextBoolean())
                    block.getRelative(0, 1, -1).setType(Material.LOG);
                block.getRelative(-1, 0, 0).setType(Material.LOG);
                block.getRelative(0, 0, -1).setType(Material.LOG);
                for (int x = -2; x <= 2; x++) {
                    for (int z = -2; z <= 2; z++) {
                        {
                            Block forBlocks = block.getRelative(x, height - 1, z);
                            if (forBlocks.getType() == Material.AIR)
                                forBlocks.setType(Material.LEAVES);
                        }
                        {
                            Block forBlocks = block.getRelative(x, height, z);
                            if (forBlocks.getType() == Material.AIR)
                                forBlocks.setType(Material.LEAVES);
                        }
                        {
                            Block forBlocks = block.getRelative(x, height + 1, z);
                            if (forBlocks.getType() == Material.AIR)
                                forBlocks.setType(Material.LEAVES);
                        }
                    }
                }
                for (int x = -1; x <= 1; x++) {
                    for (int z = -1; z <= 1; z++) {
                        Block forBlocks = block.getRelative(x, height + 2, z);
                        if (forBlocks.getType() == Material.AIR)
                            forBlocks.setType(Material.LEAVES);
                    }
                }
                break;
            }
            case PALM: {
                int height = LiteGame.random.nextInt(4) + 7;
                Block leaf = block.getRelative(0, height + 1, 0);
                leaf.setType(Material.LEAVES);
                leaf.setData((byte) 3);
                for (int h = height; h >= 0; h--) {
                    Block bl = block.getRelative(0, h, 0);
                    bl.setType(Material.LOG);
                    bl.setData((byte) 3);
                }
                for (int a = 0; a <= 7; a++) {
                    int drop1 = LiteGame.random.nextInt(3) + 2;
                    int drop2 = LiteGame.random.nextInt(3) + 2;
                    int y = 0;
                    for (int x = 1; x <= 5; x++) {
                        if ((a != 1 && a != 3 && a != 5 && a != 6) || x != 5) {
                            if (x == drop1 || x == drop2)
                                y--;
                            Block bl;
                            switch (a) {
                                default:
                                    bl = leaf.getRelative(x, y, 0);
                                    break;
                                case 1:
                                    bl = leaf.getRelative(x, y, x);
                                    break;
                                case 2:
                                    bl = leaf.getRelative(0, y, x);
                                    break;
                                case 3:
                                    bl = leaf.getRelative(-x, y, x);
                                    break;
                                case 4:
                                    bl = leaf.getRelative(-x, y, 0);
                                    break;
                                case 5:
                                    bl = leaf.getRelative(x, y, -x);
                                    break;
                                case 6:
                                    bl = leaf.getRelative(-x, y, -x);
                                    break;
                                case 7:
                                    bl = leaf.getRelative(0, y, -x);
                                    break;
                            }
                            if (bl.getType() == Material.AIR) {
                                bl.setType(Material.LEAVES);
                                bl.setData((byte) 3);
                            }
                        }
                    }
                }
                break;
            }
            case POPLAR: {
                int height = LiteGame.random.nextInt(30) + 5;
                for (int h = height; h >= 0; h--) {
                    Block bl = block.getRelative(0, h, 0);
                    bl.setType(Material.LOG);
                }
                for (int h = height; h >= 4; h--) {
                    Block bl = block.getRelative(1, h, 0);
                    bl.setType(Material.LEAVES);
                }
                for (int h = height; h >= 4; h--) {
                    Block bl = block.getRelative(0, h, 1);
                    bl.setType(Material.LEAVES);
                }
                for (int h = height; h >= 4; h--) {
                    Block bl = block.getRelative(-1, h, 0);
                    bl.setType(Material.LEAVES);
                }
                for (int h = height; h >= 4; h--) {
                    Block bl = block.getRelative(0, h, -1);
                    bl.setType(Material.LEAVES);
                }
                block.getRelative(0, height + 1, 0).setType(Material.LEAVES);
                break;
            }
        }
    }
}