package ru.kirill3345.litegame.utils.world;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.TreeType;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import ru.kirill3345.litegame.LiteGame;
import ru.kirill3345.litegame.coding.CodingUtils;
import ru.kirill3345.litegame.utils.ArtStudioUtils;
import ru.kirill3345.litegame.utils.ChatUtils;
import ru.kirill3345.litegame.utils.FileUtils;
import ru.kirill3345.litegame.utils.MusicUtils;
import ru.kirill3345.litegame.utils.NavigatorUtils;
import ru.kirill3345.litegame.utils.PlayersUtils;
import ru.kirill3345.litegame.utils.SkyBlockUtils;
import ru.kirill3345.litegame.utils.Titles;

public enum WorldManager {
    FLAT,
    VOID,
    ISLAND,
    SNOW_FLAT,
    SNOW_ISLAND,
    SKYBLOCK,
    ART_STUDIO_STAINED_CLAY,
    ART_STUDIO_WOOL,
    ART_STUDIO_STAINED_GLASS,
    ART_STUDIO_CONCRETE,
    ART_STUDIO_CONCRETE_POWDER;

    private static final File worldsFile = new File(LiteGame.dataFolder, "worlds.yml");
    private static final YamlConfiguration worlds = YamlConfiguration.loadConfiguration(worldsFile);
    public static File worldContainer = LiteGame.server.getWorldContainer();
    public static boolean oldWorldsIsWiping = false;

    public static World loadWorld(String world) {
        return new WorldCreator(world)
                .type(WorldType.FLAT)
                .seed(0)
                .generatorSettings("3;minecraft:air")
                .generateStructures(false)
                .createWorld();
    }

    public static World createWorld(Player player, WorldManager generator, int type) {
        String uuid = player.getUniqueId().toString();
        boolean isNewWorld = !getWorldFolder(uuid).isDirectory();
        int worldSize = PlayersUtils.isDonator(uuid) ? 128 : 64;
        setType(uuid, type);
        setDisplayName(uuid, player.getDisplayName());
        World world = loadWorld(uuid);
        if (isNewWorld) {
            world.getWorldBorder().setCenter(0, 0);
            world.setGameRuleValue("doDaylightCycle", "false");
            world.setGameRuleValue("announceAdvancements", "false");
            world.setGameRuleValue("doWeatherCycle", "false");
            generateLand(world, worldSize, generator);
        }
        world.getWorldBorder().setSize(worldSize * 2);
        world.getWorldBorder().setWarningDistance(0);
        switch (type) {
            default: {
                setMusic(uuid, MusicUtils.randomMusic());
                world.setPVP(false);
                world.setAutoSave(true);
                break;
            }
            case 7: {
                world.setPVP(false);
                world.setAutoSave(true);
                break;
            }
            case 17: {
                world.setPVP(false);
                world.setAutoSave(false);
                ArtStudioUtils.updateArtMenu(uuid);
                break;
            }
        }
        player.teleport(world.getSpawnLocation());
        return world;
    }

    public static World createGame(String uuid, int type) {
        setType(uuid, type);
        setDisplayName(uuid, PlayersUtils.getOfflineDisplayName(uuid));
        World world = loadWorld(uuid);
        switch (type) {
            case 2:
            case 5:
            case 6:
            case 8:
            case 10:
            case 12:
            case 13:
            case 14: {
                world.setPVP(true);
                world.setAutoSave(false);
                break;
            }
            case 3:
            case 4:
            case 11: {
                world.setPVP(false);
                world.setAutoSave(false);
                break;
            }
            case 15: {
                world.setPVP(false);
                world.setAutoSave(true);
                break;
            }
            case 18: {
                world.setPVP(false);
                world.setAutoSave(false);
                CodingUtils.createParser(world);
                break;
            }
        }
        world.setAutoSave(false);
        NavigatorUtils.updateMenu(type);
        return world;
    }

    public static void closeWorld(World world, boolean save) {
        world.getPlayers().forEach(players ->
        {
            players.teleport(Hub.location);
            players.sendTitle(Titles.worldClosed);
        });
        LiteGame.server.getScheduler().scheduleSyncDelayedTask(LiteGame.plugin, () -> LiteGame.server.unloadWorld(world, save), 1);
    }

    public static void closeSkyBlock(World world) {
        world.getPlayers().forEach(players ->
        {
            SkyBlockUtils.savePlayer(players);
            players.teleport(Hub.location);
            players.sendTitle(Titles.worldClosed);
        });
        LiteGame.server.getScheduler().scheduleSyncDelayedTask(LiteGame.plugin, () -> LiteGame.server.unloadWorld(world, true), 1);
    }

    public static void updatePlayerWorld(String uuid) {
        File playerWorldFile = getWorldFolder(uuid);
        if (playerWorldFile.isDirectory())
            playerWorldFile.setLastModified(System.currentTimeMillis());
    }

    public static void wipeOldWorlds() {
        if (!oldWorldsIsWiping) {
            long time = System.currentTimeMillis();
            int fileId = 0;
            oldWorldsIsWiping = true;
            for (File files : worldContainer.listFiles()) {
                String worldsNames = files.getName();
                fileId++;
                LiteGame.server.getScheduler().scheduleSyncDelayedTask(LiteGame.plugin, () ->
                {
                    if (files.isDirectory()) {
                        if (!PlayersUtils.isRegistered(worldsNames)) {
                            if (files.lastModified() + (3 * 24 * 60 * 60 * 1000) <= time) {
                                if (LiteGame.server.getWorld(worldsNames) == null) {
                                    if (!PlayersUtils.isDonator(worldsNames)) {
                                        ChatUtils.broadcastActionBar("Удаление мира:§7 " + worldsNames, (List<Player>) LiteGame.server.getOnlinePlayers());
                                        WorldManager.removeDataHash(worldsNames);
                                        SkyBlockUtils.removeSkyBlock(worldsNames);
                                        ArtStudioUtils.removeArts(worldsNames);
                                        FileUtils.deleteDirectory(files);
                                    }
                                }
                            }
                        }
                    }
                }, fileId * 4);
            }
            LiteGame.server.getScheduler().scheduleSyncDelayedTask(LiteGame.plugin, () -> oldWorldsIsWiping = false, 10000);
        }
    }

    public static void setType(String world, int type) {
        worlds.set(world + ".type", type);
    }

    public static void setLogin(String world, int login) {
        worlds.set(world + ".login", login);
    }

    public static void removeType(String world) {
        worlds.set(world + ".type", null);
    }

    public static void removeLogin(String world) {
        worlds.set(world + ".login", null);
    }

    public static void setStarted(String world) {
        worlds.set(world + ".started", true);
    }

    public static void removeStarted(String world) {
        worlds.set(world + ".started", null);
    }

    public static void setDisplayName(String world, String displayName) {
        worlds.set(world + ".displayName", displayName);
    }

    public static void setMusic(String world, String music) {
        worlds.set(world + ".music", music);
    }

    public static void setAllowBuild(String world) {
        worlds.set(world + ".allowBuild", true);
    }

    public static void setAllowFlying(String world) {
        worlds.set(world + ".allowFlying", null);
    }

    public static void setDisllowBuild(String world) {
        worlds.set(world + ".allowBuild", null);
    }

    public static void setDisllowFlying(String world) {
        worlds.set(world + ".allowFlying", false);
    }

    public static void clearHash() {
        worlds.getKeys(false).stream().filter(keys -> LiteGame.server.getWorld(keys) == null).forEach(keys ->
        {
            switch (WorldManager.getType(keys)) {
                case 1: {
                    removeDataHash(keys);
                    break;
                }
                case 7: {
                    removeTempHash(keys);
                    break;
                }
                case 15:
                    break;
                case 17: {
                    ArtStudioUtils.clearHash(keys);
                    removeTempHash(keys);
                    break;
                }
            }
        });
    }

    public static int getType(String world) {
        return worlds.getInt(world + ".type", 0);
    }

    public static int getLogin(String world) {
        return worlds.getInt(world + ".login", 0);
    }

    public static boolean isStarted(String world) {
        return worlds.contains(world + ".started", false);
    }

    public static String getDisplayName(String world) {
        return worlds.getString(world + ".displayName", world);
    }

    public static boolean hasMusic(String world) {
        return worlds.contains(world + ".music");
    }

    public static String getMusic(String world) {
        return worlds.getString(world + ".music", "");
    }

    public static boolean getAllowBuild(String world) {
        return worlds.getBoolean(world + ".allowBuild", false);
    }

    public static boolean getAllowFlying(String world) {
        return worlds.getBoolean(world + ".allowFlying", true);
    }

    public static void removeDataHash(String world) {
        worlds.set(world, null);
    }

    public static void removeTempHash(String world) {
        WorldManager.setDisllowBuild(world);
        WorldManager.removeLogin(world);
    }

    public static Set<String> getKeys() {
        return worlds.getKeys(false);
    }

    public static void saveHash() throws IOException {
        worlds.save(worldsFile);
    }

    public static void generateLand(World world, int worldSize, WorldManager generator) {
        switch (generator) {
            case FLAT: {
                for (int x = -worldSize; x < worldSize; x++) {
                    for (int z = -worldSize; z < worldSize; z++)
                        world.getBlockAt(x, 0, z).setType(Material.GRASS);
                }
                world.setSpawnLocation(0, 1, 0);
                break;
            }
            case VOID: {
                for (int x = -1; x < 1; x++) {
                    for (int z = -1; z < 1; z++)
                        world.getBlockAt(x, 0, z).setType(Material.GLASS);
                }
                world.setSpawnLocation(0, 1, 0);
                break;
            }
            case ISLAND: {
                for (int x = -worldSize; x < worldSize; x++) {
                    for (int z = -worldSize; z < worldSize; z++) {
                        world.getBlockAt(x, 0, z).setType(Material.SANDSTONE);
                        world.getBlockAt(x, 1, z).setType(x >= -40 && x <= 40 && z >= -40 && z <= 40 ? Material.SAND : Material.WATER);
                    }
                }
                for (int x = -32; x < 32; x++) {
                    for (int z = -32; z < 32; z++)
                        world.getBlockAt(x, 2, z).setType(Material.GRASS);
                }
                world.setSpawnLocation(0, 3, 0);
                break;
            }
            case SNOW_FLAT: {
                for (int x = -worldSize; x < worldSize; x++) {
                    for (int z = -worldSize; z < worldSize; z++)
                        world.getBlockAt(x, 0, z).setType(Material.SNOW_BLOCK);
                }
                world.setSpawnLocation(0, 1, 0);
                break;
            }
            case SNOW_ISLAND: {
                for (int x = -worldSize; x < worldSize; x++) {
                    for (int z = -worldSize; z < worldSize; z++) {
                        world.getBlockAt(x, 0, z).setType(Material.PACKED_ICE);
                        world.getBlockAt(x, 1, z).setType(x >= -40 && x <= 40 && z >= -40 && z <= 40 ? Material.ICE : Material.WATER);
                    }
                }
                for (int x = -32; x < 32; x++) {
                    for (int z = -32; z < 32; z++)
                        world.getBlockAt(x, 2, z).setType(Material.SNOW_BLOCK);
                }
                world.setSpawnLocation(0, 3, 0);
                break;
            }
            case SKYBLOCK: {
                for (int x = -1; x < 1; x++) {
                    for (int z = -1; z < 1; z++)
                        world.getBlockAt(x, 0, z).setType(Material.DIRT);
                }
                for (int x = -2; x < 2; x++) {
                    for (int z = -2; z < 2; z++)
                        world.getBlockAt(x, 1, z).setType(Material.DIRT);
                }
                for (int x = -3; x < 3; x++) {
                    for (int z = -3; z < 3; z++)
                        world.getBlockAt(x, 2, z).setType(Material.DIRT);
                }
                for (int x = -4; x < 4; x++) {
                    for (int z = -4; z < 4; z++)
                        world.getBlockAt(x, 3, z).setType(Material.GRASS);
                }
                world.generateTree(new Location(world, 0, 4, -3), TreeType.BIG_TREE);
                world.setSpawnLocation(0, 4, 0);
                break;
            }
            case ART_STUDIO_STAINED_CLAY: {
                for (int x = 23; x >= -24; x--) {
                    for (int y = 25; y >= 0; y--)
                        world.getBlockAt(x, y, 16).setType(Material.STAINED_CLAY);
                }
                world.setSpawnLocation(0, 1, 0);
                break;
            }
            case ART_STUDIO_WOOL: {
                for (int x = 23; x >= -24; x--) {
                    for (int y = 25; y >= 0; y--)
                        world.getBlockAt(x, y, 16).setType(Material.WOOL);
                }
                world.setSpawnLocation(0, 1, 0);
                break;
            }
            case ART_STUDIO_STAINED_GLASS: {
                for (int x = 23; x >= -24; x--) {
                    for (int y = 25; y >= 0; y--)
                        world.getBlockAt(x, y, 16).setType(Material.STAINED_GLASS);
                }
                world.setSpawnLocation(0, 1, 0);
                break;
            }
            case ART_STUDIO_CONCRETE: {
                for (int x = 23; x >= -24; x--) {
                    for (int y = 25; y >= 0; y--)
                        world.getBlockAt(x, y, 16).setType(Material.CONCRETE);
                }
                world.setSpawnLocation(0, 1, 0);
                break;
            }
            case ART_STUDIO_CONCRETE_POWDER: {
                for (int x = 23; x >= -24; x--) {
                    for (int y = 25; y >= 0; y--)
                        world.getBlockAt(x, y, 16).setType(Material.CONCRETE_POWDER);
                }
                world.setSpawnLocation(0, 1, 0);
                break;
            }
        }
        world.save();
    }

    public static File getWorldFolder(String world) {
        return new File(worldContainer, world);
    }
}