package ru.kirill3345.litegame.utils.world;

import com.destroystokyo.paper.Title;
import com.google.common.collect.Lists;

import java.util.List;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import ru.kirill3345.litegame.LiteGame;
import ru.kirill3345.litegame.utils.Items;
import ru.kirill3345.litegame.utils.NavigatorUtils;
import ru.kirill3345.litegame.utils.Titles;

public class GameUtils {
    public static void startGame(World world) {
        Location worldSpawn = world.getSpawnLocation();
        String worldName = world.getName();
        int type = WorldManager.getType(worldName);
        world.getPlayers().forEach(players ->
        {
            players.teleport(worldSpawn);
            players.playSound(worldSpawn, Sound.ENTITY_PLAYER_LEVELUP, Float.MAX_VALUE, 1);
            players.sendTitle(Titles.gameStarted);
        });
        WorldManager.setStarted(worldName);
        switch (type) {
            case 4: {
                List<EnderCrystal> crystalsList = Lists.newArrayList(world.getEntitiesByClass(EnderCrystal.class));
                for (int i = 0; i < world.getPlayers().size(); i++) {
                    Player players = world.getPlayers().get(i);
                    initGamePlayer(players, crystalsList.get(i).getLocation());
                    players.getInventory().setItem(0, Items.eggGun);
                }
                crystalsList.forEach(crystals -> crystals.remove());
                break;
            }
            case 5: {
                ItemStack snowBalls = new ItemStack(Material.SNOW_BALL, 16);
                List<EnderCrystal> crystalsList = Lists.newArrayList(world.getEntitiesByClass(EnderCrystal.class));
                for (int i = 0; i < world.getPlayers().size(); i++) {
                    Player players = world.getPlayers().get(i);
                    initGamePlayer(players, crystalsList.get(i).getLocation());
                    players.getInventory().setItem(0, snowBalls);
                }
                crystalsList.forEach(crystals -> crystals.remove());
                break;
            }
            case 6:
            case 8: {
                List<EnderCrystal> crystalsList = Lists.newArrayList(world.getEntitiesByClass(EnderCrystal.class));
                for (int i = 0; i < world.getPlayers().size(); i++) {
                    Player players = world.getPlayers().get(i);
                    initGamePlayer(players, crystalsList.get(i).getLocation());
                    players.getInventory().setItem(0, Items.unbreakableIronSword);
                }
                crystalsList.forEach(crystals -> crystals.remove());
                break;
            }
            case 10: {
                List<EnderCrystal> crystalsList = Lists.newArrayList(world.getEntitiesByClass(EnderCrystal.class));
                for (int i = 0; i < world.getPlayers().size(); i++) {
                    Player players = world.getPlayers().get(i);
                    initGamePlayer(players, crystalsList.get(i).getLocation());
                    players.getInventory().setItem(0, Items.marker);
                }
                crystalsList.forEach(crystals -> crystals.remove());
                break;
            }
            case 11: {
                List<EnderCrystal> crystalsList = Lists.newArrayList(world.getEntitiesByClass(EnderCrystal.class));
                for (int i = 0; i < world.getPlayers().size(); i++)
                    initGamePlayer(world.getPlayers().get(i), crystalsList.get(i).getLocation());
                crystalsList.forEach(crystals -> crystals.remove());
                break;
            }
            case 13: {
                List<EnderCrystal> crystalsList = Lists.newArrayList(world.getEntitiesByClass(EnderCrystal.class));
                for (int i = 0; i < world.getPlayers().size(); i++) {
                    Player players = world.getPlayers().get(i);
                    initGamePlayer(players, crystalsList.get(i).getLocation());
                    players.setGameMode(GameMode.SURVIVAL);
                }
                crystalsList.forEach(crystals -> crystals.remove());
                break;
            }
            case 14: {
                List<EnderCrystal> crystalsList = Lists.newArrayList(world.getEntitiesByClass(EnderCrystal.class));
                for (int i = 0; i < world.getPlayers().size(); i++) {
                    Player players = world.getPlayers().get(i);
                    initGamePlayer(players, crystalsList.get(i).getLocation());
                    players.setNoDamageTicks(200);
                }
                crystalsList.forEach(crystals -> crystals.remove());
                break;
            }
        }
        NavigatorUtils.updateMenu(type);
    }

    public static void initGamePlayer(Player player, Location spawn) {
        player.teleport(spawn);
        player.setFallDistance(0);
        player.setAllowFlight(false);
        player.getInventory().clear();
    }

    public static boolean tryStartGame(Player player) {
        if (player.getWorld().getPlayers().size() != 1) {
            startGame(player.getWorld());
            player.getInventory().remove(Material.DIAMOND);
            return true;
        } else {
            player.sendMessage("Нужны игроки для запуска игры.");
            player.setCooldown(Material.DIAMOND, 15);
            return false;
        }
    }

    public static void endGame(Player player, World world) {
        Title title = new Title("", player.getDisplayName() + "§r победил!", 5, 50, 5);
        world.getPlayers().forEach(players ->
        {
            players.teleport(Hub.location);
            players.playSound(Hub.location, Sound.ENTITY_WITHER_SPAWN, Float.MAX_VALUE, 1);
            players.sendTitle(title);
        });
        LiteGame.server.getScheduler().scheduleSyncDelayedTask(LiteGame.plugin, () -> LiteGame.server.unloadWorld(world, false), 1);
    }

    public static boolean tryEndGame(World world) {
        switch (world.getPlayers().size()) {
            case 0: {
                LiteGame.server.unloadWorld(world, false);
                return true;
            }
            case 1: {
                if (WorldManager.isStarted(world.getName())) {
                    world.getPlayers().forEach(players -> endGame(players, world));
                    return true;
                } else
                    return false;
            }
            default:
                return false;
        }
    }
}