package ru.kirill3345.litegame.utils.world;

import org.bukkit.Location;
import org.bukkit.World;
import ru.kirill3345.litegame.LiteGame;

public class Hub {
    public static World world = LiteGame.server.getWorlds().get(0);
    public static Location location = Hub.world.getSpawnLocation();
    public static String name = Hub.world.getName();
}