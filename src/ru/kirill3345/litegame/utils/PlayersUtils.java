package ru.kirill3345.litegame.utils;

import com.google.common.collect.Sets;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import ru.kirill3345.litegame.LiteGame;

public class PlayersUtils {
    private static final File playersFile = new File(LiteGame.dataFolder, "players.yml");
    private static final YamlConfiguration players = YamlConfiguration.loadConfiguration(playersFile);
    private static final File donatorsFile = new File(LiteGame.dataFolder, "donators.yml");
    private static final YamlConfiguration donators = YamlConfiguration.loadConfiguration(donatorsFile);
    private static final Set<String> logginedPlayers = Sets.newConcurrentHashSet();

    public static boolean isRegistered(String uuid) {
        return players.contains(uuid);
    }

    public static void setPassword(String uuid, String password) {
        players.set(uuid + ".password", password);
    }

    public static void setHype(Player player, int hypeCount) {
        players.set(player.getUniqueId().toString() + ".hype", hypeCount);
        player.setLevel(hypeCount);
    }

    public static void setOfflineHype(String uuid, int hypeCount) {
        players.set(uuid + ".hype", hypeCount);
    }

    public static void enable(String uuid, String function) {
        List<String> list = players.getStringList(uuid + ".settings");
        list.add(function);
        players.set(uuid + ".settings", list);
    }

    public static void switchFunction(String uuid, String function) {
        if (!isEnabled(uuid, function))
            enable(uuid, function);
        else
            disable(uuid, function);
    }

    public static boolean isEnabled(String uuid, String function) {
        return players.getStringList(uuid + ".settings").contains(function);
    }

    public static void login(String uuid) {
        logginedPlayers.add(uuid);
    }

    public static void removePlayer(String uuid) {
        players.set(uuid, null);
    }

    public static String getUuidByIp(String ip) {
        for (String keys : players.getKeys(false)) {
            if (getIp(keys).equals(ip))
                return keys;
        }
        return "";
    }

    public static void setIp(String uuid, String ip) {
        players.set(uuid + ".ip", ip);
    }

    public static boolean isLoggined(String uuid) {
        return logginedPlayers.contains(uuid);
    }

    public static String getPassword(String uuid) {
        return players.getString(uuid + ".password", "");
    }

    public static int getHype(String uuid) {
        return players.getInt(uuid + ".hype", 0);
    }

    public static void disable(String uuid, String function) {
        List<String> list = players.getStringList(uuid + ".settings");
        list.remove(function);
        players.set(uuid + ".settings", !list.isEmpty() ? list : null);
    }

    public static void unLogin(String uuid) {
        logginedPlayers.remove(uuid);
    }

    public static String getIp(String uuid) {
        return players.getString(uuid + ".ip", "");
    }

    public static String getDonate(String uuid) {
        return donators.getString(uuid, "");
    }

    public static boolean isDonator(String uuid) {
        return donators.contains(uuid);
    }

    public static boolean isSponsor(String uuid) {
        return donators.getString(uuid, "").equals("sponsor") ? true : isFavorite(uuid);
    }

    public static boolean isFavorite(String uuid) {
        return donators.getString(uuid, "").equals("favorite") ? true : isDev(uuid);
    }

    public static boolean isModer(String uuid) {
        return donators.getString(uuid, "").equals("moder") ? true : isDev(uuid);
    }

    public static boolean isDev(String uuid) {
        return donators.getString(uuid, "").equals("dev") ? true : isOwner(uuid);
    }

    public static boolean isOwner(String uuid) {
        return donators.getString(uuid, "").equals("owner");
    }

    public static String getDisplayName(Player player) {
        String playerName = player.getName();
        String uuidString = player.getUniqueId().toString();
        if (isRegistered(uuidString)) {
            if (isLoggined(uuidString)) {
                if (isDonator(uuidString)) {
                    switch (getDonate(uuidString)) {
                        default:
                            return "§8[§bDonator§8]§b " + playerName;
                        case "sponsor":
                            return "§8[§3Sponsor§8]§3 " + playerName;
                        case "favorite":
                            return "§8[§6Favorite§8]§6 " + playerName;
                        case "moder":
                            return "§8[§9Moder§8]§9 " + playerName;
                        case "dev":
                            return "§8[§aDev§8]§a " + playerName;
                        case "owner":
                            return "§8[§2Owner§8]§2 " + playerName + "§l";
                    }
                } else
                    return "§7" + playerName + "§2";
            } else
                return "§8" + playerName + "§2";
        } else
            return "§8" + playerName + "§2";
    }

    public static String getOfflineDisplayName(String uuid) {
        String name = LiteGame.server.getOfflinePlayer(UUID.fromString(uuid)).getName();
        if (isRegistered(uuid)) {
            if (isDonator(uuid)) {
                switch (getDonate(uuid)) {
                    default:
                        return "§8[§bDonator§8]§b " + name;
                    case "sponsor":
                        return "§8[§3Sponsor§8]§3 " + name;
                    case "favorite":
                        return "§8[§6Favorite§8]§6 " + name;
                    case "moder":
                        return "§8[§9Moder§8]§9 " + name;
                    case "dev":
                        return "§8[§aDev§8]§a " + name;
                    case "owner":
                        return "§8[§2Owner§8]§2 " + name + "§l";
                }
            } else
                return "§7" + name + "§2";
        } else
            return "§8" + name + "§2";
    }

    public static void setDisplayName(Player player, String displayName) {
        player.setDisplayName(displayName);
        player.setPlayerListName(displayName);
    }

    public static void giveDonate(String player, String donate) throws IOException, UnsupportedEncodingException {
        player = new String(player.getBytes("UTF-8"), "UTF-8");
        donators.set(LiteGame.server.getOfflinePlayer(player).getUniqueId().toString(), donate);
        saveDonators();
        LiteGame.server.broadcastMessage(player + " теперь " + donate + "!");
        Player onlinePlayer = LiteGame.server.getPlayer(player);
        if (onlinePlayer != null)
            setDisplayName(onlinePlayer, getDisplayName(onlinePlayer));
    }

    public static void removeDonate(String player) throws IOException, UnsupportedEncodingException {
        player = new String(player.getBytes("UTF-8"), "UTF-8");
        donators.set(LiteGame.server.getOfflinePlayer(player).getUniqueId().toString(), null);
        saveDonators();
        Player onlinePlayer = LiteGame.server.getPlayer(player);
        if (onlinePlayer != null)
            setDisplayName(onlinePlayer, getDisplayName(onlinePlayer));
    }

    public static void savePlayers() throws IOException {
        players.save(playersFile);
    }

    public static void saveDonators() throws IOException {
        donators.save(donatorsFile);
    }
}