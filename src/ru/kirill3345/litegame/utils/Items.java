package ru.kirill3345.litegame.utils;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Items {
    public static ItemStack menu = menu();
    public static ItemStack gadgets = gadgets();
    public static ItemStack settings = settings();
    public static ItemStack info = info();
    public static ItemStack unbreakableIronSword = unbreakableIronSword();
    public static ItemStack startGame = startGame();
    public static ItemStack parkourRespawn = parkourRespawn();
    public static ItemStack eggGun = eggGun();
    public static ItemStack marker = marker();
    public static ItemStack unbreakableElytra = unbreakableElytra();
    public static ItemStack otherBlocks = otherBlocks();
    public static ItemStack quests = quests();
    public static ItemStack enderPearlGadget = enderPearlGadget();
    public static ItemStack fireworkGadget = fireworkGadget();
    public static ItemStack snowBallGun = snowBallGun();
    public static ItemStack tntGadget = tntGadget();
    public static ItemStack arrowGadget = arrowGadget();
    public static ItemStack enderRodGadget = enderRodGadget();
    public static ItemStack dragonSkullGadget = dragonSkullGadget();
    public static ItemStack[] skyWarsDrops = new ItemStack[]
            {
                    new ItemStack(Material.STONE_SWORD), new ItemStack(Material.WOOD, 32), new ItemStack(Material.WOOD_PICKAXE),
                    new ItemStack(Material.IRON_SWORD), new ItemStack(Material.LOG, 8), new ItemStack(Material.IRON_PICKAXE),
                    new ItemStack(Material.WOOD_SWORD), new ItemStack(Material.STONE, 32), new ItemStack(Material.WOOD_PICKAXE)
            };
    public static ItemStack[] survivalGamesDrops = new ItemStack[]
            {
                    new ItemStack(Material.STONE_SWORD), new ItemStack(Material.IRON_SWORD), new ItemStack(Material.WOOD_SWORD),
            };
    public static ItemStack air = new ItemStack(Material.AIR);

    private static ItemStack menu() {
        ItemStack item = new ItemStack(Material.COMPASS);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("§fМеню");
        meta.setLore(Arrays.asList("§7Перемещение между мирами."));
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack gadgets() {
        ItemStack item = new ItemStack(Material.CHEST);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("§fГаджеты");
        meta.setLore(Arrays.asList("§7Доступно для всех."));
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack settings() {
        ItemStack item = new ItemStack(Material.REDSTONE_COMPARATOR);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("§fНастройки");
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack info() {
        ItemStack item = new ItemStack(Material.BOOK);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("§fИнформация");
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack unbreakableIronSword() {
        ItemStack item = new ItemStack(Material.IRON_SWORD);
        ItemMeta meta = item.getItemMeta();
        meta.setUnbreakable(true);
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack startGame() {
        ItemStack item = new ItemStack(Material.DIAMOND);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("§fЗапустить игру");
        meta.setLore(Arrays.asList("§7Нужны игроки для", "§7запуска."));
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack parkourRespawn() {
        ItemStack item = new ItemStack(Material.DIAMOND);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("§fВ начало");
        meta.setLore(Arrays.asList("§7Телепортироваться в", "§7начало паркура."));
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack eggGun() {
        ItemStack item = new ItemStack(Material.IRON_SPADE);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("§fЯйцемёт");
        meta.setLore(Arrays.asList("§7Пиу-пиу-пиу!"));
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack marker() {
        ItemStack item = new ItemStack(Material.DIAMOND_BARDING);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("§fМаркер");
        meta.setLore(Arrays.asList("§7Пиу-пиу-пиу!"));
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack unbreakableElytra() {
        ItemStack item = new ItemStack(Material.ELYTRA);
        ItemMeta meta = item.getItemMeta();
        meta.setUnbreakable(true);
        meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack otherBlocks() {
        ItemStack item = new ItemStack(Material.ITEM_FRAME);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("§fОсобые блоки");
        meta.setLore(Arrays.asList("§7Технические блоки."));
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack enderPearlGadget() {
        ItemStack item = new ItemStack(Material.ENDER_PEARL);
        ItemMeta meta = item.getItemMeta();
        meta.setLore(Arrays.asList("§7Помогает покорять", "§7вершины."));
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack fireworkGadget() {
        ItemStack item = new ItemStack(Material.FIREWORK);
        ItemMeta meta = item.getItemMeta();
        meta.setLore(Arrays.asList("§7Праздник к нам", "§7приходит!"));
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack snowBallGun() {
        ItemStack item = new ItemStack(Material.DIAMOND_HOE);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("§fСнегомёт");
        meta.setLore(Arrays.asList("§7Легенда возвращается!"));
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack tntGadget() {
        ItemStack item = new ItemStack(Material.TNT);
        ItemMeta meta = item.getItemMeta();
        meta.setLore(Arrays.asList("§7Почувствуй себя", "§7крипером!"));
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack arrowGadget() {
        ItemStack item = new ItemStack(Material.ARROW);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("§fКопьё");
        meta.setLore(Arrays.asList("§7Можно швырнуть."));
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack enderRodGadget() {
        ItemStack item = new ItemStack(Material.END_ROD);
        ItemMeta meta = item.getItemMeta();
        meta.setLore(Arrays.asList("§7Для особого", "§7применения..."));
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack dragonSkullGadget() {
        ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short) 5);
        ItemMeta meta = item.getItemMeta();
        meta.setLore(Arrays.asList("§7Можно надеть."));
        item.setItemMeta(meta);
        return item;
    }

    private static ItemStack quests() {
        ItemStack item = new ItemStack(Material.BOOK);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("§fЗадания");
        meta.setLore(Arrays.asList("§7Начните играть на SkyBlock", "§7и выполнять задания."));
        item.setItemMeta(meta);
        return item;
    }
}