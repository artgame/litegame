package ru.kirill3345.litegame.utils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import ru.kirill3345.litegame.LiteGame;
import ru.kirill3345.litegame.utils.world.WorldManager;

public class SkyBlockUtils {
    private static final File skyBlockFile = new File(LiteGame.dataFolder, "skyBlock.yml");
    private static final YamlConfiguration skyBlock = YamlConfiguration.loadConfiguration(skyBlockFile);
    public static ItemStack[] rewards = new ItemStack[]
            {
                    new ItemStack(Material.GRASS, 4),
                    new ItemStack(Material.COAL, 5),
                    new ItemStack(Material.WOOD, 8),
                    new ItemStack(Material.BONE),
                    new ItemStack(Material.SEEDS, 6),
                    new ItemStack(Material.CLAY_BALL, 4),
                    new ItemStack(Material.IRON_INGOT)
            };

    public static void savePlayer(HumanEntity player) {
        String uuid = player.getUniqueId().toString();
        skyBlock.set(uuid + ".contents", player.getInventory().getContents());
        skyBlock.set(uuid + ".health", player.getHealth());
        skyBlock.set(uuid + ".fireticks", player.getFireTicks());
    }

    public static boolean hasInventory(String uuid) {
        return skyBlock.contains(uuid);
    }

    public static void loadPlayer(Player player) {
        String uuid = player.getUniqueId().toString();
        PlayerInventory inventory = player.getInventory();
        if (hasInventory(uuid)) {
            try {
                inventory.setContents((ItemStack[]) skyBlock.get(uuid + ".contents"));
            } catch (Exception ex0) {
                try {
                    int i = 0;
                    for (ItemStack items : (List<ItemStack>) skyBlock.getList(uuid + ".contents")) {
                        if (items != null)
                            inventory.setItem(i, items);
                        i++;
                    }
                } catch (Exception ex1) {
                    try {
                        inventory.clear();
                        for (ItemStack items : (List<ItemStack>) skyBlock.getList(uuid + ".contents")) {
                            if (items != null)
                                inventory.addItem(items);
                        }
                    } catch (Exception ex2) {
                        player.sendMessage("Ошибка загрузки инвентаря...");
                    }
                }
            }
            player.setHealth(skyBlock.getDouble(uuid + ".health"));
            player.setFireTicks(skyBlock.getInt(uuid + ".fireticks"));
        } else {
            inventory.setItem(7, Items.quests);
            inventory.setItem(8, Items.menu);
            savePlayer(player);
        }
    }

    public static void removeSkyBlock(String uuid) {
        skyBlock.set(uuid, null);
    }

    public static void saveAllPlayers(World world) {
        world.getPlayers().forEach(players -> savePlayer(players));
    }

    public static void runQuestCheck(Player player) {
        String uuid = player.getUniqueId().toString();
        switch (skyBlock.getInt(uuid + ".quest")) {
            case 0:
                skyBlock.set(uuid + ".quest", 1);
                player.sendTitle(Titles.questCompleted);
                MusicUtils.playQuestSound(player.getLocation());
                player.getInventory().addItem(new ItemStack(Material.WOOD, 4));
                break;
            case 1:
                if (player.getInventory().contains(Material.WORKBENCH)) {
                    skyBlock.set(uuid + ".quest", 2);
                    player.sendTitle(Titles.questCompleted);
                    MusicUtils.playQuestSound(player.getLocation());
                } else
                    player.sendMessage("Сделайте и возьмите в руку верстак.");
                break;
            case 2:
                if (player.getInventory().contains(Material.WOOD_SPADE)) {
                    skyBlock.set(uuid + ".quest", 3);
                    player.sendTitle(Titles.questCompleted);
                    MusicUtils.playQuestSound(player.getLocation());
                } else
                    player.sendMessage("Сделайте и возьмите в руку деревянную лопату.");
                break;
            case 3:
                if (WorldManager.getLogin(uuid) == 3) {
                    skyBlock.set(uuid + ".quest", 4);
                    player.sendTitle(Titles.questCompleted);
                    MusicUtils.playQuestSound(player.getLocation());
                } else
                    player.sendMessage("Возьмите в руку меню управления миром и запретите всех входить.");
                break;
            case 4:
                if (player.getInventory().contains(Material.DIRT, 32)) {
                    skyBlock.set(uuid + ".quest", 5);
                    player.sendTitle(Titles.questCompleted);
                    MusicUtils.playQuestSound(player.getLocation());
                    player.getInventory().addItem(new ItemStack(Material.WATER_BUCKET));
                    player.getInventory().addItem(new ItemStack(Material.LAVA_BUCKET));
                } else
                    player.sendMessage("Сделайте и возьмите в руку 32 штуки земли.");
                break;
            case 5:
                if (player.getInventory().contains(Material.COBBLESTONE, 32)) {
                    skyBlock.set(uuid + ".quest", 6);
                    player.sendTitle(Titles.questCompleted);
                    MusicUtils.playQuestSound(player.getLocation());
                    player.getInventory().addItem(new ItemStack(Material.SEEDS, 8));
                } else
                    player.sendMessage("Наполните генератор булыжника досками и сделайте 32 штуки булыжника.");
                break;
            case 6:
                if (player.getInventory().contains(Material.WHEAT, 8)) {
                    skyBlock.set(uuid + ".quest", 7);
                    player.sendTitle(Titles.questCompleted);
                    MusicUtils.playQuestSound(player.getLocation());
                } else
                    player.sendMessage("Посадите пшеницу и соберите 8 штук зрелого пшена.");
                break;
            case 7:
                if (player.getInventory().contains(Material.LOG, 32)) {
                    skyBlock.set(uuid + ".quest", 8);
                    player.sendTitle(Titles.questCompleted);
                    MusicUtils.playQuestSound(player.getLocation());
                    player.getInventory().addItem(new ItemStack(Material.SAND, 8));
                } else
                    player.sendMessage("Нарубите 32 штуки дуба.");
                break;
            case 8:
                if (player.getInventory().contains(Material.COBBLESTONE, 64)) {
                    skyBlock.set(uuid + ".quest", 9);
                    player.sendTitle(Titles.questCompleted);
                    MusicUtils.playQuestSound(player.getLocation());
                    player.getInventory().addItem(new ItemStack(Material.IRON_INGOT, 3));
                } else
                    player.sendMessage("Добудьте 64 штуки булыжника.");
                break;
            case 9:
                if (player.getInventory().contains(Material.IRON_PICKAXE)) {
                    skyBlock.set(uuid + ".quest", 10);
                    player.sendTitle(Titles.questCompleted);
                    MusicUtils.playQuestSound(player.getLocation());
                    player.getInventory().addItem(new ItemStack(Material.CACTUS));
                } else
                    player.sendMessage("Сделайте 1 железную кирку.");
                break;
            case 10:
                if (player.getInventory().contains(Material.COBBLESTONE, 128)) {
                    skyBlock.set(uuid + ".quest", 11);
                    player.sendTitle(Titles.questCompleted);
                    MusicUtils.playQuestSound(player.getLocation());
                } else
                    player.sendMessage("Добудьте 128 штук булыжника.");
                break;
            case 11:
                if (player.getInventory().contains(Material.CACTUS, 3)) {
                    skyBlock.set(uuid + ".quest", 12);
                    player.sendTitle(Titles.questCompleted);
                    MusicUtils.playQuestSound(player.getLocation());
                    player.getInventory().addItem(new ItemStack(351, 1, (short) 3));
                    player.getInventory().addItem(new ItemStack(Material.LOG, 1, (short) 3));
                } else
                    player.sendMessage("Вырастите 3 штуки кактуса.");
                break;
            case 12:
                if (player.getInventory().contains(new ItemStack(351, 3, (short) 3))) {
                    skyBlock.set(uuid + ".quest", 13);
                    player.sendTitle(Titles.questCompleted);
                    MusicUtils.playQuestSound(player.getLocation());
                } else
                    player.sendMessage("Вырастите 3 штуки какао-боба.");
                break;
            case 13:
                if (player.getInventory().contains(Material.COBBLESTONE, 256)) {
                    skyBlock.set(uuid + ".quest", 14);
                    player.sendTitle(Titles.questCompleted);
                    MusicUtils.playQuestSound(player.getLocation());
                } else
                    player.sendMessage("Добудьте 256 штук булыжника.");
                break;
            default:
                switch (skyBlock.getInt(uuid + ".main.randomQuest")) {
                    default:
                        if (player.getInventory().contains(Material.LOG, 32)) {
                            skyBlock.set(uuid + ".main.randomQuest", LiteGame.random.nextInt(7));
                            player.sendTitle(Titles.questCompleted);
                            MusicUtils.playQuestSound(player.getLocation());
                            player.getInventory().removeItem(new ItemStack(Material.LOG, 32));
                            player.getInventory().addItem(randomReward());
                        } else
                            player.sendMessage("Добудьте 32 штуки дуба.");
                        break;
                    case 1:
                        if (player.getInventory().contains(Material.COBBLESTONE, 64)) {
                            skyBlock.set(uuid + ".main.randomQuest", LiteGame.random.nextInt(7));
                            player.sendTitle(Titles.questCompleted);
                            MusicUtils.playQuestSound(player.getLocation());
                            player.getInventory().remove(new ItemStack(Material.COBBLESTONE, 64));
                            player.getInventory().addItem(randomReward());
                        } else
                            player.sendMessage("Добудьте 64 штуки булыжника.");
                        break;
                    case 2:
                        if (player.getInventory().contains(Material.CACTUS, 8)) {
                            skyBlock.set(uuid + ".main.randomQuest", LiteGame.random.nextInt(7));
                            player.sendTitle(Titles.questCompleted);
                            MusicUtils.playQuestSound(player.getLocation());
                            player.getInventory().remove(new ItemStack(Material.CACTUS, 8));
                            player.getInventory().addItem(randomReward());
                        } else
                            player.sendMessage("Добудьте 8 штук кактуса.");
                        break;
                    case 3:
                        if (player.getInventory().contains(Material.WOOD, 6)) {
                            skyBlock.set(uuid + ".main.randomQuest", LiteGame.random.nextInt(7));
                            player.sendTitle(Titles.questCompleted);
                            MusicUtils.playQuestSound(player.getLocation());
                            player.getInventory().remove(new ItemStack(Material.WOOD, 64));
                            player.getInventory().addItem(randomReward());
                        } else
                            player.sendMessage("Сделайте 64 штуки досок.");
                        break;
                    case 4:
                        if (player.getInventory().contains(Material.WHEAT, 16)) {
                            skyBlock.set(uuid + ".main.randomQuest", LiteGame.random.nextInt(7));
                            player.sendTitle(Titles.questCompleted);
                            MusicUtils.playQuestSound(player.getLocation());
                            player.getInventory().remove(new ItemStack(Material.WHEAT, 16));
                            player.getInventory().addItem(randomReward());
                        } else
                            player.sendMessage("Вырастите 16 штук пшеницы.");
                        break;
                    case 5:
                        if (player.getInventory().contains(Material.SAPLING, 8)) {
                            skyBlock.set(uuid + ".main.randomQuest", LiteGame.random.nextInt(7));
                            player.sendTitle(Titles.questCompleted);
                            MusicUtils.playQuestSound(player.getLocation());
                            player.getInventory().remove(new ItemStack(Material.SAPLING, 8));
                            player.getInventory().addItem(randomReward());
                        } else
                            player.sendMessage("Добудьте 8 штук саженцев.");
                        break;
                }
                break;
        }
    }

    public static ItemStack randomReward() {
        return rewards[LiteGame.random.nextInt(rewards.length)];
    }

    public static Inventory skyBlockQuests(String uuid) {
        Inventory menu = LiteGame.server.createInventory(null, 27, "Задания");

        ItemStack item = new ItemStack(Material.SAPLING);
        ItemMeta meta = item.getItemMeta();

        switch (skyBlock.getInt(uuid + ".quest")) {
            case 0:
                meta.setDisplayName("§fЯ крафтер. Скайкрафтер.");
                meta.setLore
                        (
                                Arrays.asList
                                        (
                                                "§7Начните свой путь квестов.",
                                                "§7Для выполнения квеста просто",
                                                "§7нажмите сюда.",
                                                "§fОбучение"
                                        )
                        );
                break;
            case 1:
                item.setType(Material.WORKBENCH);
                meta.setDisplayName("§fВерстак");
                meta.setLore
                        (
                                Arrays.asList
                                        (
                                                "§7Вы получили несколько",
                                                "§7досок. Сделайте верстак.",
                                                "§fОбучение"
                                        )
                        );
                break;
            case 2:
                item.setType(Material.WOOD_SPADE);
                meta.setDisplayName("§fЛопата");
                meta.setLore
                        (
                                Arrays.asList
                                        (
                                                "§7Сделайте лопату и держите",
                                                "§7её в инвентаре.",
                                                "§fОбучение"
                                        )
                        );
                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                break;
            case 3:
                item.setType(Material.WOOD_DOOR);
                meta.setDisplayName("§fПосторонним не входить!");
                meta.setLore
                        (
                                Arrays.asList
                                        (
                                                "§7Запретите всем входить на ваш",
                                                "§7остров через меню управления",
                                                "§7миром.",
                                                "§fОбучение"
                                        )
                        );
                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                break;
            case 4:
                item.setType(Material.DIRT);
                meta.setDisplayName("§fПерестройка");
                meta.setLore
                        (
                                Arrays.asList
                                        (
                                                "§7Сломайте 32 штуки земли,",
                                                "§7выполните задание и сделайте",
                                                "§7свою форму острова.",
                                                "§fОбучение"
                                        )
                        );
                break;
            case 5:
                item.setType(Material.COBBLESTONE);
                meta.setDisplayName("§fГенератор булыжника");
                meta.setLore
                        (
                                Arrays.asList
                                        (
                                                "§7Вы получили вёдра.",
                                                "§7Сделайте генератор",
                                                "§7булыжника и добудьте",
                                                "§732 штуки булыжника.",
                                                "§fОбучение"
                                        )
                        );
                break;
            case 6:
                item.setType(Material.WHEAT);
                meta.setDisplayName("§fОгород");
                meta.setLore
                        (
                                Arrays.asList
                                        (
                                                "§7Вы получили несколько семян.",
                                                "§7Сделайте огород и вырастите",
                                                "§7пшеницу.",
                                                "§fОбучение"
                                        )
                        );
                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                break;
            case 7:
                item.setType(Material.LOG);
                meta.setDisplayName("§fЛесоруб");
                meta.setLore
                        (
                                Arrays.asList
                                        (
                                                "§7Покажите, какой вы лесоруб,",
                                                "§7нарубите 32 штуки дуба.",
                                                "§fОбучение"
                                        )
                        );
                break;
            case 8:
                item.setType(Material.COBBLESTONE);
                meta.setDisplayName("§fБольше булыжника!");
                meta.setLore
                        (
                                Arrays.asList
                                        (
                                                "§7Добудьте 64 штуки",
                                                "§7булыжника.",
                                                "§fОбучение"
                                        )
                        );
                break;
            case 9:
                item.setType(Material.IRON_PICKAXE);
                meta.setDisplayName("§fЖелезо!");
                meta.setLore
                        (
                                Arrays.asList
                                        (
                                                "§7Вы получили немного железных",
                                                "§7слитков. Сделайте из них",
                                                "§71 железную кирку.",
                                                "§fОбучение"
                                        )
                        );
                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                break;
            case 10:
                item.setType(Material.COBBLESTONE);
                meta.setDisplayName("§fБольшие запасы");
                meta.setLore
                        (
                                Arrays.asList
                                        (
                                                "§7Добудьте 128 штук",
                                                "§7булыжника.",
                                                "§fОбучение"
                                        )
                        );
                break;
            case 11:
                item.setType(Material.CACTUS);
                meta.setDisplayName("§fКактусы");
                meta.setLore
                        (
                                Arrays.asList
                                        (
                                                "§7Посадите и вырастите",
                                                "§73 штуки кактуса.",
                                                "§fОбучение"
                                        )
                        );
                break;
            case 12:
                item.setType(Material.LOG);
                meta.setDisplayName("§fКакао-бобы");
                meta.setLore
                        (
                                Arrays.asList
                                        (
                                                "§7Посадите и вырастите",
                                                "§73 штуки какао-боба.",
                                                "§fОбучение"
                                        )
                        );
                break;
            case 13:
                item.setType(Material.COBBLESTONE);
                meta.setDisplayName("§fКонец?");
                meta.setLore
                        (
                                Arrays.asList
                                        (
                                                "§7Добудьте 256 штуки",
                                                "§7булыжника.",
                                                "§fОбучение"
                                        )
                        );
                break;
            default:
                switch (skyBlock.getInt(uuid + ".randomQuest")) {
                    default:
                        item.setType(Material.LOG);
                        meta.setDisplayName("§fЛесоруб");
                        meta.setLore
                                (
                                        Arrays.asList
                                                (
                                                        "§7Покажите, какой вы лесоруб,",
                                                        "§7нарубите 32 штуки дуба.",
                                                        "§fПродать"
                                                )
                                );
                        break;
                    case 1:
                        item.setType(Material.COBBLESTONE);
                        meta.setDisplayName("§fГенератор булыжника");
                        meta.setLore
                                (
                                        Arrays.asList
                                                (
                                                        "§7Добудьте 64 штуки",
                                                        "§7булыжника.",
                                                        "§fПродать"
                                                )
                                );
                        break;
                    case 2:
                        item.setType(Material.CACTUS);
                        meta.setDisplayName("§fКактусы");
                        meta.setLore
                                (
                                        Arrays.asList
                                                (
                                                        "§7Посадите и вырастите",
                                                        "§78 штук кактуса.",
                                                        "§fПродать"
                                                )
                                );
                        break;
                    case 3:
                        item.setType(Material.WOOD);
                        meta.setDisplayName("§fДоски");
                        meta.setLore
                                (
                                        Arrays.asList
                                                (
                                                        "§7Переработайте из",
                                                        "§7древесины 64 штуки",
                                                        "§7досок.",
                                                        "§fПродать"
                                                )
                                );
                        break;
                    case 4:
                        item.setType(Material.WHEAT);
                        meta.setDisplayName("§fОгород");
                        meta.setLore
                                (
                                        Arrays.asList
                                                (
                                                        "§7Вырастите 8 штук",
                                                        "§7пшеницы.",
                                                        "§fПродать"
                                                )
                                );
                        break;
                    case 5:
                        item.setType(Material.SAPLING);
                        meta.setDisplayName("§fЗапасы");
                        meta.setLore
                                (
                                        Arrays.asList
                                                (
                                                        "§7Добудьте 8 штук",
                                                        "§7саженцев.",
                                                        "§fПродать"
                                                )
                                );
                        break;
                }
                break;
        }

        item.setItemMeta(meta);
        menu.setItem(13, item);

        return menu;
    }

    public static void saveSkyBlock() throws IOException {
        skyBlock.save(skyBlockFile);
    }
}