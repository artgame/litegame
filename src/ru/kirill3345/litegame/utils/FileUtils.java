package ru.kirill3345.litegame.utils;

import java.io.File;
import java.io.IOException;

import ru.kirill3345.litegame.coding.CodingUtils;
import ru.kirill3345.litegame.utils.world.Hub;
import ru.kirill3345.litegame.utils.world.WorldManager;

public class FileUtils {
    public static void deleteDirectory(File file) {
        if (file.isDirectory()) {
            for (File files : file.listFiles()) {
                if (files.isDirectory())
                    deleteDirectory(files);
                else
                    files.delete();
            }
        }
        file.delete();
    }

    public static void saveAllConfigs() {
        try {
            PlayersUtils.savePlayers();
            WorldManager.saveHash();
            ArtStudioUtils.saveScreens();
            SkyBlockUtils.saveSkyBlock();
            CodingUtils.saveCoding();
        } catch (IOException ex) {
        }
    }

    public static void removePlayersFiles() {
        {
            File playersData = new File(WorldManager.worldContainer, Hub.name + File.pathSeparator + "playerdata");
            if (playersData.isDirectory())
                deleteDirectory(playersData);
        }
        {
            File logsFolder = new File(WorldManager.worldContainer, "logs");
            if (logsFolder.isDirectory()) {
                for (File files : logsFolder.listFiles()) {
                    if (!files.getName().equals("latest.log"))
                        files.delete();
                }
            }
        }
        {
            File usersCache = new File(WorldManager.worldContainer, "usercache.json");
            if (usersCache.isFile())
                usersCache.delete();
        }
    }
}