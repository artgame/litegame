package ru.kirill3345.litegame.utils;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import ru.kirill3345.litegame.LiteGame;
import ru.kirill3345.litegame.utils.world.WorldManager;

public class MusicUtils {
    public static String[] musicList = new String[]
            {
                    "record.cat",
                    "record.chirp",
                    "record.far",
                    "record.mall",
                    "record.stal",
                    "record.strad",
                    "record.wait",
                    "music.credits"
            };

    public static String randomMusic() {
        return musicList[LiteGame.random.nextInt(musicList.length)];
    }

    public static void playMusic(World world, String music) {
        Location spawnLocation = world.getSpawnLocation();
        if (!WorldManager.hasMusic(world.getName())) {
            WorldManager.setMusic(world.getName(), music);
            world.playSound(spawnLocation, music, Integer.MAX_VALUE, getMusicPitch(music));
        } else {
            String playingMusic = WorldManager.getMusic(world.getName());
            WorldManager.setMusic(world.getName(), music);
            world.getPlayers().forEach(players -> players.stopSound(playingMusic));
            world.playSound(spawnLocation, music, Integer.MAX_VALUE, getMusicPitch(music));
        }
    }

    public static void stopMusic(World world) {
        if (WorldManager.hasMusic(world.getName())) {
            String playingMusic = WorldManager.getMusic(world.getName());
            WorldManager.setMusic(world.getName(), null);
            world.getPlayers().forEach(players -> players.stopSound(playingMusic));
        }
    }

    public static float getMusicPitch(String music) {
        switch (music) {
            case "music.credits":
                return 2;
            default:
                return 1;
        }
    }

    public static void playQuestSound(Location location) {
        location.getWorld().playSound(location, Sound.ENTITY_PLAYER_LEVELUP, 1, 2);
        location.getWorld().playSound(location, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 2);
        location.getWorld().playSound(location, Sound.ENTITY_PLAYER_LEVELUP, 1, 1.5F);
        location.getWorld().playSound(location, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1.5F);
        location.getWorld().playSound(location, Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
        location.getWorld().playSound(location, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
        location.getWorld().playSound(location, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 0.5F);
        location.getWorld().playSound(location, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 0.1F);
    }
}