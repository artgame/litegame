package ru.kirill3345.litegame.utils;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;
import ru.kirill3345.litegame.LiteGame;

public class LocationUtils {
    public static Vector noVelocity = new Vector(0, 0, 0);

    public static Location randomLocation(World world) {
        Location location = randomizeLocation(world);
        for (int i = 0; i < 16; i++) {
            if (location.getBlockY() != 0)
                return location;
            else
                location = randomizeLocation(world);
        }
        return location;
    }

    public static Location randomizeLocation(World world) {
        int worldSize = (int) world.getWorldBorder().getSize() / 2;
        int sizeX = worldSize / 2;
        return world.getHighestBlockAt
                        (
                                -LiteGame.random.nextInt(sizeX) + LiteGame.random.nextInt(worldSize),
                                -LiteGame.random.nextInt(sizeX) + LiteGame.random.nextInt(worldSize)
                        )
                .getLocation().add(0.5, 0, 0.5);
    }
}