package ru.kirill3345.litegame;

import com.google.common.collect.Lists;

import java.io.File;
import java.util.List;
import java.util.Random;

import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.command.CommandMap;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import ru.kirill3345.litegame.coding.CodingUtils;
import ru.kirill3345.litegame.commands.*;
import ru.kirill3345.litegame.listeners.*;
import ru.kirill3345.litegame.listeners.FlyHackListener;
import ru.kirill3345.litegame.utils.*;
import ru.kirill3345.litegame.utils.world.Hub;
import ru.kirill3345.litegame.utils.world.RealTimeUtils;
import ru.kirill3345.litegame.utils.world.WorldManager;

public class LiteGame extends JavaPlugin {
    public static Server server;
    public static Plugin plugin;
    public static File dataFolder;
    public static Random random;
    public static List<String> emptyList;

    @Override
    public void onEnable() {
        server = getServer();
        plugin = this;
        dataFolder = getDataFolder();
        random = new Random();
        emptyList = Lists.newArrayList();
        FileUtils.removePlayersFiles();
        WorldManager.clearHash();
        server.getScheduler().scheduleSyncDelayedTask(this, () -> server.getScheduler().scheduleSyncRepeatingTask(this, () -> FileUtils.saveAllConfigs(), 0, 6000), 6000);
        Hub.world.setPVP(true);
        Hub.world.setAutoSave(false);
        Hub.world.setGameRuleValue("doDaylightCycle", "false");
        server.getScheduler().scheduleSyncRepeatingTask(this, () -> Hub.world.setTime(RealTimeUtils.generate(ServerSettings.timeZone)), 0, 1200);
        registerCommands();
        server.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        server.getMessenger().registerIncomingPluginChannel(this, "WDL|INIT", new WorldDownloaderListener());
        if (ServerSettings.antiCheatFlyHack)
            server.getScheduler().scheduleSyncRepeatingTask(this, new FlyHackListener(), 0, 50);
        server.getOnlinePlayers().forEach(players ->
        {
            String uuid = players.getUniqueId().toString();
            if (PlayersUtils.getIp(uuid).equals(players.getAddress().getAddress().getHostAddress()))
                PlayersUtils.login(uuid);
        });
        for (World worlds : server.getWorlds()) {
            switch (WorldManager.getType(worlds.getName())) {
                case 18: {
                    CodingUtils.createParser(worlds);
                    break;
                }
            }
        }
        server.getPluginManager().registerEvents(new EventListener(), this);
        server.broadcastMessage("§8»§r Сервер перезагружен.");
    }

    @Override
    public void onDisable() {
        server.broadcastMessage("§8»§r Сервер перезагружается...");
        FileUtils.saveAllConfigs();
        FileUtils.removePlayersFiles();
    }

    public void registerCommands() {
        CommandMap commandMap = server.getCommandMap();
        String prefix = "litegame";
        commandMap.clearCommands();
        commandMap.register(prefix, new PasswordCommand());
        commandMap.register(prefix, new ListCommand());
        commandMap.register(prefix, new HubCommand());
        commandMap.register(prefix, new GiveDonateCommand());
        commandMap.register(prefix, new SayCommand());
        commandMap.register(prefix, new TellCommand());
        commandMap.register(prefix, new FindCommand());
        commandMap.register(prefix, new HypeCommand());
        commandMap.register(prefix, new PlayerInfoCommand());
        commandMap.register(prefix, new PingCommand());
        commandMap.register(prefix, new WipeWorldsCommand());
        commandMap.register(prefix, new TpsCommand());
        commandMap.register(prefix, new TpWorldCommand());
        commandMap.register(prefix, new UnloadWorldCommand());
        commandMap.register(prefix, new BanCommand());
        commandMap.register(prefix, new PardonCommand());
        commandMap.register(prefix, new BanIpCommand());
        commandMap.register(prefix, new PardonIpCommand());
        commandMap.register(prefix, new KitCommand());
        commandMap.register(prefix, new SettingsCommand());
        commandMap.register(prefix, new InfoCommand());
        commandMap.register(prefix, new RemovePasswordCommand());
        commandMap.register(prefix, new RemoveDonateCommand());
        commandMap.register(prefix, new UpdateCodeCommand());
    }
}