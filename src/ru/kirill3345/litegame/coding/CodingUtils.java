package ru.kirill3345.litegame.coding;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.potion.PotionEffectType;
import ru.kirill3345.litegame.LiteGame;

public class CodingUtils {
    private static final File codingFile = new File(LiteGame.dataFolder, "coding.yml");
    private static final YamlConfiguration coding = YamlConfiguration.loadConfiguration(codingFile);
    private static final HashMap<World, CodingParser> codingWorlds = Maps.newHashMap();
    private static final List<String> emptyCode = Arrays.asList
            (
                    "-PlayerJoinEvent;"
                            + "\n" +
                            "player::sendMessage::Пример и гайд;"
                            + "\n" +
                            "player::sendMessage::vk.com/litegamemc"
            );

    public static CodingParser createParser(World world) {
        return codingWorlds.put(world, new CodingParser(getCode(world.getName()), world));
    }

    public static CodingParser getParser(World world) {
        return codingWorlds.get(world);
    }

    public static void removeParser(World world) {
        codingWorlds.remove(world);
    }

    public static void saveCode(String uuid, List<String> code) {
        coding.set(uuid + ".code", !code.isEmpty() ? code : null);
    }

    public static void setNoErrors(String uuid) {
        coding.set(uuid + ".noErrors", true);
    }

    public static boolean isNoErrors(String uuid) {
        return coding.getBoolean(uuid + ".noErrors", false);
    }

    public static void removeNoErrors(String uuid) {
        coding.set(uuid + ".noErrors", null);
    }

    public static boolean saveAndCheckForError(Player player, List<String> code) {
        List<String> list = Lists.newArrayList();
        boolean noErrors = true;
        for (String pages : code) {
            pages = pages.replace("§0", "").replace("§f", "");
            if (!pages.isEmpty()) {
                if (!checkForErrors(player, pages))
                    noErrors = false;
            }
            list.add(pages);
        }
        CodingUtils.saveCode(player.getUniqueId().toString(), list);
        return noErrors;
    }

    public static boolean checkForErrors(Player player, String code) {
        String[] lines = code.replace("\n", "").split(";");
        switch (lines[0]) {
            case "-PlayerJoinEvent":
            case "-PlayerInteractEvent.LEFT":
            case "-PlayerInteractEvent.RIGHT":
            case "-PlayerInteractEvent.PHYSICAL":
            case "-PlayerDeathEvent":
            case "-PlayerQuitEvent": {
                if (lines.length == 1) {
                    CodingUtils.throwException("Нет действий.", player);
                    return false;
                } else if (code.endsWith(";")) {
                    CodingUtils.throwException("Знак§l ;§r в конце страницы не нужен.", player);
                    return false;
                } else {
                    for (String strings : (String[]) ArrayUtils.remove(lines, 0)) {
                        String[] split = strings.split("::");
                        switch (split[0]) {
                            case "player": {
                                switch (split.length) {
                                    case 2: {
                                        switch (split[1]) {
                                            case "clearInventory":
                                            case "removeAllPotions":
                                                break;
                                            default: {
                                                CodingUtils.throwException("Метод «" + strings + "» не найден.", player);
                                                return false;
                                            }
                                        }
                                        break;
                                    }
                                    case 3: {
                                        switch (split[1]) {
                                            case "sendMessage":
                                            case "sendActionBar":
                                                break;
                                            case "addItem":
                                            case "removeItem": {
                                                try {
                                                    Material.getMaterial(Integer.valueOf(split[2]));
                                                    break;
                                                } catch (NullPointerException ex) {
                                                    CodingUtils.throwException("ID «" + strings + "» не найден.", player);
                                                    return false;
                                                } catch (NumberFormatException ex) {
                                                    CodingUtils.throwException("Метод «" + strings + "» должен иметь числа.", player);
                                                    return false;
                                                }
                                            }
                                            case "shoot": {
                                                switch (split[2]) {
                                                    case "arrow":
                                                    case "egg":
                                                    case "snowball":
                                                    case "fireball":
                                                        break;
                                                    default: {
                                                        CodingUtils.throwException("Снаряд «" + split[2] + "» не найден.", player);
                                                        return false;
                                                    }
                                                }
                                                break;
                                            }
                                            case "setGameMode": {
                                                switch (split[2]) {
                                                    case "0":
                                                    case "1":
                                                    case "2":
                                                    case "3":
                                                        break;
                                                    default: {
                                                        CodingUtils.throwException("Игровой режим «" + strings + "» не найден.", player);
                                                        return false;
                                                    }
                                                }
                                                break;
                                            }
                                            case "removePotion": {
                                                try {
                                                    PotionEffectType.getById(Integer.valueOf(split[2]));
                                                    break;
                                                } catch (NullPointerException ex) {
                                                    CodingUtils.throwException("ID «" + strings + "» не найден.", player);
                                                    return false;
                                                } catch (NumberFormatException ex) {
                                                    CodingUtils.throwException("Метод " + strings + "» должен иметь числа.", player);
                                                    return false;
                                                }
                                            }
                                            case "teleport": {
                                                switch (split[2]) {
                                                    case "spawn":
                                                    case "hub":
                                                        break;
                                                    default: {
                                                        CodingUtils.throwException("Метод «" + strings + "» не найден.", player);
                                                        return false;
                                                    }
                                                }
                                                break;
                                            }
                                            default: {
                                                CodingUtils.throwException("Метод «" + strings + "» не найден.", player);
                                                return false;
                                            }
                                        }
                                        break;
                                    }
                                    case 4: {
                                        switch (split[1]) {
                                            case "addItem":
                                            case "removeItem":
                                            case "setCooldown": {
                                                try {
                                                    Material.getMaterial(Integer.valueOf(split[2]));
                                                    Integer.valueOf(split[3]);
                                                    break;
                                                } catch (NullPointerException ex) {
                                                    CodingUtils.throwException("ID «" + strings + "» не найден.", player);
                                                    return false;
                                                } catch (NumberFormatException ex) {
                                                    CodingUtils.throwException("Метод «" + strings + "» должен иметь числа.", player);
                                                    return false;
                                                }
                                            }
                                            case "sendTitle":
                                                break;
                                        }
                                        break;
                                    }
                                    case 5: {
                                        switch (split[1]) {
                                            case "teleport": {
                                                try {
                                                    Double.valueOf(split[2]);
                                                    Double.valueOf(split[3]);
                                                    Double.valueOf(split[4]);
                                                    break;
                                                } catch (NumberFormatException ex) {
                                                    CodingUtils.throwException("Метод «" + strings + "» должен иметь числа.", player);
                                                    return false;
                                                }
                                            }
                                            case "addItem":
                                            case "removeItem": {
                                                try {
                                                    Material.getMaterial(Integer.valueOf(split[2]));
                                                    Integer.valueOf(split[3]);
                                                    Byte.valueOf(split[4]);
                                                    break;
                                                } catch (NullPointerException ex) {
                                                    CodingUtils.throwException("ID «" + strings + "» не найден.", player);
                                                    return false;
                                                } catch (NumberFormatException ex) {
                                                    CodingUtils.throwException("Метод «" + strings + "» должен иметь числа.", player);
                                                    return false;
                                                }
                                            }
                                        }
                                        break;
                                    }
                                    case 6: {
                                        switch (split[1]) {
                                            case "effect": {
                                                switch (split[5]) {
                                                    case "true":
                                                    case "false": {
                                                        try {
                                                            PotionEffectType.getById(Integer.valueOf(split[2]));
                                                            Integer.valueOf(split[3]);
                                                            Integer.valueOf(split[4]);
                                                            break;
                                                        } catch (NullPointerException ex) {
                                                            CodingUtils.throwException("ID «" + strings + "» не найден.", player);
                                                            return false;
                                                        } catch (NumberFormatException ex) {
                                                            CodingUtils.throwException("Метод «" + strings + "» должен иметь числа.", player);
                                                            return false;
                                                        }
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                    case 7: {
                                        switch (split[1]) {
                                            case "sendTitle": {
                                                try {
                                                    Integer.valueOf(split[4]);
                                                    Integer.valueOf(split[5]);
                                                    Integer.valueOf(split[6]);
                                                    break;
                                                } catch (NumberFormatException ex) {
                                                    CodingUtils.throwException("Метод «" + strings + "» должен иметь числа.", player);
                                                    return false;
                                                }
                                            }
                                        }
                                        break;
                                    }
                                    default: {
                                        CodingUtils.throwException("Метод «" + strings + "» не найден.", player);
                                        return false;
                                    }
                                }
                                break;
                            }
                            case "world": {
                                switch (split.length) {
                                    case 3: {
                                        switch (split[1]) {
                                            case "sendMessage":
                                            case "sendActionBar":
                                                break;
                                            case "setPvp": {
                                                switch (split[2]) {
                                                    case "true":
                                                    case "false":
                                                        break;
                                                    default: {
                                                        CodingUtils.throwException("Метод «" + strings + "» может иметь только значения: true/false", player);
                                                        return false;
                                                    }
                                                }
                                                break;
                                            }
                                            default: {
                                                CodingUtils.throwException("Метод «" + strings + "» не найден.", player);
                                                return false;
                                            }
                                        }
                                        break;
                                    }
                                    default: {
                                        CodingUtils.throwException("Метод «" + strings + "» не найден.", player);
                                        return false;
                                    }
                                }
                                break;
                            }
                            case "if":
                            case "!if": {
                                switch (split.length) {
                                    case 3: {
                                        switch (split[1]) {
                                            case "player": {
                                                switch (split[2]) {
                                                    case "isRegistered":
                                                    case "isLoggined":
                                                    case "isDonator":
                                                    case "isSponsor":
                                                    case "isFavorite":
                                                    case "isModer":
                                                    case "isOwner":
                                                        break;
                                                    default: {
                                                        CodingUtils.throwException("Метод «" + strings + "» не найден.", player);
                                                        return false;
                                                    }
                                                }
                                                break;
                                            }
                                            default: {
                                                CodingUtils.throwException("Метод «" + strings + "» не найден.", player);
                                                return false;
                                            }
                                        }
                                        break;
                                    }
                                    case 4: {
                                        switch (split[1]) {
                                            case "player": {
                                                switch (split[2]) {
                                                    case "nameIs":
                                                        break;
                                                    case "hasItem":
                                                    case "isInHand": {
                                                        try {
                                                            Material.getMaterial(Integer.valueOf(split[3]));
                                                            break;
                                                        } catch (NullPointerException ex) {
                                                            CodingUtils.throwException("ID «" + strings + "» не найден.", player);
                                                            return false;
                                                        } catch (NumberFormatException ex) {
                                                            CodingUtils.throwException("Метод «" + strings + "» должен иметь числа.", player);
                                                            return false;
                                                        }
                                                    }
                                                    default: {
                                                        CodingUtils.throwException("Метод «" + strings + "» не найден.", player);
                                                        return false;
                                                    }
                                                }
                                                break;
                                            }
                                            default: {
                                                CodingUtils.throwException("Метод «" + strings + "» не найден.", player);
                                                return false;
                                            }
                                        }
                                        break;
                                    }
                                    case 5: {
                                        switch (split[1]) {
                                            case "player": {
                                                switch (split[2]) {
                                                    case "hasItem":
                                                    case "isInHand":
                                                    case "getCooldown": {
                                                        try {
                                                            Material.getMaterial(Integer.valueOf(split[3]));
                                                            Integer.valueOf(split[4]);
                                                            break;
                                                        } catch (NullPointerException ex) {
                                                            CodingUtils.throwException("ID «" + strings + "» не найден.", player);
                                                            return false;
                                                        } catch (NumberFormatException ex) {
                                                            CodingUtils.throwException("Метод «" + strings + "» должен иметь числа.", player);
                                                            return false;
                                                        }
                                                    }
                                                    default: {
                                                        CodingUtils.throwException("Метод «" + strings + "» не найден.", player);
                                                        return false;
                                                    }
                                                }
                                                break;
                                            }
                                            default: {
                                                CodingUtils.throwException("Метод «" + strings + "» не найден.", player);
                                                return false;
                                            }
                                        }
                                        break;
                                    }
                                    case 6: {
                                        switch (split[1]) {
                                            case "player": {
                                                switch (split[2]) {
                                                    case "isInHand": {
                                                        try {
                                                            Material.getMaterial(Integer.valueOf(split[3]));
                                                            Integer.valueOf(split[4]);
                                                            Byte.valueOf(split[5]);
                                                            break;
                                                        } catch (NullPointerException ex) {
                                                            CodingUtils.throwException("ID «" + strings + "» не найден.", player);
                                                            return false;
                                                        } catch (NumberFormatException ex) {
                                                            CodingUtils.throwException("Метод «" + strings + "» должен иметь числа.", player);
                                                            return false;
                                                        }
                                                    }
                                                    default: {
                                                        CodingUtils.throwException("Метод «" + strings + "» не найден.", player);
                                                        return false;
                                                    }
                                                }
                                                break;
                                            }
                                            default: {
                                                CodingUtils.throwException("Метод «" + strings + "» не найден.", player);
                                                return false;
                                            }
                                        }
                                        break;
                                    }
                                    default: {
                                        CodingUtils.throwException("Метод «" + strings + "» не найден.", player);
                                        return false;
                                    }
                                }
                                break;
                            }
                            default: {
                                CodingUtils.throwException("Метод «" + strings + "» не найден.", player);
                                return false;
                            }
                        }
                    }
                }
                return true;
            }
            default: {
                CodingUtils.throwException("Событие «" + lines[0] + "» не найдено.", player);
                return false;
            }
        }
    }

    public static List<String> getCode(String uuid) {
        List<String> code = coding.getStringList(uuid);
        if (!code.isEmpty()) {
            coding.set(uuid, null);
            coding.set(uuid + ".code", code);
            return code;
        } else if (coding.contains(uuid + ".code"))
            return coding.getStringList(uuid + ".code");
        else
            return emptyCode;
    }

    public static void removeCode(String uuid) {
        coding.set(uuid, null);
    }

    public static ItemStack generateBook(String uuid) {
        ItemStack item = new ItemStack(Material.BOOK_AND_QUILL);
        BookMeta meta = (BookMeta) item.getItemMeta();
        List<String> code = getCode(uuid);
        meta.setDisplayName("§fМой код");
        meta.setPages(!code.isEmpty() ? code : emptyCode);
        item.setItemMeta(meta);
        return item;
    }

    public static void throwWarning(String message, Player player) {
        player.sendMessage("§eПредупреждение:§r " + message);
    }

    public static void throwException(String message, Player player) {
        player.sendMessage("§4Ошибка:§r " + message);
    }

    public static void saveCoding() throws IOException {
        coding.save(codingFile);
    }
}