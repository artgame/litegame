package ru.kirill3345.litegame.coding;

import com.google.common.collect.Lists;

import java.util.List;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import ru.kirill3345.litegame.utils.ChatUtils;
import ru.kirill3345.litegame.utils.PlayersUtils;
import ru.kirill3345.litegame.utils.world.Hub;

public final class CodingParser {
    private final World world;
    private List<List<String>> playerJoinEvent = Lists.newArrayList();
    private List<List<String>> playerLeftInteractEvent = Lists.newArrayList();
    private List<List<String>> playerRightInteractEvent = Lists.newArrayList();
    private List<List<String>> playerPhysicalInteractEvent = Lists.newArrayList();
    private List<List<String>> playerDeathEvent = Lists.newArrayList();
    private List<List<String>> playerQuitEvent = Lists.newArrayList();

    public CodingParser(List<String> list, World world) {
        this.world = world;
        for (String strings : list) {
            List<String> actions = Lists.newArrayList(strings.replace("\n", "").split(";"));
            int actionsSize = actions.size() - 1;
            if (actionsSize != 0) {
                String eventName = actions.get(0);
                switch (eventName) {
                    case "-PlayerJoinEvent": {
                        actions.remove(eventName);
                        playerJoinEvent.add(actions);
                        break;
                    }
                    case "-PlayerInteractEvent.LEFT": {
                        actions.remove(eventName);
                        playerLeftInteractEvent.add(actions);
                        break;
                    }
                    case "-PlayerInteractEvent.RIGHT": {
                        actions.remove(eventName);
                        playerRightInteractEvent.add(actions);
                        break;
                    }
                    case "-PlayerInteractEvent.PHYSICAL": {
                        actions.remove(eventName);
                        playerPhysicalInteractEvent.add(actions);
                        break;
                    }
                    case "-PlayerDeathEvent": {
                        actions.remove(eventName);
                        playerDeathEvent.add(actions);
                        break;
                    }
                    case "-PlayerQuitEvent": {
                        actions.remove(eventName);
                        playerQuitEvent.add(actions);
                        break;
                    }
                }
            }
        }
    }

    public void onPlayerJoin(PlayerChangedWorldEvent event) {
        playerJoinEvent.forEach(lists -> runAction(lists, event));
    }

    public void onPlayerLeftInteract(PlayerInteractEvent event) {
        playerLeftInteractEvent.forEach(lists -> runAction(lists, event));
    }

    public void onPlayerRightInteract(PlayerInteractEvent event) {
        playerRightInteractEvent.forEach(lists -> runAction(lists, event));
    }

    public void onPlayerPhysicalInteract(PlayerInteractEvent event) {
        playerPhysicalInteractEvent.forEach(lists -> runAction(lists, event));
    }

    public void onPlayerDeath(PlayerDeathEvent event) {
        playerDeathEvent.forEach(lists -> runAction
                (
                        lists,
                        new PlayerEvent(event.getEntity()) {
                            @Override
                            public HandlerList getHandlers() {
                                return event.getHandlers();
                            }
                        }
                ));
    }

    public void onPlayerQuit(PlayerChangedWorldEvent event) {
        playerQuitEvent.forEach(lists -> runAction(lists, event));
    }

    public void runAction(List<String> list, PlayerEvent event) {
        for (String actions : list) {
            if (!parseAction(event, actions))
                return;
        }
    }

    private boolean parseAction(PlayerEvent event, String action) {
        String[] split = action.split("::");
        switch (split[0]) {
            case "player": {
                switch (split.length) {
                    case 2: {
                        switch (split[1]) {
                            case "clearInventory": {
                                event.getPlayer().getInventory().clear();
                                return true;
                            }
                            case "removeAllPotions": {
                                Player player = event.getPlayer();
                                player.getActivePotionEffects().forEach(potions -> player.removePotionEffect(potions.getType()));
                                return true;
                            }
                            default:
                                return false;
                        }
                    }
                    case 3: {
                        switch (split[1]) {
                            case "sendMessage": {
                                event.getPlayer().sendMessage(split[2].replace("%name%", event.getPlayer().getDisplayName() + "§r"));
                                return true;
                            }
                            case "sendActionBar": {
                                event.getPlayer().sendActionBar(split[2].replace("%name%", event.getPlayer().getDisplayName() + "§r"));
                                return true;
                            }
                            case "addItem": {
                                event.getPlayer().getInventory().addItem(new ItemStack(Integer.valueOf(split[2])));
                                return true;
                            }
                            case "removeItem": {
                                event.getPlayer().getInventory().removeItem(new ItemStack(Integer.valueOf(split[2])));
                                return true;
                            }
                            case "shoot": {
                                switch (split[2]) {
                                    case "arrow": {
                                        event.getPlayer().launchProjectile(Arrow.class);
                                        return true;
                                    }
                                    case "egg": {
                                        event.getPlayer().launchProjectile(Egg.class);
                                        return true;
                                    }
                                    case "snowball": {
                                        event.getPlayer().launchProjectile(Snowball.class);
                                        return true;
                                    }
                                    case "fireball": {
                                        event.getPlayer().launchProjectile(Fireball.class);
                                        return true;
                                    }
                                    default:
                                        return false;
                                }
                            }
                            case "setGameMode": {
                                switch (split[2]) {
                                    case "0": {
                                        event.getPlayer().setGameMode(GameMode.SURVIVAL);
                                        return true;
                                    }
                                    case "1": {
                                        event.getPlayer().setGameMode(GameMode.CREATIVE);
                                        return true;
                                    }
                                    case "2": {
                                        event.getPlayer().setGameMode(GameMode.ADVENTURE);
                                        return true;
                                    }
                                    case "3": {
                                        event.getPlayer().setGameMode(GameMode.SPECTATOR);
                                        return true;
                                    }
                                    default:
                                        return false;
                                }
                            }
                            case "removePotion": {
                                event.getPlayer().removePotionEffect(PotionEffectType.getById(Integer.valueOf(split[2])));
                                return true;
                            }
                            case "teleport": {
                                switch (split[2]) {
                                    case "spawn": {
                                        event.getPlayer().teleport(world.getSpawnLocation());
                                        return true;
                                    }
                                    case "hub": {
                                        event.getPlayer().teleport(Hub.location);
                                        return true;
                                    }
                                }
                                return true;
                            }
                            default:
                                return false;
                        }
                    }
                    case 4: {
                        switch (split[1]) {
                            case "addItem": {
                                event.getPlayer().getInventory().addItem(new ItemStack(Integer.valueOf(split[2]), Integer.valueOf(split[3])));
                                return true;
                            }
                            case "removeItem": {
                                event.getPlayer().getInventory().removeItem(new ItemStack(Integer.valueOf(split[2]), Integer.valueOf(split[3])));
                                return true;
                            }
                            case "setCooldown": {
                                event.getPlayer().setCooldown(Material.getMaterial(Integer.valueOf(split[2])), Integer.valueOf(split[3]));
                                return true;
                            }
                            case "sendTitle": {
                                event.getPlayer().sendTitle(split[2].replace("%name%", event.getPlayer().getDisplayName() + "§r"), split[3].replace("%name%", event.getPlayer().getDisplayName() + "§r"));
                                return true;
                            }
                            default:
                                return false;
                        }
                    }
                    case 5: {
                        switch (split[1]) {
                            case "teleport": {
                                event.getPlayer().teleport(new Location(world, Double.valueOf(split[2]), Double.valueOf(split[3]), Double.valueOf(split[4])));
                                return true;
                            }
                            case "addItem": {
                                event.getPlayer().getInventory().addItem(new ItemStack(Integer.valueOf(split[2]), Integer.valueOf(split[3]), Byte.valueOf(split[4])));
                                return true;
                            }
                            case "removeItem": {
                                event.getPlayer().getInventory().removeItem(new ItemStack(Integer.valueOf(split[2]), Integer.valueOf(split[3]), Byte.valueOf(split[4])));
                                return true;
                            }
                            default:
                                return false;
                        }
                    }
                    case 6: {
                        switch (split[1]) {
                            case "effect": {
                                switch (split[5]) {
                                    case "true": {
                                        event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.getById(Integer.valueOf(split[2])), Integer.valueOf(split[3]), Integer.valueOf(split[4]), true, true));
                                        return true;
                                    }
                                    case "false": {
                                        event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.getById(Integer.valueOf(split[2])), Integer.valueOf(split[3]), Integer.valueOf(split[4]), true, false));
                                        return true;
                                    }
                                    default:
                                        return false;
                                }
                            }
                        }
                    }
                    case 7: {
                        switch (split[1]) {
                            case "sendTitle": {
                                event.getPlayer().sendTitle(split[2].replace("%name%", event.getPlayer().getDisplayName() + "§r"), split[3].replace("%name%", event.getPlayer().getDisplayName() + "§r"), Integer.valueOf(split[4]), Integer.valueOf(split[5]), Integer.valueOf(split[6]));
                                return true;
                            }
                            default:
                                return false;
                        }
                    }
                    default:
                        return false;
                }
            }
            case "world": {
                switch (split.length) {
                    case 3: {
                        switch (split[1]) {
                            case "sendMessage": {
                                ChatUtils.broadcastMessage(split[2].replace("%name%", event.getPlayer().getDisplayName() + "§r"), world.getPlayers());
                                return true;
                            }
                            case "sendActionBar": {
                                ChatUtils.broadcastActionBar(split[2].replace("%name%", event.getPlayer().getDisplayName() + "§r"), world.getPlayers());
                                return true;
                            }
                            case "setPvp": {
                                switch (split[2]) {
                                    case "true": {
                                        world.setPVP(true);
                                        return true;
                                    }
                                    case "false": {
                                        world.setPVP(false);
                                        return true;
                                    }
                                    default:
                                        return false;
                                }
                            }
                            default:
                                return false;
                        }
                    }
                    default:
                        return false;
                }
            }
            case "if": {
                switch (split.length) {
                    case 3: {
                        switch (split[1]) {
                            case "player": {
                                switch (split[2]) {
                                    case "isRegistered":
                                        return PlayersUtils.isRegistered(event.getPlayer().getUniqueId().toString());
                                    case "isLoggined":
                                        return PlayersUtils.isLoggined(event.getPlayer().getUniqueId().toString());
                                    case "isDonator":
                                        return PlayersUtils.isDonator(event.getPlayer().getUniqueId().toString());
                                    case "isSponsor":
                                        return PlayersUtils.isSponsor(event.getPlayer().getUniqueId().toString());
                                    case "isFavorite":
                                        return PlayersUtils.isFavorite(event.getPlayer().getUniqueId().toString());
                                    case "isModer":
                                        return PlayersUtils.isModer(event.getPlayer().getUniqueId().toString());
                                    case "isOwner":
                                        return PlayersUtils.isOwner(event.getPlayer().getUniqueId().toString());
                                }
                            }
                            default:
                                return false;
                        }
                    }
                    case 4: {
                        switch (split[1]) {
                            case "player": {
                                switch (split[2]) {
                                    case "nameIs":
                                        return event.getPlayer().getName().equals(split[3]);
                                    case "hasItem":
                                        return event.getPlayer().getInventory().contains(Integer.valueOf(split[3]));
                                    case "isInHand":
                                        return event.getPlayer().getInventory().getItemInMainHand().equals(new ItemStack(Integer.valueOf(split[3])));
                                    default:
                                        return false;
                                }
                            }
                            default:
                                return false;
                        }
                    }
                    case 5: {
                        switch (split[1]) {
                            case "player": {
                                switch (split[2]) {
                                    case "hasItem":
                                        return event.getPlayer().getInventory().contains(Integer.valueOf(split[3]), Integer.valueOf(split[4]));
                                    case "isInHand":
                                        return event.getPlayer().getInventory().getItemInMainHand().equals(new ItemStack(Integer.valueOf(split[3]), Integer.valueOf(split[4])));
                                    case "getCooldown":
                                        return event.getPlayer().getCooldown(Material.getMaterial(Integer.valueOf(split[3]))) == Integer.valueOf(split[4]);
                                    default:
                                        return false;
                                }
                            }
                            default:
                                return false;
                        }
                    }
                    case 6: {
                        switch (split[1]) {
                            case "player": {
                                switch (split[2]) {
                                    case "isInHand":
                                        return event.getPlayer().getInventory().getItemInMainHand().equals(new ItemStack(Integer.valueOf(split[3]), Integer.valueOf(split[4]), Byte.valueOf(split[5])));
                                    default:
                                        return false;
                                }
                            }
                            default:
                                return false;
                        }
                    }
                    default:
                        return false;
                }
            }
            case "!if": {
                switch (split.length) {
                    case 3: {
                        switch (split[1]) {
                            case "player": {
                                switch (split[2]) {
                                    case "isRegistered":
                                        return !PlayersUtils.isRegistered(event.getPlayer().getUniqueId().toString());
                                    case "isLoggined":
                                        return !PlayersUtils.isLoggined(event.getPlayer().getUniqueId().toString());
                                    case "isDonator":
                                        return !PlayersUtils.isDonator(event.getPlayer().getUniqueId().toString());
                                    case "isSponsor":
                                        return !PlayersUtils.isSponsor(event.getPlayer().getUniqueId().toString());
                                    case "isFavorite":
                                        return !PlayersUtils.isFavorite(event.getPlayer().getUniqueId().toString());
                                    case "isModer":
                                        return !PlayersUtils.isModer(event.getPlayer().getUniqueId().toString());
                                    case "isOwner":
                                        return !PlayersUtils.isOwner(event.getPlayer().getUniqueId().toString());
                                }
                            }
                            default:
                                return false;
                        }
                    }
                    case 4: {
                        switch (split[1]) {
                            case "player": {
                                switch (split[2]) {
                                    case "nameIs":
                                        return !event.getPlayer().getName().equals(split[3]);
                                    case "hasItem":
                                        return !event.getPlayer().getInventory().contains(Integer.valueOf(split[3]));
                                    case "isInHand":
                                        return !event.getPlayer().getInventory().getItemInMainHand().equals(new ItemStack(Integer.valueOf(split[3])));
                                    default:
                                        return false;
                                }
                            }
                            default:
                                return false;
                        }
                    }
                    case 5: {
                        switch (split[1]) {
                            case "player": {
                                switch (split[2]) {
                                    case "hasItem":
                                        return !event.getPlayer().getInventory().contains(Integer.valueOf(split[3]), Integer.valueOf(split[4]));
                                    case "isInHand":
                                        return !event.getPlayer().getInventory().getItemInMainHand().equals(new ItemStack(Integer.valueOf(split[3]), Integer.valueOf(split[4])));
                                    case "getCooldown":
                                        return event.getPlayer().getCooldown(Material.getMaterial(Integer.valueOf(split[3]))) != Integer.valueOf(split[4]);
                                    default:
                                        return false;
                                }
                            }
                            default:
                                return false;
                        }
                    }
                    case 6: {
                        switch (split[1]) {
                            case "player": {
                                switch (split[2]) {
                                    case "isInHand":
                                        return !event.getPlayer().getInventory().getItemInMainHand().equals(new ItemStack(Integer.valueOf(split[3]), Integer.valueOf(split[4]), Byte.valueOf(split[5])));
                                    default:
                                        return false;
                                }
                            }
                            default:
                                return false;
                        }
                    }
                    default:
                        return false;
                }
            }
            default:
                return false;
        }
    }
}