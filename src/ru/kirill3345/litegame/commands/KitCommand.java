package ru.kirill3345.litegame.commands;

import java.util.Arrays;

import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;
import ru.kirill3345.litegame.utils.Items;
import ru.kirill3345.litegame.utils.Menus;
import ru.kirill3345.litegame.utils.ServerSettings;
import ru.kirill3345.litegame.utils.world.WorldManager;

public class KitCommand extends Command {
    public KitCommand() {
        super
                (
                        "kit",
                        "Выбрать набор.",
                        "/kit",
                        Arrays.asList("лше")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (WorldManager.getType(player.getWorld().getName()) == 12) {
                if (player.getHealth() == 20) {
                    if (player.getGameMode() != GameMode.SPECTATOR) {
                        PlayerInventory inventory = player.getInventory();
                        player.setGameMode(GameMode.SPECTATOR);
                        player.setHealth(20);
                        inventory.clear();
                        inventory.setItem(8, Items.menu);
                    }
                    player.openInventory(Menus.kitPvp);
                } else
                    sender.sendMessage("У вас мало здоровья!");
            } else
                player.sendMessage(ServerSettings.unknowCommand);
        } else
            sender.sendMessage("Данную команду нельзя выполнить через консколь.");
        return true;
    }
}