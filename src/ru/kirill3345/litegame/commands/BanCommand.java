package ru.kirill3345.litegame.commands;

import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.bukkit.BanList;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;
import ru.kirill3345.litegame.LiteGame;
import ru.kirill3345.litegame.utils.ChatUtils;
import ru.kirill3345.litegame.utils.PlayersUtils;

public class BanCommand extends Command {
    public BanCommand() {
        super
                (
                        "ban",
                        "Заблокировать игрока.",
                        "/ban <игрок> <причина...>",
                        Arrays.asList("ифт")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        String senderDisplayName;
        World senderWorld = null;
        String senderName = sender.getName();
        if (sender instanceof Player) {
            Player player = (Player) sender;
            String uuid = player.getUniqueId().toString();
            if (PlayersUtils.isModer(uuid) && PlayersUtils.isLoggined(uuid)) {
                senderDisplayName = player.getDisplayName();
                senderWorld = player.getWorld();
            } else {
                player.sendMessage("Недостаточно прав.");
                return true;
            }
        } else
            senderDisplayName = senderName;
        if (args.length != 0) {
            String reason = args.length > 0 ? StringUtils.join(args, " ", 1, args.length) : "Не указана.";
            LiteGame.server.getBanList(BanList.Type.NAME).addBan(args[0], reason, null, senderName);
            Player player = LiteGame.server.getPlayer(args[0]);
            if (player != null) {
                player.kickPlayer
                        (
                                "§lВы заблокированы!§r"
                                        + "\n" +
                                        "Причина: " + reason
                        );
                ChatUtils.broadcastMessage(senderDisplayName + "§r заблокировал " + player.getDisplayName() + "§r:§7 " + reason, player.getWorld().getPlayers());
            } else if (senderWorld != null)
                ChatUtils.broadcastMessage(senderDisplayName + "§r заблокировал " + PlayersUtils.getOfflineDisplayName(LiteGame.server.getOfflinePlayer(args[0]).getUniqueId().toString()) + "§r:§7 " + reason, senderWorld.getPlayers());
        } else
            sender.sendMessage("§cИспользование: " + usageMessage);
        return true;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String commandLabel, String[] args) {
        if (args.length == 1) {
            List<String> matchedPlayers = Lists.newArrayList();
            for (Player players : LiteGame.server.getOnlinePlayers()) {
                String names = players.getName();
                if (StringUtil.startsWithIgnoreCase(names, args[args.length - 1]))
                    matchedPlayers.add(names);
            }
            Collections.sort(matchedPlayers, String.CASE_INSENSITIVE_ORDER);
            return matchedPlayers;
        } else
            return LiteGame.emptyList;
    }
}