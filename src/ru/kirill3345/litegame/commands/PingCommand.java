package ru.kirill3345.litegame.commands;

import java.util.Arrays;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PingCommand extends Command {
    public PingCommand() {
        super
                (
                        "ping",
                        "Узнать свой пинг.",
                        "/ping",
                        Arrays.asList("зштп")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player)
            sender.sendMessage("Пинг: " + format(((Player) sender).spigot().getPing()));
        else
            sender.sendMessage("Данную команду нельзя выполнить через консколь.");
        return true;
    }

    public String format(int ping) {
        return "§" + (ping <= 100 ? "a" : ping <= 180 ? "e" : "c") + ping;
    }
}