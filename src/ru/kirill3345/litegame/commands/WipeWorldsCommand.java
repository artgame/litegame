package ru.kirill3345.litegame.commands;

import java.util.Arrays;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import ru.kirill3345.litegame.utils.world.WorldManager;

public class WipeWorldsCommand extends Command {
    public WipeWorldsCommand() {
        super
                (
                        "wipeworlds",
                        "Удалить все миры.",
                        "/wipewolrds",
                        Arrays.asList("цшзуцщкдвы")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender.isOp())
            WorldManager.wipeOldWorlds();
        else
            sender.sendMessage("Недостаточно прав.");
        return true;
    }
}