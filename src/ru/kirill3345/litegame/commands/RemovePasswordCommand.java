package ru.kirill3345.litegame.commands;

import java.util.Arrays;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.kirill3345.litegame.utils.PlayersUtils;

public class RemovePasswordCommand extends Command {
    public RemovePasswordCommand() {
        super
                (
                        "removepassword",
                        "Убрать пароль у игрока.",
                        "/removepassword <игрок>",
                        Arrays.asList("куьщмузфыыцщкв")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player)
            sender.sendMessage("Данную команду нельзя выполнить через чат.");
        else if (args.length == 1)
            PlayersUtils.removePlayer(args[0]);
        else
            sender.sendMessage("§cИспользование: " + usageMessage);
        return true;
    }
}