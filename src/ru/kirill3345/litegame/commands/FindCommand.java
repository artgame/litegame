package ru.kirill3345.litegame.commands;

import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;
import ru.kirill3345.litegame.LiteGame;
import ru.kirill3345.litegame.utils.PlayersUtils;
import ru.kirill3345.litegame.utils.world.Hub;

public class FindCommand extends Command {
    public FindCommand() {
        super
                (
                        "find",
                        "Узнать где находится игрок.",
                        "/find <игрок>",
                        Arrays.asList("аштв")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (args.length == 1) {
            Player findPlayer = LiteGame.server.getPlayer(args[0]);
            if (findPlayer != null) {
                World findedWorld = findPlayer.getWorld();
                if (findedWorld == Hub.world)
                    sender.sendMessage(findPlayer.getDisplayName() + "§r находится в лобби.");
                else {
                    String findedWorldName = findedWorld.getName();
                    if (!findedWorldName.equals(findPlayer.getUniqueId().toString()))
                        sender.sendMessage(findPlayer.getDisplayName() + "§r находится в мире " + PlayersUtils.getOfflineDisplayName(findedWorldName) + "§r.");
                    else
                        sender.sendMessage(findPlayer.getDisplayName() + "§r находится в своём мире.");
                }
            } else
                sender.sendMessage("Игрок не найден!");
        } else
            sender.sendMessage("§cИспользование: " + usageMessage);
        return true;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String commandLabel, String[] args) {
        if (args.length == 1) {
            List<String> matchedPlayers = Lists.newArrayList();
            for (Player players : LiteGame.server.getOnlinePlayers()) {
                String names = players.getName();
                if (StringUtil.startsWithIgnoreCase(names, args[args.length - 1]))
                    matchedPlayers.add(names);
            }
            Collections.sort(matchedPlayers, String.CASE_INSENSITIVE_ORDER);
            return matchedPlayers;
        } else
            return LiteGame.emptyList;
    }
}