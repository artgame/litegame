package ru.kirill3345.litegame.commands;

import java.util.Arrays;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.kirill3345.litegame.utils.PlayersUtils;

public class HypeCommand extends Command {
    public HypeCommand() {
        super
                (
                        "hype",
                        "Проверить свой хайп.",
                        "/hype",
                        Arrays.asList("money", "balance", "bank", "рнзу", "ьщтун", "ифдфтсу", "ифтл")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player)
            sender.sendMessage("Хайп:§2 " + PlayersUtils.getHype(((Player) sender).getUniqueId().toString()));
        else
            sender.sendMessage("Данную команду нельзя выполнить через консколь.");
        return true;
    }
}