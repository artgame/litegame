package ru.kirill3345.litegame.commands;

import java.util.Arrays;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.kirill3345.litegame.utils.world.Hub;

public class HubCommand extends Command {
    public HubCommand() {
        super
                (
                        "hub",
                        "Телепортироваться в главное лобби.",
                        "/hub",
                        Arrays.asList("lobby", "рги", "дщиин")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player)
            ((Player) sender).teleport(Hub.location);
        else
            sender.sendMessage("Данную команду нельзя выполнить через консколь.");
        return true;
    }
}