package ru.kirill3345.litegame.commands;

import java.util.Arrays;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import ru.kirill3345.litegame.LiteGame;

public class TpsCommand extends Command {
    public TpsCommand() {
        super
                (
                        "tps",
                        "Узнать количество тиков в секунду.",
                        "/tps",
                        Arrays.asList("езы")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        double[] tps = LiteGame.server.getTPS();
        sender.sendMessage("TPS за 1м, 5м, 15м: " + format(tps[0]) + "§r, " + format(tps[1]) + "§r, " + format(tps[2]) + "§r.");
        return true;
    }

    public String format(double tps) {
        return "§" + (tps > 18 ? "a" : tps > 16 ? "e" : "c") + (Math.round(tps * 100D) / 100D);
    }
}