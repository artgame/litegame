package ru.kirill3345.litegame.commands;

import java.util.Arrays;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import ru.kirill3345.litegame.LiteGame;

public class SayCommand extends Command {
    public SayCommand() {
        super
                (
                        "say",
                        "Отправить глобальное сообщение.",
                        "/say <сообщение...>",
                        Arrays.asList("broadcast", "ыфн", "икщфвсфыу")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender.isOp()) {
            if (args.length >= 1) {
                StringBuilder message = new StringBuilder("");
                for (String arg : args)
                    message.append(arg).append(" ");
                LiteGame.server.broadcastMessage("§8»§r " + message.toString());
            } else
                sender.sendMessage("§cИспользование: " + usageMessage);
        } else
            sender.sendMessage("Недостаточно прав.");
        return true;
    }
}