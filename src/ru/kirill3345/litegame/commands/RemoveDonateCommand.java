package ru.kirill3345.litegame.commands;

import java.io.IOException;
import java.util.Arrays;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.kirill3345.litegame.utils.PlayersUtils;

public class RemoveDonateCommand extends Command {
    public RemoveDonateCommand() {
        super
                (
                        "removedonate",
                        "Убрать донат у игрока.",
                        "/removedonate <игрок>",
                        Arrays.asList("куьщмувщтфеу")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player)
            sender.sendMessage("Данную команду нельзя выполнить через чат.");
        else if (args.length == 1) try {
            PlayersUtils.removeDonate(args[0]);
        } catch (IOException ex) {
        }
        else
            sender.sendMessage("§cИспользование: " + usageMessage);
        return true;
    }
}