package ru.kirill3345.litegame.commands;

import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.bukkit.BanList;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;
import ru.kirill3345.litegame.LiteGame;
import ru.kirill3345.litegame.utils.ChatUtils;
import ru.kirill3345.litegame.utils.PlayersUtils;

public class BanIpCommand extends Command {
    public static Pattern ipValidity = Pattern.compile("^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");

    public BanIpCommand() {
        super
                (
                        "ban-ip",
                        "Заблокировать игрока по IP.",
                        "/ban-ip <игрок/IP> <причина...>",
                        Arrays.asList("ифт-шз")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        String senderName = sender.getName();
        String senderDisplayName;
        if (sender instanceof Player) {
            Player player = (Player) sender;
            String uuid = player.getUniqueId().toString();
            if (PlayersUtils.isModer(uuid) && PlayersUtils.isLoggined(uuid))
                senderDisplayName = player.getDisplayName();
            else {
                player.sendMessage("Недостаточно прав.");
                return true;
            }
        } else
            senderDisplayName = senderName;
        if (args.length != 0) {
            String reason = args.length > 0 ? StringUtils.join(args, " ", 1, args.length) : "Не указана.";
            if (ipValidity.matcher(args[0]).matches())
                processIPBan(args[0], senderName, senderDisplayName, reason);
            else {
                Player player = LiteGame.server.getPlayer(args[0]);
                if (player != null)
                    processIPBan(player.getAddress().getAddress().getHostAddress(), senderName, senderDisplayName, reason);
                else
                    sender.sendMessage("§cИспользование: " + usageMessage);
            }
        } else
            sender.sendMessage("§cИспользование: " + usageMessage);
        return true;
    }

    public void processIPBan(String ip, String senderName, String senderDisplayName, String reason) {
        LiteGame.server.getBanList(BanList.Type.IP).addBan(ip, reason, null, senderName);
        LiteGame.server.getOnlinePlayers().stream().filter(player -> player.getAddress().getAddress().getHostAddress().equals(ip)).map(player ->
        {
            player.kickPlayer
                    (
                            "§lВаш IP заблокирован!§r"
                                    + "\n" +
                                    "Причина: " + reason
                    );
            return player;
        }).forEach(player -> ChatUtils.broadcastMessage(senderDisplayName + "§r заблокировал IP " + player.getDisplayName() + "§r:§7 " + reason, player.getWorld().getPlayers()));
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String commandLabel, String[] args) {
        if (args.length == 1) {
            List<String> matchedPlayers = Lists.newArrayList();
            for (Player players : LiteGame.server.getOnlinePlayers()) {
                String names = players.getName();
                if (StringUtil.startsWithIgnoreCase(names, args[args.length - 1]))
                    matchedPlayers.add(names);
            }
            Collections.sort(matchedPlayers, String.CASE_INSENSITIVE_ORDER);
            return matchedPlayers;
        } else
            return LiteGame.emptyList;
    }
}