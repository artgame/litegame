package ru.kirill3345.litegame.commands;

import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.bukkit.BanEntry;
import org.bukkit.BanList;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;
import ru.kirill3345.litegame.LiteGame;
import ru.kirill3345.litegame.utils.ChatUtils;
import ru.kirill3345.litegame.utils.PlayersUtils;

public class PardonCommand extends Command {
    public PardonCommand() {
        super
                (
                        "pardon",
                        "Разблокировать игрока.",
                        "/ban <игрок>",
                        Arrays.asList("unban", "зфквщт", "гтифт")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        String senderName = sender.getName();
        String senderDisplayName;
        World senderWorld = null;
        if (sender instanceof Player) {
            Player player = (Player) sender;
            String uuid = player.getUniqueId().toString();
            if (PlayersUtils.isModer(uuid) && PlayersUtils.isLoggined(uuid)) {
                senderDisplayName = player.getDisplayName();
                senderWorld = player.getWorld();
            } else {
                player.sendMessage("Недостаточно прав.");
                return true;
            }
        } else
            senderDisplayName = senderName;
        if (args.length == 1) {
            if (LiteGame.server.getBanList(BanList.Type.NAME).isBanned(args[0])) {
                LiteGame.server.getBanList(BanList.Type.NAME).pardon(args[0]);
                if (senderWorld != null)
                    ChatUtils.broadcastMessage(senderDisplayName + "§r разблокировал " + PlayersUtils.getOfflineDisplayName(LiteGame.server.getOfflinePlayer(args[0]).getUniqueId().toString()) + "§r.", senderWorld.getPlayers());
            } else
                sender.sendMessage("Указано неверное имя игрока, отключённого от сервера.");
        } else
            sender.sendMessage("§cИспользование: " + usageMessage);
        return true;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String commandLabel, String[] args) {
        if (args.length == 1) {
            List<String> matchedPlayers = Lists.newArrayList();
            for (BanEntry banEntries : LiteGame.server.getBanList(BanList.Type.NAME).getBanEntries()) {
                String names = banEntries.getTarget();
                if (StringUtil.startsWithIgnoreCase(names, args[args.length - 1]))
                    matchedPlayers.add(names);
            }
            Collections.sort(matchedPlayers, String.CASE_INSENSITIVE_ORDER);
            return matchedPlayers;
        } else
            return LiteGame.emptyList;
    }
}