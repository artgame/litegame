package ru.kirill3345.litegame.commands;

import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;
import ru.kirill3345.litegame.LiteGame;

import static ru.kirill3345.litegame.utils.ChatUtils.hasIp;
import static ru.kirill3345.litegame.utils.ChatUtils.hasWeb;

import ru.kirill3345.litegame.utils.PlayersUtils;

public class TellCommand extends Command {
    public TellCommand() {
        super
                (
                        "tell",
                        "Отправить личное сообщение.",
                        "/tell <игрок> <сообщение...>",
                        Arrays.asList("msg", "message", "w", "me", "talk", "m", "ьып", "ьуыыфпу", "ц", "ьу", "ефдл", "ь")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length >= 2) {
                if (!PlayersUtils.isEnabled(player.getUniqueId().toString(), "privateMessagesDisabled")) {
                    Player to = LiteGame.server.getPlayer(args[0]);
                    if (to != null) {
                        if (to != player) {
                            if (!PlayersUtils.isEnabled(to.getUniqueId().toString(), "privateMessagesDisabled")) {
                                StringBuilder stringBuilder = new StringBuilder("");
                                String message;
                                if (!player.isOp()) {
                                    for (int i = 1; i < args.length; i++) {
                                        stringBuilder.append
                                                (
                                                        !hasWeb(args[i]) && !hasIp(args[i])
                                                                ? args[i]
                                                                : StringUtils.repeat("*", args[i].length())
                                                ).append(" ");
                                    }
                                    message = stringBuilder.toString();
                                } else {
                                    for (int i = 1; i < args.length; i++)
                                        stringBuilder.append(args[i]).append(" ");
                                    message = stringBuilder.toString();
                                }
                                String formatMessage = player.getDisplayName() + "§8 » " + to.getDisplayName() + "§8:§r " + message;
                                player.sendMessage(formatMessage);
                                to.sendMessage(formatMessage);
                                to.sendTitle(player.getDisplayName(), message, 5, 140, 5);
                                if (!PlayersUtils.isEnabled(to.getUniqueId().toString(), "disableSoundNotify"))
                                    to.playSound(to.getLocation(), Sound.BLOCK_NOTE_PLING, Float.MAX_VALUE, 1);
                            } else
                                player.sendMessage("У этого игрока выключены личные сообщения.");
                        } else
                            sender.sendMessage("Невозможно отправить личное сообщение самому себе!");
                    } else
                        sender.sendMessage("Игрок не найден!");
                } else
                    player.sendMessage("У вас выключены личные сообщения.");
            } else
                player.sendMessage("§cИспользование: " + usageMessage);
        } else
            sender.sendMessage("Данную команду нельзя выполнить через консколь.");
        return true;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String commandLabel, String[] args) {
        if (args.length == 1 && sender instanceof Player) {
            List<String> matchedPlayers = Lists.newArrayList();
            for (Player players : LiteGame.server.getOnlinePlayers()) {
                String names = players.getName();
                if (StringUtil.startsWithIgnoreCase(names, args[args.length - 1]))
                    matchedPlayers.add(names);
            }
            Collections.sort(matchedPlayers, String.CASE_INSENSITIVE_ORDER);
            return matchedPlayers;
        } else
            return LiteGame.emptyList;
    }
}