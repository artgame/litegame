package ru.kirill3345.litegame.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;
import ru.kirill3345.litegame.LiteGame;
import ru.kirill3345.litegame.coding.CodingUtils;
import ru.kirill3345.litegame.utils.PlayersUtils;
import ru.kirill3345.litegame.utils.SkyBlockUtils;
import ru.kirill3345.litegame.utils.world.WorldManager;

public class PasswordCommand extends Command {
    public final List<String> argsList = Arrays.asList("set", "login", "remove");

    public PasswordCommand() {
        super
                (
                        "password",
                        "Установить пароль",
                        "/password <login|set|remove> <пароль>",
                        Arrays.asList("pass", "зфыыцщкв", "зфыы")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length == 2) {
                switch (args[0]) {
                    case "set": {
                        String uuid = player.getUniqueId().toString();
                        if (!PlayersUtils.isRegistered(uuid)) {
                            String ip = player.getAddress().getAddress().getHostAddress();
                            String profileIp = PlayersUtils.getUuidByIp(ip);
                            if (profileIp.isEmpty()) {
                                player.sendMessage("Пароль успешно установлен!");
                                PlayersUtils.login(uuid);
                                PlayersUtils.setPassword(uuid, args[1]);
                                PlayersUtils.setIp(uuid, player.getAddress().getAddress().getHostAddress());
                                PlayersUtils.setDisplayName(player, PlayersUtils.getDisplayName(player));
                            } else
                                player.sendMessage("У вас уже есть аккаунт:§7 " + profileIp);
                        } else
                            player.sendMessage("У данного аккаунта уже есть пароль!");
                        return true;
                    }
                    case "login": {
                        String uuidString = player.getUniqueId().toString();
                        if (PlayersUtils.isRegistered(uuidString)) {
                            String ip = player.getAddress().getAddress().getHostAddress();
                            if (!PlayersUtils.isLoggined(uuidString)) {
                                String profileIp = PlayersUtils.getUuidByIp(ip);
                                if (profileIp.isEmpty()) {
                                    if (PlayersUtils.getPassword(uuidString).equals(args[1])) {
                                        player.sendMessage("Вы успешно вошли!");
                                        PlayersUtils.login(uuidString);
                                        PlayersUtils.setIp(uuidString, ip);
                                        PlayersUtils.setDisplayName(player, PlayersUtils.getDisplayName(player));
                                    } else
                                        player.sendMessage("Неправильный пароль!");
                                } else
                                    player.sendMessage("У вас уже есть аккаунт:§7 " + PlayersUtils.getOfflineDisplayName(profileIp));
                            } else
                                player.sendMessage("Вы уже вошли!");
                        } else
                            player.sendMessage("У данного аккаунта нет пароля!");
                        return true;
                    }
                    case "remove": {
                        UUID uuid = player.getUniqueId();
                        String uuidString = uuid.toString();
                        if (PlayersUtils.isRegistered(uuidString)) {
                            if (PlayersUtils.getPassword(uuidString).equals(args[1])) {
                                if (!WorldManager.getWorldFolder(uuidString).isDirectory()) {
                                    switch (WorldManager.getType(player.getWorld().getName())) {
                                        default: {
                                            player.sendMessage("Вы успешно удалили пароль!");
                                            PlayersUtils.unLogin(uuidString);
                                            PlayersUtils.removePlayer(uuidString);
                                            SkyBlockUtils.removeSkyBlock(uuidString);
                                            CodingUtils.removeCode(uuidString);
                                            PlayersUtils.setDisplayName(player, PlayersUtils.getDisplayName(player));
                                            break;
                                        }
                                        case 7:
                                        case 15: {
                                            player.sendMessage("Телепортируйтесь в лобби для удаления пароля.");
                                            break;
                                        }
                                    }
                                } else
                                    player.sendMessage("Удалите свой мир для удаления аккаунта.");
                            } else
                                player.sendMessage("Неправильный пароль!");
                        } else
                            player.sendMessage("У данного аккаунта нет пароля!");
                        return true;
                    }
                    default: {
                        player.sendMessage("§cИспользование: " + usageMessage);
                        return true;
                    }
                }
            } else
                player.sendMessage("§cИспользование: " + usageMessage);
        } else
            sender.sendMessage("Данную команду нельзя выполнить через консколь.");
        return true;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String commandLabel, String[] args) {
        return args.length == 1 && sender instanceof Player
                ? StringUtil.copyPartialMatches(args[0], argsList, new ArrayList<String>(argsList.size()))
                : LiteGame.emptyList;
    }
}