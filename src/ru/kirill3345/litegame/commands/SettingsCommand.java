package ru.kirill3345.litegame.commands;

import java.util.Arrays;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.kirill3345.litegame.utils.Menus;
import ru.kirill3345.litegame.utils.PlayersUtils;

public class SettingsCommand extends Command {
    public SettingsCommand() {
        super
                (
                        "settings",
                        "Открыть настройки.",
                        "/settings",
                        Arrays.asList("ыуеештпы")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            player.openInventory
                    (
                            PlayersUtils.isLoggined(player.getUniqueId().toString())
                                    ? Menus.playerSettings(player)
                                    : Menus.needLogin
                    );
        } else
            sender.sendMessage("Данную команду нельзя выполнить через консколь.");
        return true;
    }
}