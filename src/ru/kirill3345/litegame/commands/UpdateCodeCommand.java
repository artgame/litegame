package ru.kirill3345.litegame.commands;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;
import ru.kirill3345.litegame.LiteGame;
import ru.kirill3345.litegame.coding.CodingUtils;

public class UpdateCodeCommand extends Command {
    public final List<String> argsList = Arrays.asList("confirm");

    public UpdateCodeCommand() {
        super
                (
                        "updatecode",
                        "Обновить код.",
                        "/updatecode",
                        Arrays.asList("гзвфеусщву")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            if (args.length == 1) {
                if (args[0].equals("confirm")) {
                    sender.sendMessage("Код обновлён в новый формат.");
                    String uuid = ((Player) sender).getUniqueId().toString();
                    List<String> list = Lists.newArrayList();
                    CodingUtils.getCode(uuid).forEach(pages -> list.add(pages.replace(":", "::").replace("::::", "::")));
                    CodingUtils.saveCode(uuid, list);
                } else
                    sender.sendMessage("Данная команда обновит старое разделение кода§l :§r на§l ::§r. Напишите§2 /updatecode confirm§r для продолжения.");
            } else
                sender.sendMessage("Данная команда обновит старое разделение кода§l :§r на§l ::§r. Напишите§2 /updatecode confirm§r для продолжения.");
        } else
            sender.sendMessage("Данную команду нельзя выполнить через консколь.");
        return true;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String commandLabel, String[] args) {
        return args.length == 1 && sender instanceof Player
                ? StringUtil.copyPartialMatches(args[0], argsList, new ArrayList<String>(argsList.size()))
                : LiteGame.emptyList;
    }
}