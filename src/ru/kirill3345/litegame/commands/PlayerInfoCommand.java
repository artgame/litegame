package ru.kirill3345.litegame.commands;

import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;
import ru.kirill3345.litegame.LiteGame;
import ru.kirill3345.litegame.utils.PlayersUtils;

public class PlayerInfoCommand extends Command {
    public PlayerInfoCommand() {
        super
                (
                        "playerinfo",
                        "Узнать информацию о игроке.",
                        "/playerinfo <игрок>",
                        Arrays.asList("здфнукштащ")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            String uuid = player.getUniqueId().toString();
            if (!PlayersUtils.isModer(uuid) || !PlayersUtils.isLoggined(uuid)) {
                player.sendMessage("Недостаточно прав.");
                return true;
            }
        }
        switch (args.length) {
            case 0: {
                if (sender instanceof Player) {
                    Player player = (Player) sender;
                    sender.sendMessage
                            (
                                    generateMessage
                                            (
                                                    player.getUniqueId().toString(),
                                                    player.getAddress().getAddress().getHostAddress(),
                                                    player.isOp()
                                            )
                            );
                } else
                    sender.sendMessage("§cИспользование: " + usageMessage);
                break;
            }
            case 1: {
                Player victim = LiteGame.server.getPlayer(args[0]);
                if (victim != null) {
                    sender.sendMessage
                            (
                                    generateMessage
                                            (
                                                    victim.getUniqueId().toString(),
                                                    victim.getAddress().getAddress().getHostAddress(),
                                                    sender.isOp()
                                            )
                            );
                } else {
                    String victimUuidString = LiteGame.server.getOfflinePlayer(args[0]).getUniqueId().toString();
                    if (PlayersUtils.isRegistered(victimUuidString)) {
                        sender.sendMessage
                                (
                                        generateMessage
                                                (
                                                        victimUuidString,
                                                        PlayersUtils.getIp(victimUuidString),
                                                        sender.isOp()
                                                )
                                );
                    } else
                        sender.sendMessage("Игрок не найден!");
                }
                break;
            }
            default: {
                sender.sendMessage("§cИспользование: " + usageMessage);
                break;
            }
        }
        return true;
    }

    public String generateMessage(String uuid, String ip, boolean showPassword) {
        return
                "Ник: " + PlayersUtils.getOfflineDisplayName(uuid)
                        + "\n" +
                        "§rUUID:§7 " + uuid
                        + "\n" +
                        "§rIP:§7 " + ip
                        +
                        (
                                showPassword
                                        ? "§r\nПароль:§7 " + PlayersUtils.getPassword(uuid)
                                        : ""
                        );
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String commandLabel, String[] args) {
        if (args.length == 1) {
            String uuid = ((Player) sender).getUniqueId().toString();
            if (sender.isOp() || (PlayersUtils.isLoggined(uuid) && PlayersUtils.isModer(uuid))) {
                List<String> matchedPlayers = Lists.newArrayList();
                for (Player players : LiteGame.server.getOnlinePlayers()) {
                    String names = players.getName();
                    if (StringUtil.startsWithIgnoreCase(names, args[args.length - 1]))
                        matchedPlayers.add(names);
                }
                Collections.sort(matchedPlayers, String.CASE_INSENSITIVE_ORDER);
                return matchedPlayers;
            } else
                return LiteGame.emptyList;
        } else
            return LiteGame.emptyList;
    }
}