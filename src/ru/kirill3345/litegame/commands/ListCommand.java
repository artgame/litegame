package ru.kirill3345.litegame.commands;

import java.util.Arrays;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.kirill3345.litegame.LiteGame;

public class ListCommand extends Command {
    public ListCommand() {
        super
                (
                        "list",
                        "Узнать количество онлайн игроков.",
                        "/list",
                        Arrays.asList("online", "дшые", "щтдшту")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        sender.sendMessage("Глобальный онлайн:§2 " + LiteGame.server.getOnlinePlayers().size());
        if (sender instanceof Player)
            sender.sendMessage("Онлайн в мире:§2 " + ((Player) sender).getWorld().getPlayerCount());
        return true;
    }
}