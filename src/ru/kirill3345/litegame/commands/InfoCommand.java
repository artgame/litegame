package ru.kirill3345.litegame.commands;

import java.util.Arrays;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.kirill3345.litegame.utils.Menus;

public class InfoCommand extends Command {
    public InfoCommand() {
        super
                (
                        "info",
                        "Открыть меню информации.",
                        "/info",
                        Arrays.asList("information", "штащ", "штащкьфешщт")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player)
            ((Player) sender).openInventory(Menus.info);
        else
            sender.sendMessage("Данную команду нельзя выполнить через консколь.");
        return true;
    }
}