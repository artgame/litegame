package ru.kirill3345.litegame.commands;

import java.io.IOException;
import java.util.Arrays;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.kirill3345.litegame.utils.PlayersUtils;

public class GiveDonateCommand extends Command {
    public GiveDonateCommand() {
        super
                (
                        "givedonate",
                        "Выдать донат игроку.",
                        "/givedonate <игрок> <донат>",
                        Arrays.asList("пшмувщтфеу")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player)
            sender.sendMessage("Данную команду нельзя выполнить через чат.");
        else if (args.length == 2) try {
            PlayersUtils.giveDonate(args[0], args[1]);
        } catch (IOException ex) {
        }
        else
            sender.sendMessage("§cИспользование: " + usageMessage);
        return true;
    }
}