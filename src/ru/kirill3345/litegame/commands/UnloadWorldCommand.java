package ru.kirill3345.litegame.commands;

import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;
import ru.kirill3345.litegame.LiteGame;
import ru.kirill3345.litegame.utils.world.WorldManager;

public class UnloadWorldCommand extends Command {
    public UnloadWorldCommand() {
        super
                (
                        "unloadworld",
                        "Закрыть мир.",
                        "/unloadworld <мир>",
                        Arrays.asList("гтдщфвцщкдв")
                );
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.isOp()) {
                if (args.length == 1) {
                    World world = LiteGame.server.getWorld(args[0]);
                    if (world != null) {
                        WorldManager.closeWorld(world, true);
                        player.sendMessage("Мир отгружен.");
                    } else
                        player.sendMessage("Мир не запущен.");
                } else
                    player.sendMessage("§cИспользование: " + usageMessage);
            } else
                player.sendMessage("Недостаточно прав.");
        } else
            sender.sendMessage("Данную команду нельзя выполнить через консколь.");
        return true;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String commandLabel, String[] args) {
        if (args.length == 1 && sender instanceof Player && sender.isOp()) {
            List<String> matchedWorlds = Lists.newArrayList();
            for (World worlds : LiteGame.server.getWorlds()) {
                String names = worlds.getName();
                if (StringUtil.startsWithIgnoreCase(names, args[args.length - 1]))
                    matchedWorlds.add(names);
            }
            Collections.sort(matchedWorlds, String.CASE_INSENSITIVE_ORDER);
            return matchedWorlds;
        } else
            return LiteGame.emptyList;
    }
}