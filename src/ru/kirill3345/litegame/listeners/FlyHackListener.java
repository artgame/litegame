package ru.kirill3345.litegame.listeners;

import org.bukkit.GameMode;
import org.bukkit.potion.PotionEffectType;
import ru.kirill3345.litegame.LiteGame;

public class FlyHackListener implements Runnable {
    @Override
    public void run() {
        LiteGame.server.getOnlinePlayers().forEach(players ->
        {
            if
            (
                    !players.isOnGround()
                            && !players.isGliding()
                            && players.getFallDistance() <= 50
                            && players.getVelocity().length() >= 3
                            && !players.hasPotionEffect(PotionEffectType.LEVITATION)
                            &&
                            (
                                    players.getGameMode() == GameMode.SURVIVAL
                                            || players.getGameMode() == GameMode.ADVENTURE
                            )
            ) {
                players.kickPlayer
                        (
                                "§lВы кикнуты!§r"
                                        + "\n" +
                                        "Причина: FlyHack."
                        );
            }
        });
    }
}