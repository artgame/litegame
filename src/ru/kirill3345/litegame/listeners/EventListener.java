package ru.kirill3345.litegame.listeners;

import com.destroystokyo.paper.Title;
import com.destroystokyo.paper.event.block.BeaconEffectEvent;
import com.destroystokyo.paper.event.player.IllegalPacketEvent;
import com.destroystokyo.paper.event.server.ServerExceptionEvent;

import java.util.Set;

import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.FireworkEffect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.block.ShulkerBox;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Egg;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Firework;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.block.NotePlayEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.enchantment.PrepareItemEnchantEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerEditBookEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerStatisticIncrementEvent;
import org.bukkit.event.server.MapInitializeEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.event.vehicle.VehicleCreateEvent;
import org.bukkit.event.weather.LightningStrikeEvent;
import org.bukkit.event.weather.ThunderChangeEvent;
import org.bukkit.event.world.PortalCreateEvent;
import org.bukkit.event.world.StructureGrowEvent;
import org.bukkit.event.world.WorldSaveEvent;
import org.bukkit.event.world.WorldUnloadEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.map.MapView;
import ru.kirill3345.litegame.LiteGame;
import ru.kirill3345.litegame.coding.CodingUtils;
import ru.kirill3345.litegame.utils.ArtStudioUtils;
import ru.kirill3345.litegame.utils.ChatUtils;
import ru.kirill3345.litegame.utils.FileUtils;
import ru.kirill3345.litegame.utils.GameplayUtils;
import ru.kirill3345.litegame.utils.Items;
import ru.kirill3345.litegame.utils.KitPVP;
import ru.kirill3345.litegame.utils.LocationUtils;
import ru.kirill3345.litegame.utils.Menus;
import ru.kirill3345.litegame.utils.MusicUtils;
import ru.kirill3345.litegame.utils.NavigatorUtils;
import ru.kirill3345.litegame.utils.PlayersUtils;
import ru.kirill3345.litegame.utils.ServerSettings;
import ru.kirill3345.litegame.utils.SkyBlockUtils;
import ru.kirill3345.litegame.utils.TextComponents;
import ru.kirill3345.litegame.utils.Titles;
import ru.kirill3345.litegame.utils.world.GameUtils;
import ru.kirill3345.litegame.utils.world.Hub;
import ru.kirill3345.litegame.utils.world.Trees;
import ru.kirill3345.litegame.utils.world.WorldManager;

public class EventListener implements Listener {
    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();
        event.setJoinMessage(null);
        player.getInventory().clear();
        GameplayUtils.runWorldTabPlayers(player);
        player.setAllowFlight(PlayersUtils.isLoggined(uuid) && PlayersUtils.isDonator(uuid));
        GameplayUtils.giveHubItems(player.getInventory());
        player.setLevel(PlayersUtils.getHype(uuid));
        PlayersUtils.setDisplayName(player, PlayersUtils.getDisplayName(player));
        player.sendTitle(ServerSettings.joinTitle);
        player.setPlayerListHeaderFooter(ServerSettings.tabHeader, TextComponents.hub);
        WorldManager.updatePlayerWorld(uuid);
        if (!PlayersUtils.isEnabled(uuid, "musicInHubDisabled")) {
            String music = MusicUtils.randomMusic();
            player.playSound(Hub.location, music, Float.MAX_VALUE, MusicUtils.getMusicPitch(music));
        }
        if (!PlayersUtils.isRegistered(uuid))
            player.sendMessage("У вас нет пароля. Создайте его:§2 /pass set <пароль>");
        player.sendMessage("IP сервера:§l play.litegamemc.tk");
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void onLogin(PlayerLoginEvent event) {
        switch (event.getResult()) {
            case KICK_FULL: {
                event.allow();
                break;
            }
            case KICK_BANNED: {
                event.setKickMessage
                        (
                                event.getKickMessage()
                                        .replace("You are banned from this server!", "§lВы заблокированы!§r")
                                        .replace("Your IP address is banned from this server!", "§lВаш IP заблокирован!§r")
                                        .replace("Reason: ", "Причина: ")
                        );
                break;
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPreLogin(AsyncPlayerPreLoginEvent event) {
        String name = event.getName();
        if (name.contains(" ") || name.contains("§"))
            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, "В нике есть пробел или параграф.");
        else {
            String uuid = event.getUniqueId().toString();
            if (PlayersUtils.getIp(uuid).equals(event.getAddress().getHostAddress()))
                PlayersUtils.login(uuid);
            else if (PlayersUtils.isOwner(uuid))
                event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, "Данный IP не совпадает с авторизированным IP.");
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        event.setQuitMessage(null);
        player.teleport(Hub.location);
        PlayersUtils.unLogin(player.getUniqueId().toString());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        World world = player.getWorld();
        String worldName = player.getWorld().getName();
        int worldType = WorldManager.getType(worldName);
        if (worldType != 18) {
            switch (event.getMaterial()) {
                case COMPASS: {
                    Action action = event.getAction();
                    if (action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) {
                        event.setUseItemInHand(Event.Result.DENY);
                        if (player.getCooldown(Material.COMPASS) == 0) {
                            player.openInventory(NavigatorUtils.getMenuByType(WorldManager.getType(player.getWorld().getName())));
                            player.setCooldown(Material.COMPASS, 30);
                        }
                    }
                    break;
                }
                case CHEST: {
                    Action action = event.getAction();
                    if ((action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) && player.getWorld() == Hub.world) {
                        event.setUseItemInHand(Event.Result.DENY);
                        if (player.getCooldown(Material.CHEST) == 0) {
                            player.openInventory(Menus.gadgets);
                            player.setCooldown(Material.CHEST, 15);
                        }
                    }
                    break;
                }
                case REDSTONE_COMPARATOR: {
                    Action action = event.getAction();
                    if ((action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) && player.getWorld() == Hub.world) {
                        event.setUseItemInHand(Event.Result.DENY);
                        if (player.getCooldown(Material.REDSTONE_COMPARATOR) == 0) {
                            player.openInventory(Menus.playerSettings(player));
                            player.setCooldown(Material.REDSTONE_COMPARATOR, 50);
                        }
                    }
                    break;
                }
                case BOOK: {
                    Action action = event.getAction();
                    if (action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) {
                        switch (WorldManager.getType(player.getWorld().getName())) {
                            case 0: {
                                event.setUseItemInHand(Event.Result.DENY);
                                if (player.getCooldown(Material.BOOK) == 0) {
                                    player.openInventory(Menus.info);
                                    player.setCooldown(Material.BOOK, 30);
                                }
                                break;
                            }
                            case 7: {
                                event.setUseItemInHand(Event.Result.DENY);
                                if (player.getCooldown(Material.BOOK) == 0) {
                                    player.openInventory(SkyBlockUtils.skyBlockQuests(player.getUniqueId().toString()));
                                    player.setCooldown(Material.BOOK, 50);
                                }
                                break;
                            }
                        }
                    }
                    break;
                }
                case ITEM_FRAME: {
                    Action action = event.getAction();
                    if (action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) {
                        event.setUseItemInHand(Event.Result.DENY);
                        if (player.getCooldown(Material.ITEM_FRAME) == 0) {
                            event.getPlayer().openInventory(Menus.otherBlocks);
                            player.setCooldown(Material.ITEM_FRAME, 15);
                        }
                    }
                    break;
                }
                case WOOL: {
                    if (WorldManager.getType(player.getWorld().getName()) == 17) {
                        event.setUseItemInHand(Event.Result.DENY);
                        switch (event.getAction()) {
                            case RIGHT_CLICK_BLOCK: {
                                event.getClickedBlock().setData((byte) event.getItem().getDurability());
                                break;
                            }
                            case RIGHT_CLICK_AIR: {
                                Block targetBlock = player.getTargetBlock((Set<Material>) null, 128);
                                if (targetBlock.getType() != Material.AIR)
                                    targetBlock.setData((byte) event.getItem().getDurability());
                                break;
                            }
                            default: {
                                player.openInventory(ArtStudioUtils.selectColorMenu);
                                break;
                            }
                        }
                    }
                    break;
                }
                case BONE: {
                    if (worldType == 17) {
                        event.setUseItemInHand(Event.Result.DENY);
                        if (player.getCooldown(Material.BONE) == 0) {
                            Action action = event.getAction();
                            if (action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) {
                                if (!player.isSneaking())
                                    player.openInventory(ArtStudioUtils.getArtMenu(worldName));
                                else
                                    ArtStudioUtils.loadNextArt(world);
                            } else if (player.isSneaking())
                                ArtStudioUtils.loadBackArt(world);
                        } else
                            player.setCooldown(Material.BONE, 15);
                    }
                    break;
                }
                case ENDER_PEARL: {
                    Action action = event.getAction();
                    if ((action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) && WorldManager.getType(player.getWorld().getName()) == 0) {
                        event.setUseItemInHand(Event.Result.DENY);
                        if (player.getCooldown(Material.ENDER_PEARL) == 0) {
                            EnderPearl enderPearl = player.launchProjectile(EnderPearl.class);
                            enderPearl.addPassenger(player);
                            player.setCooldown(Material.ENDER_PEARL, 50);
                        }
                    }
                    break;
                }
                case FIREWORK: {
                    Action action = event.getAction();
                    if (action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) {
                        event.setUseItemInHand(Event.Result.DENY);
                        if (world == Hub.world && player.getCooldown(Material.FIREWORK) == 0) {
                            Location fireworkLocation = player.getEyeLocation();
                            Firework firework = (Firework) player.getWorld().spawn(fireworkLocation, Firework.class);
                            FireworkMeta fireworkMeta = firework.getFireworkMeta();
                            fireworkMeta.addEffect(FireworkEffect.builder().withColor(Color.fromBGR
                                    (
                                            LiteGame.random.nextInt(256),
                                            LiteGame.random.nextInt(256),
                                            LiteGame.random.nextInt(256)
                                    )).build());
                            firework.setFireworkMeta(fireworkMeta);
                            player.setCooldown(Material.FIREWORK, 15);
                        }
                    }
                    break;
                }
                case DIAMOND_HOE: {
                    Action action = event.getAction();
                    if ((action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) && WorldManager.getType(player.getWorld().getName()) == 0) {
                        event.setUseItemInHand(Event.Result.DENY);
                        if (player.getCooldown(Material.DIAMOND_HOE) == 0) {
                            player.launchProjectile(Snowball.class);
                            world.playSound(player.getEyeLocation(), Sound.ENTITY_PLAYER_BURP, 1, 2);
                            player.setCooldown(Material.DIAMOND_HOE, 3);
                        }
                    }
                    break;
                }
                case TNT: {
                    Action action = event.getAction();
                    if ((action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) && WorldManager.getType(player.getWorld().getName()) == 0) {
                        event.setUseItemInHand(Event.Result.DENY);
                        if (player.getCooldown(Material.TNT) == 0) {
                            Location eyeLocation = player.getEyeLocation();
                            world.playEffect(eyeLocation, Effect.EXPLOSION_HUGE, 1);
                            world.playSound(eyeLocation, Sound.ENTITY_GENERIC_EXPLODE, 1, 1);
                            player.setCooldown(Material.TNT, 3);
                        }
                    }
                    break;
                }
                case ARROW: {
                    Action action = event.getAction();
                    if ((action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) && WorldManager.getType(player.getWorld().getName()) == 0) {
                        event.setUseItemInHand(Event.Result.DENY);
                        if (player.getCooldown(Material.ARROW) == 0) {
                            player.launchProjectile(Arrow.class);
                            player.setCooldown(Material.ARROW, 15);
                        }
                    }
                    break;
                }
                case END_ROD: {
                    Action action = event.getAction();
                    if ((action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) && WorldManager.getType(player.getWorld().getName()) == 0) {
                        event.setUseItemInHand(Event.Result.DENY);
                        if (player.getCooldown(Material.END_ROD) == 0) {
                            world.playSound(player.getEyeLocation(), Sound.ENTITY_VILLAGER_YES, 1, 1);
                            player.setCooldown(Material.END_ROD, 3);
                        }
                    }
                    break;
                }
                case SKULL_ITEM: {
                    Action action = event.getAction();
                    if (action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) {
                        event.setUseItemInHand(Event.Result.DENY);
                        GameplayUtils.equipArmor(event.getPlayer(), EquipmentSlot.HEAD, event.getItem(), event.getHand(), Sound.ITEM_ARMOR_EQUIP_GENERIC);
                    }
                    break;
                }
                case DIAMOND: {
                    Action action = event.getAction();
                    if (action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) {
                        switch (WorldManager.getType(player.getWorld().getName())) {
                            case 3: {
                                event.setUseItemInHand(Event.Result.DENY);
                                if (player.getCooldown(Material.DIAMOND) == 0) {
                                    player.teleport(player.getWorld().getSpawnLocation());
                                    player.setFallDistance(0);
                                    player.setVelocity(LocationUtils.noVelocity);
                                    player.setCooldown(Material.DIAMOND, 60);
                                }
                            }
                            case 4:
                            case 5:
                            case 6:
                            case 8:
                            case 10:
                            case 11:
                            case 13:
                            case 14: {
                                event.setUseItemInHand(Event.Result.DENY);
                                if (player.getCooldown(Material.DIAMOND) == 0) {
                                    if (player.getWorld().getEntitiesByClass(EnderCrystal.class).size() > 1)
                                        GameUtils.tryStartGame(player);
                                    else
                                        player.sendMessage("Арена построена неправильно: нет точек появления.");
                                    player.setCooldown(Material.DIAMOND, 30);
                                }
                                break;
                            }
                        }
                    }
                    break;
                }
                case IRON_SPADE: {
                    Action action = event.getAction();
                    if ((action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) && WorldManager.getType(player.getWorld().getName()) == 4) {
                        event.setUseItemInHand(Event.Result.DENY);
                        player.launchProjectile(Egg.class);
                        player.getWorld().playSound(player.getEyeLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 1);
                    }
                    break;
                }
                case SNOW_BALL: {
                    Action action = event.getAction();
                    if ((action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) && WorldManager.getType(player.getWorld().getName()) == 5) {
                        event.setUseItemInHand(Event.Result.DENY);
                        player.launchProjectile(Snowball.class);
                        player.updateInventory();
                    }
                    break;
                }
                case DIAMOND_BARDING: {
                    Action action = event.getAction();
                    if ((action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) && WorldManager.getType(player.getWorld().getName()) == 10) {
                        event.setUseItemInHand(Event.Result.DENY);
                        player.launchProjectile(Snowball.class);
                        player.getWorld().playSound(player.getEyeLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 2);
                    }
                    break;
                }
                case LEATHER_HELMET: {
                    equipEvent(event, EquipmentSlot.HEAD, Sound.ITEM_ARMOR_EQUIP_LEATHER);
                    break;
                }
                case LEATHER_CHESTPLATE: {
                    equipEvent(event, EquipmentSlot.CHEST, Sound.ITEM_ARMOR_EQUIP_LEATHER);
                    break;
                }
                case LEATHER_LEGGINGS: {
                    equipEvent(event, EquipmentSlot.LEGS, Sound.ITEM_ARMOR_EQUIP_LEATHER);
                    break;
                }
                case LEATHER_BOOTS: {
                    equipEvent(event, EquipmentSlot.FEET, Sound.ITEM_ARMOR_EQUIP_LEATHER);
                    break;
                }
                case IRON_HELMET: {
                    equipEvent(event, EquipmentSlot.HEAD, Sound.ITEM_ARMOR_EQUIP_IRON);
                    break;
                }
                case IRON_CHESTPLATE: {
                    equipEvent(event, EquipmentSlot.CHEST, Sound.ITEM_ARMOR_EQUIP_IRON);
                    break;
                }
                case IRON_LEGGINGS: {
                    equipEvent(event, EquipmentSlot.LEGS, Sound.ITEM_ARMOR_EQUIP_IRON);
                    break;
                }
                case IRON_BOOTS: {
                    equipEvent(event, EquipmentSlot.FEET, Sound.ITEM_ARMOR_EQUIP_IRON);
                    break;
                }
                case GOLD_HELMET: {
                    equipEvent(event, EquipmentSlot.HEAD, Sound.ITEM_ARMOR_EQUIP_GOLD);
                    break;
                }
                case GOLD_CHESTPLATE: {
                    equipEvent(event, EquipmentSlot.CHEST, Sound.ITEM_ARMOR_EQUIP_GOLD);
                    break;
                }
                case GOLD_LEGGINGS: {
                    equipEvent(event, EquipmentSlot.LEGS, Sound.ITEM_ARMOR_EQUIP_GOLD);
                    break;
                }
                case GOLD_BOOTS: {
                    equipEvent(event, EquipmentSlot.FEET, Sound.ITEM_ARMOR_EQUIP_GOLD);
                    break;
                }
                case DIAMOND_HELMET: {
                    equipEvent(event, EquipmentSlot.HEAD, Sound.ITEM_ARMOR_EQUIP_DIAMOND);
                    break;
                }
                case DIAMOND_CHESTPLATE: {
                    equipEvent(event, EquipmentSlot.CHEST, Sound.ITEM_ARMOR_EQUIP_DIAMOND);
                    break;
                }
                case DIAMOND_LEGGINGS: {
                    equipEvent(event, EquipmentSlot.LEGS, Sound.ITEM_ARMOR_EQUIP_DIAMOND);
                    break;
                }
                case DIAMOND_BOOTS: {
                    equipEvent(event, EquipmentSlot.FEET, Sound.ITEM_ARMOR_EQUIP_DIAMOND);
                    break;
                }
                case CHAINMAIL_HELMET: {
                    equipEvent(event, EquipmentSlot.HEAD, Sound.ITEM_ARMOR_EQUIP_CHAIN);
                    break;
                }
                case CHAINMAIL_CHESTPLATE: {
                    equipEvent(event, EquipmentSlot.CHEST, Sound.ITEM_ARMOR_EQUIP_CHAIN);
                    break;
                }
                case CHAINMAIL_LEGGINGS: {
                    equipEvent(event, EquipmentSlot.LEGS, Sound.ITEM_ARMOR_EQUIP_CHAIN);
                    break;
                }
                case CHAINMAIL_BOOTS: {
                    equipEvent(event, EquipmentSlot.FEET, Sound.ITEM_ARMOR_EQUIP_CHAIN);
                    break;
                }
                case ELYTRA: {
                    equipEvent(event, EquipmentSlot.CHEST, Sound.ITEM_ARMOR_EQUIP_GENERIC);
                    break;
                }
            }
        } else {
            switch (event.getAction()) {
                case LEFT_CLICK_AIR:
                case LEFT_CLICK_BLOCK: {
                    CodingUtils.getParser(world).onPlayerLeftInteract(event);
                    break;
                }
                case RIGHT_CLICK_AIR:
                case RIGHT_CLICK_BLOCK: {
                    CodingUtils.getParser(world).onPlayerRightInteract(event);
                    break;
                }
                case PHYSICAL: {
                    CodingUtils.getParser(world).onPlayerPhysicalInteract(event);
                    break;
                }
            }
        }
    }

    public void equipEvent(PlayerInteractEvent event, EquipmentSlot slot, Sound sound) {
        if (event.getAction() == Action.RIGHT_CLICK_AIR) {
            event.setCancelled(true);
            GameplayUtils.equipArmor(event.getPlayer(), slot, event.getItem(), event.getHand(), sound);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onInventoryClick(InventoryClickEvent event) {
        switch (event.getInventory().getTitle()) {
            case "Мой мир": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                switch (event.getCurrentItem().getType()) {
                    case MAP: {
                        event.getWhoClicked().openInventory(Menus.selectGenerator);
                        break;
                    }
                    case SAPLING: {
                        Player player = (Player) event.getWhoClicked();
                        player.closeInventory();
                        WorldManager.createWorld(player, WorldManager.SKYBLOCK, 7);
                        break;
                    }
                    case BRICK: {
                        Player player = (Player) event.getWhoClicked();
                        player.closeInventory();
                        WorldManager.createWorld(player, null, 1);
                        break;
                    }
                    case PAINTING: {
                        Player player = (Player) event.getWhoClicked();
                        if (WorldManager.getWorldFolder(player.getUniqueId().toString()).isDirectory())
                            WorldManager.createWorld(player, null, 17);
                        else
                            player.openInventory(ArtStudioUtils.selectGeneratorMenu);
                        break;
                    }
                    case DROPPER: {
                        event.getWhoClicked().openInventory(Menus.myWorldArena);
                        break;
                    }
                    case BARRIER: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        WorldManager.removeDataHash(player.getUniqueId().toString());
                        break;
                    }
                    case LAVA_BUCKET: {
                        event.getWhoClicked().openInventory(Menus.deleteWorld);
                        break;
                    }
                }
                break;
            }
            case "Арена": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                switch (event.getSlot()) {
                    case 10: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        player.teleport(GameplayUtils.setArenaType(player.getUniqueId().toString(), 2).getSpawnLocation());
                        break;
                    }
                    case 11: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        player.teleport(GameplayUtils.setArenaType(player.getUniqueId().toString(), 3).getSpawnLocation());
                        break;
                    }
                    case 12: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        player.teleport(GameplayUtils.setArenaType(player.getUniqueId().toString(), 4).getSpawnLocation());
                        break;
                    }
                    case 13: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        player.teleport(GameplayUtils.setArenaType(player.getUniqueId().toString(), 5).getSpawnLocation());
                        break;
                    }
                    case 14: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        player.teleport(GameplayUtils.setArenaType(player.getUniqueId().toString(), 6).getSpawnLocation());
                        break;
                    }
                    case 15: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        player.teleport(GameplayUtils.setArenaType(player.getUniqueId().toString(), 8).getSpawnLocation());
                        break;
                    }
                    case 16: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        player.teleport(GameplayUtils.setArenaType(player.getUniqueId().toString(), 10).getSpawnLocation());
                        break;
                    }
                    case 20: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        player.teleport(GameplayUtils.setArenaType(player.getUniqueId().toString(), 11).getSpawnLocation());
                        break;
                    }
                    case 21: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        player.teleport(GameplayUtils.setArenaType(player.getUniqueId().toString(), 12).getSpawnLocation());
                        break;
                    }
                    case 22: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        player.teleport(GameplayUtils.setArenaType(player.getUniqueId().toString(), 13).getSpawnLocation());
                        break;
                    }
                    case 23: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        player.teleport(GameplayUtils.setArenaType(player.getUniqueId().toString(), 14).getSpawnLocation());
                        break;
                    }
                    case 24: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        if (CodingUtils.isNoErrors(player.getUniqueId().toString()))
                            player.teleport(GameplayUtils.setArenaType(player.getUniqueId().toString(), 18).getSpawnLocation());
                        else
                            player.sendMessage("В коде есть ошибки. Войдите в режим строительства и исправьте их.");
                        break;
                    }
                    case 40: {
                        HumanEntity player = event.getWhoClicked();
                        String uuid = player.getUniqueId().toString();
                        World world = LiteGame.server.getWorld(uuid);
                        player.closeInventory();
                        if (world != null)
                            WorldManager.closeWorld(world, false);
                        LiteGame.server.getScheduler().scheduleSyncDelayedTask(LiteGame.plugin, () -> WorldManager.removeType(uuid), 1);
                        break;
                    }
                }
                break;
            }
            case "Особые блоки": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                ItemStack currentItem = event.getCurrentItem();
                if (currentItem.getType() != Material.AIR) {
                    Player player = (Player) event.getWhoClicked();
                    PlayerInventory inventory = player.getInventory();
                    player.closeInventory();
                    if (!inventory.contains(currentItem))
                        inventory.addItem(currentItem);
                    else
                        inventory.removeItem(currentItem);
                }
                break;
            }
            case "Студия анимации": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                ArtStudioUtils.clickWorldMenu(event);
                break;
            }
            case "Анимация": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                ArtStudioUtils.clickArtMenu(event);
                break;
            }
            case "Холст": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                ArtStudioUtils.clickSelectGeneratorMenu(event);
                break;
            }
            case "Цвет": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                ArtStudioUtils.clickColorMenu(event);
                break;
            }
            case "Управление рисованием": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                ArtStudioUtils.clickDrawSettingsMenu(event);
                break;
            }
            case "Набор": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                switch (event.getSlot()) {
                    case 11: {
                        KitPVP.join(event.getWhoClicked(), KitPVP.DEFAULT);
                        break;
                    }
                    case 13: {
                        KitPVP.join(event.getWhoClicked(), KitPVP.ARCHER);
                        break;
                    }
                    case 15: {
                        KitPVP.join(event.getWhoClicked(), KitPVP.BERSERKER);
                        break;
                    }
                }
                break;
            }
            case "Гаджеты": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                ItemStack currentItem = event.getCurrentItem();
                if (currentItem.getType() != Material.AIR) {
                    Player player = (Player) event.getWhoClicked();
                    player.closeInventory();
                    PlayerInventory inventory = player.getInventory();
                    if (!inventory.contains(currentItem)) {
                        if (inventory.getItem(4) != null)
                            inventory.addItem(currentItem);
                        else
                            inventory.setItem(4, currentItem);
                    } else
                        inventory.removeItem(currentItem);
                }
                break;
            }
            case "Войдите в аккаунт": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                if (event.getSlot() == 13) {
                    HumanEntity entity = event.getWhoClicked();
                    entity.closeInventory();
                    entity.sendMessage("Вы не вошли в аккаунт. �спользуйте команду§7 /pass");
                }
                break;
            }
            case "Управление миром": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                switch (event.getSlot()) {
                    case 10: {
                        event.getWhoClicked().openInventory(Menus.saveWorld);
                        break;
                    }
                    case 12: {
                        HumanEntity player = event.getWhoClicked();
                        player.openInventory(Menus.buildSettings(player.getWorld()));
                        break;
                    }
                    case 14: {
                        event.getWhoClicked().openInventory(Menus.setTime);
                        break;
                    }
                    case 16: {
                        event.getWhoClicked().openInventory(Menus.setMusic);
                        break;
                    }
                    case 27: {
                        HumanEntity player = event.getWhoClicked();
                        player.openInventory(Menus.kickPlayer(player.getWorld()));
                        break;
                    }
                    case 29: {
                        HumanEntity player = event.getWhoClicked();
                        player.openInventory(Menus.loginManager(player.getWorld().getName()));
                        break;
                    }
                    case 31: {
                        HumanEntity player = event.getWhoClicked();
                        player.openInventory(Menus.flyingSettings(player.getWorld()));
                        break;
                    }
                    case 33: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        if (event.isLeftClick())
                            player.getWorld().setSpawnLocation(player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ());
                        else
                            player.getWorld().setSpawnLocation(0, 1, 0);
                        break;
                    }
                    case 35: {
                        event.getWhoClicked().openInventory(Menus.setWeather);
                        break;
                    }
                }
                break;
            }
            case "Настройки": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                switch (event.getSlot()) {
                    case 10: {
                        Player player = (Player) event.getWhoClicked();
                        player.closeInventory();
                        PlayersUtils.switchFunction(player.getUniqueId().toString(), "stackingDisabled");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, Float.MAX_VALUE, 1);
                        break;
                    }
                    case 12: {
                        Player player = (Player) event.getWhoClicked();
                        player.closeInventory();
                        PlayersUtils.switchFunction(player.getUniqueId().toString(), "musicInHubDisabled");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, Float.MAX_VALUE, 1);
                        break;
                    }
                    case 14: {
                        Player player = (Player) event.getWhoClicked();
                        player.closeInventory();
                        PlayersUtils.switchFunction(player.getUniqueId().toString(), "privateMessagesDisabled");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, Float.MAX_VALUE, 1);
                        break;
                    }
                    case 16: {
                        Player player = (Player) event.getWhoClicked();
                        player.closeInventory();
                        PlayersUtils.switchFunction(player.getUniqueId().toString(), "disableSoundNotify");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, Float.MAX_VALUE, 1);
                        break;
                    }
                }
                break;
            }
            case "Управление SkyBlock": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                switch (event.getSlot()) {
                    case 10: {
                        WorldManager.closeSkyBlock(event.getWhoClicked().getWorld());
                        break;
                    }
                    case 12: {
                        HumanEntity player = event.getWhoClicked();
                        player.openInventory(Menus.buildSettings(player.getWorld()));
                        break;
                    }
                    case 14: {
                        HumanEntity player = event.getWhoClicked();
                        player.openInventory(Menus.loginManager(player.getWorld().getName()));
                        break;
                    }
                    case 16: {
                        HumanEntity player = event.getWhoClicked();
                        player.openInventory(Menus.kickPlayer(player.getWorld()));
                        break;
                    }
                }
                break;
            }
            case "Удалить мир": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                switch (event.getSlot()) {
                    case 12: {
                        HumanEntity player = event.getWhoClicked();
                        String uuid = player.getUniqueId().toString();
                        player.closeInventory();
                        switch (WorldManager.getType(uuid)) {
                            default: {
                                FileUtils.deleteDirectory(WorldManager.getWorldFolder(uuid));
                                break;
                            }
                            case 7: {
                                SkyBlockUtils.removeSkyBlock(uuid);
                                WorldManager.removeDataHash(uuid);
                                FileUtils.deleteDirectory(WorldManager.getWorldFolder(uuid));
                                break;
                            }
                            case 17: {
                                ArtStudioUtils.removeArts(uuid);
                                WorldManager.removeDataHash(uuid);
                                FileUtils.deleteDirectory(WorldManager.getWorldFolder(uuid));
                                break;
                            }
                            case 18: {
                                CodingUtils.removeCode(uuid);
                                FileUtils.deleteDirectory(WorldManager.getWorldFolder(uuid));
                                break;
                            }
                        }
                        break;
                    }
                    case 14: {
                        event.getWhoClicked().closeInventory();
                        break;
                    }
                }
                break;
            }
            case "Сохранить мир": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                switch (event.getSlot()) {
                    case 11: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        player.getWorld().save();
                        break;
                    }
                    case 13: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        WorldManager.closeWorld(player.getWorld(), true);
                        break;
                    }
                    case 15: {
                        HumanEntity player = event.getWhoClicked();
                        World world = player.getWorld();
                        player.closeInventory();
                        world.setAutoSave(false);
                        WorldManager.closeWorld(world, false);
                        break;
                    }
                }
                break;
            }
            case "Время": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                switch (event.getSlot()) {
                    case 10: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        player.getWorld().setTime(0);
                        break;
                    }
                    case 12: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        player.getWorld().setTime(6000);
                        break;
                    }
                    case 14: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        player.getWorld().setTime(13000);
                        break;
                    }
                    case 16: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        player.getWorld().setTime(18000);
                        break;
                    }
                }
                break;
            }
            case "Генерация": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                switch (event.getSlot()) {
                    case 11: {
                        Player player = (Player) event.getWhoClicked();
                        player.closeInventory();
                        WorldManager.createWorld(player, WorldManager.FLAT, 1);
                        break;
                    }
                    case 13: {
                        Player player = (Player) event.getWhoClicked();
                        player.closeInventory();
                        WorldManager.createWorld(player, WorldManager.VOID, 1);
                        break;
                    }
                    case 15: {
                        Player player = (Player) event.getWhoClicked();
                        player.closeInventory();
                        if (PlayersUtils.isDonator(player.getUniqueId().toString()))
                            WorldManager.createWorld(player, WorldManager.ISLAND, 1);
                        else
                            player.sendMessage("Нужен донат.");
                        break;
                    }
                    case 30: {
                        Player player = (Player) event.getWhoClicked();
                        player.closeInventory();
                        WorldManager.createWorld(player, WorldManager.SNOW_FLAT, 1);
                        break;
                    }
                    case 32: {
                        Player player = (Player) event.getWhoClicked();
                        player.closeInventory();
                        if (PlayersUtils.isDonator(player.getUniqueId().toString()))
                            WorldManager.createWorld(player, WorldManager.SNOW_ISLAND, 1);
                        else
                            player.sendMessage("Нужен донат.");
                        break;
                    }
                }
                break;
            }
            case "Погода": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                switch (event.getSlot()) {
                    case 12: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        player.getWorld().setStorm(false);
                        player.getWorld().setWeatherDuration(0);
                        break;
                    }
                    case 14: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        player.getWorld().setStorm(true);
                        player.getWorld().setWeatherDuration(Integer.MAX_VALUE);
                        break;
                    }
                }
                break;
            }
            case "Меню": {
                NavigatorUtils.clickDefault(event);
                break;
            }
            case "Building":
            case "Survival":
            case "ArtStudio": {
                NavigatorUtils.clickLoginable(event);
                break;
            }
            case "SkyBlock": {
                NavigatorUtils.clickSkyBlock(event);
                break;
            }
            case "PVP":
            case "Parkour":
            case "Splegg":
            case "ThrowOut":
            case "Duel":
            case "Murder":
            case "Paintball":
            case "TNTRun":
            case "KitPVP":
            case "SkyWars":
            case "SurvivalGames":
            case "Game": {
                NavigatorUtils.clickArena(event);
                break;
            }
            case "Управление входом": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                switch (event.getCurrentItem().getType()) {
                    case WOOD_DOOR: {
                        HumanEntity player = event.getWhoClicked();
                        String world = event.getWhoClicked().getWorld().getName();
                        player.closeInventory();
                        if (WorldManager.getLogin(world) != 0) {
                            WorldManager.removeLogin(world);
                            NavigatorUtils.updateMenu(WorldManager.getType(world));
                        }
                        break;
                    }
                    case IRON_DOOR: {
                        HumanEntity player = event.getWhoClicked();
                        String world = event.getWhoClicked().getWorld().getName();
                        player.closeInventory();
                        if (WorldManager.getLogin(world) != 1) {
                            WorldManager.setLogin(world, 1);
                            NavigatorUtils.updateMenu(WorldManager.getType(world));
                        }
                        break;
                    }
                    case NOTE_BLOCK: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        if (PlayersUtils.isDonator(player.getUniqueId().toString())) {
                            String world = event.getWhoClicked().getWorld().getName();
                            if (WorldManager.getLogin(world) != 2) {
                                WorldManager.setLogin(world, 2);
                                NavigatorUtils.updateMenu(WorldManager.getType(world));
                            }
                        }
                        break;
                    }
                    case BARRIER: {
                        HumanEntity player = event.getWhoClicked();
                        String world = event.getWhoClicked().getWorld().getName();
                        player.closeInventory();
                        if (WorldManager.getLogin(world) != 3) {
                            WorldManager.setLogin(world, 3);
                            NavigatorUtils.updateMenu(WorldManager.getType(world));
                        }
                        break;
                    }
                }
                break;
            }
            case "�нформация": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                switch (event.getSlot()) {
                    case 11: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        player.sendMessage("Группа вконтакте:§2 " + ServerSettings.vkLink);
                        break;
                    }
                    case 13: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        player.sendMessage("Купить донат:§2 " + ServerSettings.donateLink);
                        break;
                    }
                }
                break;
            }
            case "Музыка": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                switch (event.getSlot()) {
                    case 10: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        MusicUtils.playMusic(player.getWorld(), "record.cat");
                        break;
                    }
                    case 11: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        MusicUtils.playMusic(player.getWorld(), "record.chirp");
                        break;
                    }
                    case 12: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        MusicUtils.playMusic(player.getWorld(), "record.far");
                        break;
                    }
                    case 13: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        MusicUtils.playMusic(player.getWorld(), "record.mall");
                        break;
                    }
                    case 14: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        MusicUtils.playMusic(player.getWorld(), "record.stal");
                        break;
                    }
                    case 15: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        MusicUtils.playMusic(player.getWorld(), "record.strad");
                        break;
                    }
                    case 16: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        MusicUtils.playMusic(player.getWorld(), "record.wait");
                        break;
                    }
                    case 19: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        MusicUtils.playMusic(player.getWorld(), "music.credits");
                        break;
                    }
                    case 40: {
                        HumanEntity player = event.getWhoClicked();
                        player.closeInventory();
                        MusicUtils.stopMusic(player.getWorld());
                        break;
                    }
                }
                break;
            }
            case "Выгнать игрока": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                ItemStack currentItem = event.getCurrentItem();
                if (currentItem.getType() != Material.AIR) {
                    HumanEntity player = event.getWhoClicked();
                    Player playerToKick = LiteGame.server.getPlayer(currentItem.getItemMeta().getLocalizedName());
                    player.closeInventory();
                    if (playerToKick != null) {
                        if (player.getWorld() == playerToKick.getWorld()) {
                            playerToKick.sendTitle(Titles.kicked);
                            if (WorldManager.getType(player.getWorld().getName()) == 7)
                                SkyBlockUtils.savePlayer(playerToKick);
                            playerToKick.teleport(Hub.location);
                        }
                    }
                }
                break;
            }
            case "Управление строительством": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                ItemStack currentItem = event.getCurrentItem();
                switch (currentItem.getType()) {
                    case SKULL_ITEM: {
                        HumanEntity player = event.getWhoClicked();
                        Player currentPlayer = LiteGame.server.getPlayer(currentItem.getItemMeta().getLocalizedName());
                        player.closeInventory();
                        if (currentPlayer != null) {
                            if (currentPlayer.getWorld() == player.getWorld()) {
                                boolean allowFlying = currentPlayer.getAllowFlight();
                                if (currentPlayer.getGameMode() == GameMode.ADVENTURE) {
                                    currentPlayer.setGameMode(WorldManager.getType(player.getWorld().getName()) != 7 ? GameMode.CREATIVE : GameMode.SURVIVAL);
                                    currentPlayer.setAllowFlight(allowFlying);
                                } else {
                                    currentPlayer.setGameMode(GameMode.ADVENTURE);
                                    if (allowFlying) {
                                        currentPlayer.setAllowFlight(true);
                                        currentPlayer.setFlying(true);
                                    }
                                }
                            }
                        }
                        break;
                    }
                    case BRICK: {
                        HumanEntity player = event.getWhoClicked();
                        String worldName = player.getWorld().getName();
                        player.closeInventory();
                        if (!WorldManager.getAllowBuild(worldName))
                            WorldManager.setAllowBuild(worldName);
                        else
                            WorldManager.setDisllowBuild(worldName);
                    }
                }
                break;
            }
            case "Управление полётом": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                ItemStack currentItem = event.getCurrentItem();
                switch (currentItem.getType()) {
                    case SKULL_ITEM: {
                        HumanEntity player = event.getWhoClicked();
                        Player currentPlayer = LiteGame.server.getPlayer(currentItem.getItemMeta().getLocalizedName());
                        player.closeInventory();
                        if (currentPlayer != null) {
                            if (currentPlayer.getWorld() == player.getWorld())
                                currentPlayer.setAllowFlight(!currentPlayer.getAllowFlight());
                        }
                        break;
                    }
                    case FEATHER: {
                        HumanEntity player = event.getWhoClicked();
                        String worldName = player.getWorld().getName();
                        player.closeInventory();
                        if (!WorldManager.getAllowFlying(worldName))
                            WorldManager.setAllowFlying(worldName);
                        else
                            WorldManager.setDisllowFlying(worldName);
                    }
                }
                break;
            }
            case "Задания": {
                event.setCancelled(true);
                if (event.getClickedInventory() != event.getInventory())
                    break;
                if (event.getSlot() == 13) {
                    Player player = (Player) event.getWhoClicked();
                    player.closeInventory();
                    SkyBlockUtils.runQuestCheck(player);
                }
                break;
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onRedstone(BlockRedstoneEvent event) {
        Block block = event.getBlock();
        event.setNewCurrent(0);
        switch (block.getType()) {
            case WOOD_PLATE: {
                GameplayUtils.runPlateEvent(block);
                break;
            }
            case WOOD_BUTTON: {
                block.getWorld().playSound(block.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
                break;
            }
            case STONE_PLATE: {
                GameplayUtils.runPlateEvent(block);
                break;
            }
            case STONE_BUTTON: {
                block.getWorld().playSound(block.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
                break;
            }
            case IRON_PLATE: {
                GameplayUtils.runPlateEvent(block);
                break;
            }
            case GOLD_PLATE: {
                GameplayUtils.runPlateEvent(block);
                break;
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onSignChange(SignChangeEvent event) {
        StringBuilder stringBuilder = new StringBuilder("");
        String[] lines = event.getLines();
        for (String strings : lines)
            stringBuilder.append(strings);
        String message = stringBuilder.toString();
        if (ChatUtils.hasIp(message) || ChatUtils.hasWeb(message)) {
            event.setCancelled(true);
            event.getBlock().breakNaturally();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onDrop(PlayerDropItemEvent event) {
        switch (WorldManager.getType(event.getPlayer().getWorld().getName())) {
            default: {
                event.setCancelled(true);
                break;
            }
            case 7:
            case 13:
            case 14:
            case 18:
                break;
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onExplosion(EntityExplodeEvent event) {
        event.setCancelled(true);
        event.getEntity().remove();
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPrimeExplosion(ExplosionPrimeEvent event) {
        event.setCancelled(true);
        event.getEntity().remove();
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onChat(AsyncPlayerChatEvent event) {
        event.setCancelled(true);
        ChatUtils.chat(event.getPlayer(), event.getMessage());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onStrucutreGrow(StructureGrowEvent event) {
        switch (event.getSpecies()) {
            case TREE: {
                if (LiteGame.random.nextBoolean()) {
                    event.setCancelled(true);
                    Trees.generate(event.getLocation().getBlock(), Trees.APPLE_TREE);
                }
                break;
            }
            case BIG_TREE: {
                if (LiteGame.random.nextBoolean()) {
                    event.setCancelled(true);
                    Trees.generate(event.getLocation().getBlock(), Trees.POPLAR);
                }
                break;
            }
            case SMALL_JUNGLE: {
                if (LiteGame.random.nextBoolean()) {
                    event.setCancelled(true);
                    Trees.generate(event.getLocation().getBlock(), Trees.PALM);
                }
                break;
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPortalCreate(PortalCreateEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBookEdit(PlayerEditBookEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();
        if (player.getWorld().getName().equals(uuid)) {
            if (!event.isSigning()) {
                if (CodingUtils.saveAndCheckForError(player, event.getNewBookMeta().getPages())) {
                    player.sendMessage("Код сохранён. Ошибок нет.");
                    CodingUtils.setNoErrors(uuid);
                } else
                    CodingUtils.removeNoErrors(uuid);
            } else {
                event.setCancelled(true);
                player.sendMessage("Код не сохранён. В следующий раз нажмите кнопку «Готово», чтобы сохранить код.");
            }
        } else
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBlockPlace(BlockPlaceEvent event) {
        Block block = event.getBlock();
        switch (block.getType()) {
            case SKULL: {
                event.setCancelled(true);
                break;
            }
            case STANDING_BANNER:
            case WALL_BANNER: {
                if (!((Banner) block.getState()).getPatterns().isEmpty())
                    event.setCancelled(true);
                break;
            }
            case WHITE_SHULKER_BOX:
            case ORANGE_SHULKER_BOX:
            case MAGENTA_SHULKER_BOX:
            case LIGHT_BLUE_SHULKER_BOX:
            case YELLOW_SHULKER_BOX:
            case LIME_SHULKER_BOX:
            case PINK_SHULKER_BOX:
            case GRAY_SHULKER_BOX:
            case SILVER_SHULKER_BOX:
            case CYAN_SHULKER_BOX:
            case PURPLE_SHULKER_BOX:
            case BLUE_SHULKER_BOX:
            case BROWN_SHULKER_BOX:
            case GREEN_SHULKER_BOX:
            case RED_SHULKER_BOX:
            case BLACK_SHULKER_BOX: {
                ((ShulkerBox) block.getState()).getInventory().clear();
                break;
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBlockFall(BlockPhysicsEvent event) {
        switch (WorldManager.getType(event.getBlock().getWorld().getName())) {
            default: {
                event.setCancelled(true);
                break;
            }
            case 7:
            case 13:
            case 14:
                break;
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBlockFlow(BlockFromToEvent event) {
        switch (WorldManager.getType(event.getBlock().getWorld().getName())) {
            default: {
                event.setCancelled(true);
                break;
            }
            case 7:
            case 13:
            case 14:
                break;
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBlockExplode(BlockExplodeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBucketEmpty(PlayerBucketEmptyEvent event) {
        if (event.getPlayer().getGameMode() == GameMode.ADVENTURE)
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBucketFill(PlayerBucketFillEvent event) {
        if (event.getPlayer().getGameMode() == GameMode.ADVENTURE)
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onThunderChange(ThunderChangeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onLightningStrike(LightningStrikeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBlockIgnite(BlockIgniteEvent event) {
        switch (WorldManager.getType(event.getBlock().getWorld().getName())) {
            default: {
                event.setCancelled(true);
                break;
            }
            case 7:
            case 13:
            case 14:
                break;
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onLeavesDecay(LeavesDecayEvent event) {
        switch (WorldManager.getType(event.getBlock().getWorld().getName())) {
            default: {
                event.setCancelled(true);
                break;
            }
            case 7:
            case 13:
            case 14:
                break;
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBlockMelt(BlockFadeEvent event) {
        switch (WorldManager.getType(event.getBlock().getWorld().getName())) {
            default: {
                event.setCancelled(true);
                break;
            }
            case 7:
            case 13:
            case 14:
                break;
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onEntitySpawn(EntitySpawnEvent event) {
        Entity entity = event.getEntity();
        switch (WorldManager.getType(entity.getWorld().getName())) {
            default: {
                event.setCancelled(true);
                break;
            }
            case 7:
            case 13:
            case 14: {
                if (entity instanceof LivingEntity)
                    event.setCancelled(true);
                break;
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPotionSplash(PotionSplashEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBeaconEffect(BeaconEffectEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onWorldChange(PlayerChangedWorldEvent event) {
        Player player = event.getPlayer();
        PlayerInventory playerInventory = player.getInventory();
        World world = player.getWorld();
        World from = event.getFrom();
        String worldName = world.getName();
        String fromName = from.getName();
        int fromType = WorldManager.getType(fromName);
        switch (fromType) {
            case 0: {
                NavigatorUtils.updateMenu(fromType);
                break;
            }
            case 1:
            case 17: {
                ChatUtils.broadcastMessage(player.getDisplayName() + "§r покинул игру.", from.getPlayers());
                if (player.getUniqueId().toString().equals(fromName) || from.getPlayers().isEmpty())
                    WorldManager.closeWorld(from, true);
                else
                    NavigatorUtils.updateMenu(fromType);
                break;
            }
            case 7: {
                ChatUtils.broadcastMessage(player.getDisplayName() + "§r покинул игру.", from.getPlayers());
                if (!player.getUniqueId().toString().equals(fromName) && !from.getPlayers().isEmpty()) {
                    SkyBlockUtils.savePlayer(player);
                    NavigatorUtils.updateMenu(fromType);
                } else
                    WorldManager.closeSkyBlock(from);
                break;
            }
            case 18: {
                CodingUtils.getParser(from).onPlayerQuit(event);
                player.getActivePotionEffects().forEach(potions -> player.removePotionEffect(potions.getType()));
                break;
            }
            default: {
                ChatUtils.broadcastMessage(player.getDisplayName() + "§r покинул игру.", from.getPlayers());
                if (GameUtils.tryEndGame(from)) ;
                NavigatorUtils.updateMenu(fromType);
                break;
            }
        }
        playerInventory.clear();
        GameplayUtils.runWorldTabPlayers(player);
        switch (WorldManager.getType(worldName)) {
            default: {
                String uuid = player.getUniqueId().toString();
                player.setGameMode(GameMode.ADVENTURE);
                GameplayUtils.giveHubItems(playerInventory);
                player.setAllowFlight(PlayersUtils.isLoggined(uuid) && PlayersUtils.isDonator(uuid));
                player.setHealth(20);
                if (!PlayersUtils.isEnabled(uuid, "musicInHubDisabled")) {
                    String music = MusicUtils.randomMusic();
                    player.playSound(Hub.location, music, Integer.MAX_VALUE, MusicUtils.getMusicPitch(music));
                }
                player.setPlayerListHeaderFooter(ServerSettings.tabHeader, TextComponents.hub);
                break;
            }
            case 1: {
                boolean playerIsWorldOwner = worldName.equals(player.getUniqueId().toString());
                player.setGameMode
                        (
                                playerIsWorldOwner || WorldManager.getAllowBuild(worldName)
                                        ? GameMode.CREATIVE
                                        : GameMode.ADVENTURE
                        );
                if (playerIsWorldOwner)
                    playerInventory.setItem(0, CodingUtils.generateBook(worldName));
                playerInventory.setItem(7, Items.otherBlocks);
                playerInventory.setItem(8, Items.menu);
                player.setAllowFlight(WorldManager.getAllowFlying(worldName));
                if (WorldManager.hasMusic(worldName)) {
                    String playingMusic = WorldManager.getMusic(worldName);
                    player.playSound(world.getSpawnLocation(), playingMusic, Integer.MAX_VALUE, MusicUtils.getMusicPitch(playingMusic));
                }
                player.setPlayerListHeaderFooter
                        (
                                ServerSettings.tabHeader,
                                new TextComponent("Building " + WorldManager.getDisplayName(worldName))
                        );
                ChatUtils.broadcastMessage(player.getDisplayName() + "§r присоединился к игре.", world.getPlayers());
                NavigatorUtils.updateMenu(1);
                break;
            }
            case 2: {
                player.setGameMode(GameMode.ADVENTURE);
                playerInventory.setItem(0, Items.unbreakableIronSword);
                playerInventory.setItem(8, Items.menu);
                player.setAllowFlight(false);
                player.setNoDamageTicks(50);
                player.setPlayerListHeaderFooter
                        (
                                ServerSettings.tabHeader,
                                new TextComponent("PVP " + WorldManager.getDisplayName(worldName))
                        );
                ChatUtils.broadcastMessage(player.getDisplayName() + "§r присоединился к игре.", world.getPlayers());
                NavigatorUtils.updateMenu(2);
                break;
            }
            case 3: {
                player.setGameMode(GameMode.ADVENTURE);
                playerInventory.setItem(0, Items.parkourRespawn);
                playerInventory.setItem(8, Items.menu);
                player.setAllowFlight(false);
                player.setPlayerListHeaderFooter
                        (
                                ServerSettings.tabHeader,
                                new TextComponent("Parkour " + WorldManager.getDisplayName(worldName))
                        );
                ChatUtils.broadcastMessage(player.getDisplayName() + "§r присоединился к игре.", world.getPlayers());
                player.setCooldown(Material.DIAMOND, 60);
                if (world.getEntitiesByClass(EnderCrystal.class).isEmpty())
                    player.sendMessage("Арена построена неправильно: нет финиша.");
                NavigatorUtils.updateMenu(3);
                break;
            }
            case 4: {
                int crystals = world.getEntitiesByClass(EnderCrystal.class).size();
                player.setGameMode(GameMode.ADVENTURE);
                if (world.getName().equals(player.getUniqueId().toString()))
                    playerInventory.setItem(0, Items.startGame);
                playerInventory.setItem(8, Items.menu);
                player.setAllowFlight(true);
                player.setPlayerListHeaderFooter
                        (
                                ServerSettings.tabHeader,
                                new TextComponent("Splegg " + WorldManager.getDisplayName(worldName))
                        );
                ChatUtils.broadcastMessage(player.getDisplayName() + "§r присоединился к игре§8 (§r" + world.getPlayerCount() + "§7/§r" + crystals + "§8)", world.getPlayers());
                if (crystals <= 1) {
                    player.sendMessage("Арена построена неправильно: нет точек появления.");
                    NavigatorUtils.updateMenu(4);
                } else if (world.getPlayerCount() == crystals)
                    GameUtils.startGame(world);
                else
                    NavigatorUtils.updateMenu(4);
                break;
            }
            case 5: {
                int crystals = world.getEntitiesByClass(EnderCrystal.class).size();
                player.setGameMode(GameMode.ADVENTURE);
                if (world.getName().equals(player.getUniqueId().toString()))
                    playerInventory.setItem(0, Items.startGame);
                playerInventory.setItem(8, Items.menu);
                player.setAllowFlight(true);
                player.setPlayerListHeaderFooter
                        (
                                ServerSettings.tabHeader,
                                new TextComponent("ThrowOut " + WorldManager.getDisplayName(worldName))
                        );
                ChatUtils.broadcastMessage(player.getDisplayName() + "§r присоединился к игре§8 (§r" + world.getPlayerCount() + "§7/§r" + crystals + "§8)", world.getPlayers());
                if (crystals <= 1) {
                    player.sendMessage("Арена построена неправильно: нет точек появления.");
                    NavigatorUtils.updateMenu(5);
                } else if (world.getPlayerCount() == crystals)
                    GameUtils.startGame(world);
                else
                    NavigatorUtils.updateMenu(5);
                break;
            }
            case 6: {
                int crystals = world.getEntitiesByClass(EnderCrystal.class).size();
                player.setGameMode(GameMode.ADVENTURE);
                playerInventory.setItem(8, Items.menu);
                player.setAllowFlight(true);
                player.setPlayerListHeaderFooter
                        (
                                ServerSettings.tabHeader,
                                new TextComponent("Duel " + WorldManager.getDisplayName(worldName))
                        );
                ChatUtils.broadcastMessage(player.getDisplayName() + "§r присоединился к игре§8 (§r" + world.getPlayerCount() + "§7/§r" + crystals + "§8)", world.getPlayers());
                if (crystals != 2) {
                    player.sendMessage("Арена построена неправильно: нужно 2 точки появления.");
                    NavigatorUtils.updateMenu(6);
                } else if (world.getPlayerCount() == crystals)
                    GameUtils.startGame(world);
                else
                    NavigatorUtils.updateMenu(6);
                break;
            }
            case 7: {
                player.setGameMode
                        (
                                world.getName().equals(player.getUniqueId().toString())
                                        ? GameMode.SURVIVAL
                                        : GameMode.ADVENTURE
                        );
                player.setAllowFlight(false);
                SkyBlockUtils.loadPlayer(player);
                player.setPlayerListHeaderFooter
                        (
                                ServerSettings.tabHeader,
                                new TextComponent("SkyBlock " + WorldManager.getDisplayName(worldName))
                        );
                ChatUtils.broadcastMessage(player.getDisplayName() + "§r присоединился к игре.", world.getPlayers());
                NavigatorUtils.updateMenu(7);
                break;
            }
            case 8: {
                int crystals = world.getEntitiesByClass(EnderCrystal.class).size();
                player.setGameMode(GameMode.ADVENTURE);
                if (world.getName().equals(player.getUniqueId().toString()))
                    playerInventory.setItem(0, Items.startGame);
                playerInventory.setItem(8, Items.menu);
                player.setAllowFlight(true);
                player.setPlayerListHeaderFooter
                        (
                                ServerSettings.tabHeader,
                                new TextComponent("Murder " + WorldManager.getDisplayName(worldName))
                        );
                ChatUtils.broadcastMessage(player.getDisplayName() + "§r присоединился к игре§8 (§r" + world.getPlayerCount() + "§7/§r" + crystals + "§8)", world.getPlayers());
                if (crystals <= 1) {
                    player.sendMessage("Арена построена неправильно: нет точек появления.");
                    NavigatorUtils.updateMenu(8);
                } else if (world.getPlayerCount() == crystals)
                    GameUtils.startGame(world);
                else
                    NavigatorUtils.updateMenu(8);
                break;
            }
            case 10: {
                int crystals = world.getEntitiesByClass(EnderCrystal.class).size();
                player.setGameMode(GameMode.ADVENTURE);
                if (world.getName().equals(player.getUniqueId().toString()))
                    playerInventory.setItem(0, Items.startGame);
                playerInventory.setItem(8, Items.menu);
                player.setAllowFlight(true);
                player.setPlayerListHeaderFooter
                        (
                                ServerSettings.tabHeader,
                                new TextComponent("Paintball " + WorldManager.getDisplayName(worldName))
                        );
                ChatUtils.broadcastMessage(player.getDisplayName() + "§r присоединился к игре§8 (§r" + world.getPlayerCount() + "§7/§r" + crystals + "§8)", world.getPlayers());
                if (crystals <= 1) {
                    player.sendMessage("Арена построена неправильно: нет точек появления.");
                    NavigatorUtils.updateMenu(10);
                } else if (world.getPlayerCount() == crystals)
                    GameUtils.startGame(world);
                else
                    NavigatorUtils.updateMenu(10);
                break;
            }
            case 11: {
                int crystals = world.getEntitiesByClass(EnderCrystal.class).size();
                player.setGameMode(GameMode.ADVENTURE);
                if (world.getName().equals(player.getUniqueId().toString()))
                    playerInventory.setItem(0, Items.startGame);
                playerInventory.setItem(8, Items.menu);
                player.setAllowFlight(true);
                player.setPlayerListHeaderFooter
                        (
                                ServerSettings.tabHeader,
                                new TextComponent("TNTRun " + WorldManager.getDisplayName(worldName))
                        );
                ChatUtils.broadcastMessage(player.getDisplayName() + "§r присоединился к игре§8 (§r" + world.getPlayerCount() + "§7/§r" + crystals + "§8)", world.getPlayers());
                if (crystals <= 1) {
                    player.sendMessage("Арена построена неправильно: нет точек появления.");
                    NavigatorUtils.updateMenu(11);
                } else if (world.getPlayerCount() == crystals)
                    GameUtils.startGame(world);
                else
                    NavigatorUtils.updateMenu(11);
                break;
            }
            case 12: {
                player.setGameMode(GameMode.SPECTATOR);
                playerInventory.setItem(8, Items.menu);
                player.setPlayerListHeaderFooter
                        (
                                ServerSettings.tabHeader,
                                new TextComponent("KitPVP " + WorldManager.getDisplayName(worldName))
                        );
                player.openInventory(Menus.kitPvp);
                player.sendMessage("Вы наблюдатель, напишите§2 /kit§r для выбора набора.");
                ChatUtils.broadcastMessage(player.getDisplayName() + "§r присоединился к игре.", world.getPlayers());
                NavigatorUtils.updateMenu(12);
                break;
            }
            case 13: {
                int crystals = world.getEntitiesByClass(EnderCrystal.class).size();
                player.setGameMode(GameMode.ADVENTURE);
                if (world.getName().equals(player.getUniqueId().toString()))
                    playerInventory.setItem(0, Items.startGame);
                playerInventory.setItem(8, Items.menu);
                player.setAllowFlight(true);
                player.setPlayerListHeaderFooter
                        (
                                ServerSettings.tabHeader,
                                new TextComponent("SkyWars " + WorldManager.getDisplayName(worldName))
                        );
                ChatUtils.broadcastMessage(player.getDisplayName() + "§r присоединился к игре§8 (§r" + world.getPlayerCount() + "§7/§r" + crystals + "§8)", world.getPlayers());
                if (crystals <= 1) {
                    player.sendMessage("Арена построена неправильно: нет точек появления.");
                    NavigatorUtils.updateMenu(13);
                } else if (world.getPlayerCount() == crystals)
                    GameUtils.startGame(world);
                else
                    NavigatorUtils.updateMenu(13);
                break;
            }
            case 14: {
                int crystals = world.getEntitiesByClass(EnderCrystal.class).size();
                player.setGameMode(GameMode.ADVENTURE);
                if (world.getName().equals(player.getUniqueId().toString()))
                    playerInventory.setItem(0, Items.startGame);
                playerInventory.setItem(8, Items.menu);
                player.setAllowFlight(true);
                player.setPlayerListHeaderFooter
                        (
                                ServerSettings.tabHeader,
                                new TextComponent("SurvivalGames " + WorldManager.getDisplayName(worldName))
                        );
                ChatUtils.broadcastMessage(player.getDisplayName() + "§r присоединился к игре§8 (§r" + world.getPlayerCount() + "§7/§r" + crystals + "§8)", world.getPlayers());
                if (crystals <= 1) {
                    player.sendMessage("Арена построена неправильно: нет точек появления.");
                    NavigatorUtils.updateMenu(14);
                } else if (world.getPlayerCount() == crystals)
                    GameUtils.startGame(world);
                else
                    NavigatorUtils.updateMenu(14);
                break;
            }
            case 17: {
                player.setGameMode(GameMode.ADVENTURE);
                if (worldName.equals(player.getUniqueId().toString())) {
                    playerInventory.setItem(0, ArtStudioUtils.brushItem);
                    playerInventory.setItem(7, ArtStudioUtils.menuItem);
                } else if (WorldManager.getAllowBuild(worldName))
                    playerInventory.setItem(0, ArtStudioUtils.brushItem);
                playerInventory.setItem(8, Items.menu);
                player.setAllowFlight(true);
                player.setFlying(true);
                player.setPlayerListHeaderFooter
                        (
                                ServerSettings.tabHeader,
                                new TextComponent("ArtStudio " + WorldManager.getDisplayName(worldName))
                        );
                ChatUtils.broadcastMessage(player.getDisplayName() + "§r присоединился к игре.", world.getPlayers());
                NavigatorUtils.updateMenu(17);
                break;
            }
            case 18: {
                player.setGameMode(GameMode.ADVENTURE);
                player.setAllowFlight(false);
                player.setPlayerListHeaderFooter
                        (
                                ServerSettings.tabHeader,
                                new TextComponent("Game " + WorldManager.getDisplayName(worldName))
                        );
                CodingUtils.getParser(world).onPlayerJoin(event);
                NavigatorUtils.updateMenu(18);
                break;
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onDamage(EntityDamageEvent event) {
        Entity entity = event.getEntity();
        World world = entity.getWorld();
        String worldName = world.getName();
        switch (WorldManager.getType(worldName)) {
            case 0: {
                event.setCancelled(true);
                switch (event.getCause()) {
                    case VOID: {
                        entity.teleport(world.getSpawnLocation());
                        break;
                    }
                    case ENTITY_ATTACK: {
                        Entity damager = ((EntityDamageByEntityEvent) event).getDamager();
                        if (damager.getPassengers().contains(entity)) {
                            entity.leaveVehicle();
                            entity.setVelocity(entity.getVelocity().setY(1));
                        } else if (!PlayersUtils.isEnabled(entity.getUniqueId().toString(), "stackingDisabled")) {
                            if (!PlayersUtils.isEnabled(damager.getUniqueId().toString(), "stackingDisabled")) {
                                damager.leaveVehicle();
                                damager.addPassenger(entity);
                                world.playSound(damager.getLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 1);
                            } else
                                ((Player) damager).sendTitle(Titles.stackingDisabled);
                        } else
                            ((Player) damager).sendTitle(Titles.victimStackingDisabled);
                        break;
                    }
                }
                break;
            }
            case 1: {
                event.setCancelled(true);
                switch (event.getCause()) {
                    case VOID: {
                        entity.teleport(world.getSpawnLocation());
                        break;
                    }
                    case ENTITY_ATTACK: {
                        if (entity instanceof EnderCrystal) {
                            if (((Player) ((EntityDamageByEntityEvent) event).getDamager()).getGameMode() != GameMode.ADVENTURE)
                                entity.remove();
                        }
                        break;
                    }
                }
                break;
            }
            case 2: {
                if (event.getCause() == DamageCause.VOID)
                    event.setDamage(20);
                break;
            }
            case 3: {
                event.setCancelled(true);
                if (entity instanceof EnderCrystal) {
                    Player player = (Player) ((EntityDamageByEntityEvent) event).getDamager();
                    player.teleport(world.getSpawnLocation());
                    ChatUtils.broadcastMessage(player.getDisplayName() + "§r прошёл паркур!", world.getPlayers());
                } else {
                    entity.teleport(world.getSpawnLocation());
                    entity.setFallDistance(0);
                    entity.setVelocity(LocationUtils.noVelocity);
                }
                break;
            }
            case 4: {
                if (WorldManager.isStarted(worldName)) {
                    if (event.getCause() == EntityDamageEvent.DamageCause.VOID)
                        event.setDamage(20);
                } else if (event.getCause() == EntityDamageEvent.DamageCause.VOID) {
                    event.setCancelled(true);
                    entity.teleport(world.getSpawnLocation());
                } else
                    event.setCancelled(true);
                break;
            }
            case 5: {
                switch (event.getCause()) {
                    case VOID: {
                        if (WorldManager.isStarted(worldName))
                            event.setDamage(20);
                        else {
                            event.setCancelled(true);
                            entity.teleport(world.getSpawnLocation());
                        }
                        break;
                    }
                    case PROJECTILE: {
                        entity.setVelocity(entity.getLocation().getDirection().multiply(-1));
                        break;
                    }
                    default: {
                        event.setCancelled(true);
                        break;
                    }
                }
                break;
            }
            case 6: {
                if (WorldManager.isStarted(worldName)) {
                    if (event.getCause() == EntityDamageEvent.DamageCause.VOID)
                        event.setDamage(20);
                } else if (event.getCause() == EntityDamageEvent.DamageCause.VOID) {
                    event.setCancelled(true);
                    entity.teleport(world.getSpawnLocation());
                } else
                    event.setCancelled(true);
                break;
            }
            case 7:
                break;
            case 8: {
                if (WorldManager.isStarted(worldName)) {
                    if (event.getCause() == EntityDamageEvent.DamageCause.VOID)
                        event.setDamage(20);
                } else if (event.getCause() == EntityDamageEvent.DamageCause.VOID) {
                    event.setCancelled(true);
                    entity.teleport(world.getSpawnLocation());
                } else
                    event.setCancelled(true);
                break;
            }
            case 10: {
                switch (event.getCause()) {
                    case VOID: {
                        if (WorldManager.isStarted(worldName))
                            event.setDamage(20);
                        else {
                            event.setCancelled(true);
                            entity.teleport(world.getSpawnLocation());
                        }
                        break;
                    }
                    case FALL: {
                        if (WorldManager.isStarted(worldName))
                            event.setCancelled(true);
                        break;
                    }
                    case PROJECTILE: {
                        event.setDamage(3);
                        break;
                    }
                    default: {
                        event.setCancelled(true);
                        break;
                    }
                }
                break;
            }
            case 11: {
                if (event.getCause() == DamageCause.VOID) {
                    if (WorldManager.isStarted(worldName))
                        event.setDamage(20);
                    else {
                        event.setCancelled(true);
                        entity.teleport(world.getSpawnLocation());
                    }
                } else
                    event.setCancelled(true);
                break;
            }
            case 12: {
                if (event.getCause() == DamageCause.VOID)
                    event.setDamage(20);
                break;
            }
            case 13: {
                if (WorldManager.isStarted(worldName)) {
                    if (event.getCause() == DamageCause.VOID)
                        event.setDamage(20);
                } else {
                    event.setCancelled(true);
                    if (event.getCause() == DamageCause.VOID)
                        entity.teleport(world.getSpawnLocation());
                }
                break;
            }
            case 14: {
                if (!WorldManager.isStarted(worldName)) {
                    event.setCancelled(true);
                    if (event.getCause() == DamageCause.VOID)
                        entity.teleport(world.getSpawnLocation());
                }
                break;
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();
        World world = player.getWorld();
        event.setDeathMessage(null);
        player.setHealth(20);
        if (player.getKiller() != null) {
            Player attacker = player.getKiller();
            if (attacker != player) {
                attacker.sendTitle(new Title("", "Вы убили " + player.getDisplayName(), 5, 50, 5));
                player.sendTitle(new Title("", "Вы убиты " + attacker.getDisplayName(), 5, 50, 5));
                attacker.playSound(attacker.getEyeLocation(), Sound.ENTITY_PLAYER_LEVELUP, Float.MAX_VALUE, 2);
                PlayersUtils.setHype(attacker, PlayersUtils.getHype(attacker.getUniqueId().toString()) + 1);
                attacker.sendActionBar("+1 хайп");
            } else
                player.sendTitle(Titles.death);
        } else
            player.sendTitle(Titles.death);
        switch (WorldManager.getType(world.getName())) {
            default: {
                player.teleport(world.getSpawnLocation());
                player.setFallDistance(0);
                player.setVelocity(LocationUtils.noVelocity);
                player.setFireTicks(-20);
                break;
            }
            case 4:
            case 5:
            case 6:
            case 8:
            case 10:
            case 11: {
                player.teleport(Hub.location);
                GameUtils.tryEndGame(world);
                break;
            }
            case 7: {
                player.teleport(world.getSpawnLocation());
                player.setFallDistance(0);
                player.setVelocity(LocationUtils.noVelocity);
                player.setFireTicks(-20);
                if (!player.getInventory().contains(Items.quests))
                    player.getInventory().setItem(7, Items.quests);
                if (!player.getInventory().contains(Items.menu))
                    player.getInventory().setItem(8, Items.menu);
                break;
            }
            case 2:
            case 12: {
                player.teleport(world.getSpawnLocation());
                player.setFallDistance(0);
                player.setVelocity(LocationUtils.noVelocity);
                player.setNoDamageTicks(50);
                player.setFireTicks(-20);
                break;
            }
            case 13:
            case 14: {
                Location location = player.getLocation();
                if (location.getBlockY() >= 0)
                    event.getDrops().stream().filter(items -> (items != null)).forEach(items -> world.dropItemNaturally(location, items));
                player.teleport(Hub.location);
                player.setFallDistance(0);
                player.setVelocity(LocationUtils.noVelocity);
                GameUtils.tryEndGame(world);
                break;
            }
            case 18: {
                player.teleport(world.getSpawnLocation());
                player.setFallDistance(0);
                player.setVelocity(LocationUtils.noVelocity);
                player.setFireTicks(-20);
                CodingUtils.getParser(world).onPlayerDeath(event);
                break;
            }
        }
        player.setVelocity(LocationUtils.noVelocity);
        player.playSound(player.getEyeLocation(), Sound.ENTITY_IRONGOLEM_DEATH, Float.MAX_VALUE, 1);
        event.setKeepInventory(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onCommand(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        if (!player.isOp()) {
            if (!ChatUtils.chatCooldown.contains(player)) {
                String[] split = event.getMessage().split(" ");
                ChatUtils.chatCooldown.add(player);
                if (!split[0].contains(":")) {
                    switch (split[0].toLowerCase()) {
                        case "/me":
                        case "/trigger": {
                            event.setCancelled(true);
                            player.sendMessage(ServerSettings.unknowCommand);
                            break;
                        }
                    }
                } else {
                    event.setCancelled(true);
                    player.sendMessage(ServerSettings.unknowCommand);
                }
                LiteGame.server.getScheduler().scheduleSyncDelayedTask(LiteGame.plugin, () -> ChatUtils.chatCooldown.remove(player), 30);
            } else {
                event.setCancelled(true);
                player.sendMessage("Вы слишком быстро пишите в чат.");
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onError(ServerExceptionEvent event) {
        String message = event.getException().getMessage();
        switch (message) {
            case "java.io.IOException: Disk quota exceeded": {
                WorldManager.wipeOldWorlds();
                break;
            }
            default: {
                String formatMessage = "Ошибка:§7 " + message;
                LiteGame.server.getOnlinePlayers().forEach(players -> players.sendActionBar(formatMessage));
                break;
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onNote(NotePlayEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onMap(MapInitializeEvent event) {
        MapView map = event.getMap();
        map.getRenderers().forEach(renderers -> map.removeRenderer(renderers));
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onItemConsume(PlayerItemConsumeEvent event) {
        if (WorldManager.getType(event.getPlayer().getWorld().getName()) != 18)
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onVehicle(VehicleCreateEvent event) {
        switch (WorldManager.getType(event.getVehicle().getWorld().getName())) {
            default: {
                event.getVehicle().remove();
                break;
            }
            case 7:
            case 13:
            case 14:
                break;
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBed(PlayerBedEnterEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onHanging(HangingPlaceEvent event) {
        switch (WorldManager.getType(event.getEntity().getWorld().getName())) {
            default: {
                event.setCancelled(true);
                break;
            }
            case 7:
            case 13:
            case 14:
                break;
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onInventoryOpen(InventoryOpenEvent event) {
        HumanEntity player = event.getPlayer();
        switch (WorldManager.getType(player.getWorld().getName())) {
            default: {
                Inventory inventory = event.getInventory();
                if (inventory.getType() == InventoryType.CHEST) {
                    if (inventory.getHolder() != null)
                        event.setCancelled(true);
                } else
                    event.setCancelled(true);
                break;
            }
            case 7: {
                Inventory inventory = event.getInventory();
                if (inventory.getHolder() != null) {
                    if (player.getGameMode() == GameMode.ADVENTURE)
                        event.setCancelled(true);
                }
                break;
            }
            case 13: {
                GameplayUtils.skyWarsInventoryOpenEvent(event);
                break;
            }
            case 14: {
                Inventory inventory = event.getInventory();
                if (inventory.getType() == InventoryType.CHEST) {
                    if (inventory.getHolder() != null) {
                        Location location = inventory.getLocation().add(0.5, 0.5, 0.5);
                        event.setCancelled(true);
                        if (WorldManager.isStarted(location.getWorld().getName())) {
                            location.getBlock().setType(Material.AIR);
                            location.getWorld().dropItemNaturally(location, Items.survivalGamesDrops[LiteGame.random.nextInt(Items.survivalGamesDrops.length)]);
                        }
                    }
                } else
                    event.setCancelled(true);
                break;
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onHeal(EntityRegainHealthEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onHunger(FoodLevelChangeEvent event) {
        event.setFoodLevel(20);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPrepareAnvilEvent(PrepareAnvilEvent event) {
        event.setResult(Items.air);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPrepareItemCraft(PrepareItemCraftEvent event) {
        Recipe recipe = event.getRecipe();
        if (recipe != null) {
            switch (recipe.getResult().getType()) {
                case CHEST: {
                    if (WorldManager.getType(event.getView().getPlayer().getWorld().getName()) == 13)
                        event.getInventory().setResult(Items.air);
                    break;
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPrepareItemEnchant(PrepareItemEnchantEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onEnchantItem(EnchantItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onExpChange(PlayerExpChangeEvent event) {
        event.setAmount(0);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onProjectileLaunch(ProjectileLaunchEvent event) {
        Entity entity = event.getEntity();
        switch (WorldManager.getType(entity.getWorld().getName())) {
            default: {
                event.setCancelled(true);
                break;
            }
            case 0:
            case 4:
            case 5:
            case 7:
            case 10:
            case 12:
            case 13:
            case 14:
            case 18:
                break;
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onProjectileHit(ProjectileHitEvent event) {
        Projectile entity = event.getEntity();
        switch (WorldManager.getType(entity.getWorld().getName())) {
            default: {
                entity.remove();
                break;
            }
            case 4: {
                Block block = event.getHitBlock();
                if (block == null)
                    break;
                block.setType(Material.AIR);
                break;
            }
            case 10: {
                Block block = event.getHitBlock();
                if (block == null)
                    break;
                switch (block.getType()) {
                    case STAINED_CLAY: {
                        block.setData((byte) LiteGame.random.nextInt(16));
                        break;
                    }
                    case GLASS: {
                        block.setType(Material.STAINED_GLASS);
                        block.setData((byte) LiteGame.random.nextInt(16));
                        break;
                    }
                    case STAINED_GLASS: {
                        block.setData((byte) LiteGame.random.nextInt(16));
                        break;
                    }
                    case IRON_FENCE: {
                        block.setType(Material.STAINED_GLASS_PANE);
                        block.setData((byte) LiteGame.random.nextInt(16));
                        break;
                    }
                    case THIN_GLASS: {
                        block.setType(Material.STAINED_GLASS_PANE);
                        block.setData((byte) LiteGame.random.nextInt(16));
                        break;
                    }
                    case STAINED_GLASS_PANE: {
                        block.setData((byte) LiteGame.random.nextInt(16));
                        break;
                    }
                    default: {
                        if (block.getType().isSolid()) {
                            block.setType(Material.STAINED_CLAY);
                            block.setData((byte) LiteGame.random.nextInt(16));
                        }
                        break;
                    }
                }
                break;
            }
            case 12:
            case 18: {
                if (event.getHitEntity() == null)
                    entity.remove();
                break;
            }
            case 5:
            case 7:
            case 13:
            case 14:
                break;
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onStatisticIncrement(PlayerStatisticIncrementEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onExtend(BlockPistonExtendEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onRetract(BlockPistonRetractEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPing(ServerListPingEvent event) {
        event.setMotd(ServerSettings.motd);
        event.setMaxPlayers(LiteGame.server.getOnlinePlayers().size() + 1);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onKick(PlayerKickEvent event) {
        if (!event.isCancelled()) {
            Player player = event.getPlayer();
            String reason = event.getReason();
            switch (reason) {
                case "Flying is not enabled on this server": {
                    if (WorldManager.getType(player.getWorld().getName()) != 0) {
                        event.setReason
                                (
                                        "§lВы кикнуты!§r"
                                                + "\n" +
                                                "Причина: FlyHack."
                                );
                        ChatUtils.broadcastMessage(player.getDisplayName() + "§r кикнут:§7 FlyHack.", player.getWorld().getPlayers());
                    } else
                        event.setCancelled(true);
                    break;
                }
                case "§lВы кикнуты!§r\nПричина: FlyHack.": {
                    ChatUtils.broadcastMessage(player.getDisplayName() + "§r кикнут:§7 FlyHack.", player.getWorld().getPlayers());
                    break;
                }
                case "You dropped your items too quickly (Hacking?)": {
                    event.setCancelled(true);
                    break;
                }
                case "Invalid hotbar selection (Hacking?)": {
                    event.setCancelled(true);
                    break;
                }
                case "disconnect.spam": {
                    event.setCancelled(true);
                    break;
                }
                case "Illegal characters in chat": {
                    event.setCancelled(true);
                    player.sendMessage("�­то сообщение содержит неподдерживаемые символы.");
                    break;
                }
                case "You have been idle for too long!": {
                    event.setCancelled(true);
                    break;
                }
                case "You logged in from another location": {
                    if (PlayersUtils.isLoggined(player.getUniqueId().toString()))
                        event.setCancelled(true);
                    else
                        event.setReason("Вы вошли с другого клиента!");
                    break;
                }
                case "Kicked by an operator.": {
                    event.setReason("§lВы кикнуты!");
                    ChatUtils.broadcastMessage(player.getDisplayName() + "§r кикнут.", player.getWorld().getPlayers());
                    break;
                }
                case "You are banned from this server.": {
                    event.setReason("§lВы заблокированы!");
                    ChatUtils.broadcastMessage(player.getDisplayName() + "§r заблокирован.", player.getWorld().getPlayers());
                    break;
                }
                case "You have been IP banned.": {
                    event.setReason("§lВаш IP заблокирован!");
                    ChatUtils.broadcastMessage(player.getDisplayName() + "§r заблокирован по IP.", player.getWorld().getPlayers());
                    break;
                }
                case "Disconnected": {
                    event.setReason("Вы вышли.");
                    break;
                }
                case "Book edited too quickly!": {
                    event.setReason
                            (
                                    "§lВы кикнуты!"
                                            + "\n" +
                                            "§rПричина: PacketSpam."
                            );
                    ChatUtils.broadcastMessage(player.getDisplayName() + "§r кикнут:§7 PacketSpam.", player.getWorld().getPlayers());
                    break;
                }
                case "ReadTimeoutException : null": {
                    event.setCancelled(true);
                    break;
                }
                default: {
                    switch (reason.split("\n")[0]) {
                        case "§lВы заблокированы!§r":
                            break;
                        case "§lВаш IP заблокирован!§r":
                            break;
                        default: {
                            ChatUtils.broadcastMessage(player.getDisplayName() + "§r кикнут:§7 " + event.getReason(), player.getWorld().getPlayers());
                            break;
                        }
                    }
                    break;
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onIllegalPacket(IllegalPacketEvent event) {
        event.setKickMessage("Недопустимый пакет (читы?)");
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onWorldSave(WorldSaveEvent event) {
        World world = event.getWorld();
        ChatUtils.broadcastMessage("Мир сохранён.", world.getPlayers());
        switch (WorldManager.getType(world.getName())) {
            case 7: {
                SkyBlockUtils.saveAllPlayers(world);
                break;
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onWorldUnload(WorldUnloadEvent event) {
        World world = event.getWorld();
        String worldName = world.getName();
        int type = WorldManager.getType(worldName);
        LiteGame.server.getScheduler().scheduleSyncDelayedTask(LiteGame.plugin, () -> NavigatorUtils.updateMenu(type), 1);
        switch (WorldManager.getType(worldName)) {
            default: {
                WorldManager.removeStarted(worldName);
                break;
            }
            case 1: {
                WorldManager.removeDataHash(worldName);
                break;
            }
            case 7: {
                WorldManager.removeTempHash(worldName);
                break;
            }
            case 17: {
                ArtStudioUtils.clearHash(worldName);
                WorldManager.removeTempHash(worldName);
                break;
            }
            case 18: {
                CodingUtils.removeParser(world);
                break;
            }
        }
    }
}