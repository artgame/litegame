package ru.kirill3345.litegame.listeners;

import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

public class WorldDownloaderListener implements PluginMessageListener {
    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] bytes) {
        player.kickPlayer("Не используйте WorldDownloader!");
    }
}